<?php

namespace DYNBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/le-saviez-vous/")
     */
    public function indexAction()
    {
        return $this->render('DYNBundle:Default:index.html.twig');
    }
}
