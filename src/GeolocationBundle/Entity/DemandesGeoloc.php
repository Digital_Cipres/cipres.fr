<?php

namespace GeolocationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DemandesGeoloc
 *
 * @ORM\Table(name="demandes_geoloc")
 * @ORM\Entity(repositoryClass="GeolocationBundle\Repository\DemandesGeolocRepository")
 */
class DemandesGeoloc
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255, nullable=true)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255)
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="CodePostal", type="string", length=255, nullable=true)
     */
    private $codePostal;

    /**
     * @var string
     *
     * @ORM\Column(name="Ville", type="string", length=255, nullable=true)
     */
    private $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="ChoixContact", type="string", length=255)
     */
    private $choixContact;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=255, nullable=true)
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="CourtierAffiche", type="string", length=255, nullable=true)
     */
    private $CourtierAffiche;

    /**
     * @var string
     *
     * @ORM\Column(name="ProvenanceDemande", type="string", length=255, nullable=true)
     */
    private $ProvenanceDemande;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    public function __construct()
    {
        $this->date = new \DateTime();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return DemandesGeoloc
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return DemandesGeoloc
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return DemandesGeoloc
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set codePostal
     *
     * @param string $codePostal
     *
     * @return DemandesGeoloc
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * Get codePostal
     *
     * @return string
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return DemandesGeoloc
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set choixContact
     *
     * @param string $choixContact
     *
     * @return DemandesGeoloc
     */
    public function setChoixContact($choixContact)
    {
        $this->choixContact = $choixContact;

        return $this;
    }

    /**
     * Get choixContact
     *
     * @return string
     */
    public function getChoixContact()
    {
        return $this->choixContact;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return DemandesGeoloc
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return DemandesGeoloc
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return DemandesGeoloc
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set courtierAffiche
     *
     * @param string $courtierAffiche
     *
     * @return DemandesGeoloc
     */
    public function setCourtierAffiche($courtierAffiche)
    {
        $this->CourtierAffiche = $courtierAffiche;

        return $this;
    }

    /**
     * Get courtierAffiche
     *
     * @return string
     */
    public function getCourtierAffiche()
    {
        return $this->CourtierAffiche;
    }

    /**
     * Set provenanceDemande
     *
     * @param string $provenanceDemande
     *
     * @return DemandesGeoloc
     */
    public function setProvenanceDemande($provenanceDemande)
    {
        $this->ProvenanceDemande = $provenanceDemande;

        return $this;
    }

    /**
     * Get provenanceDemande
     *
     * @return string
     */
    public function getProvenanceDemande()
    {
        return $this->ProvenanceDemande;
    }
}
