<?php

namespace GeolocationBundle\Controller;

use GeolocationBundle\Entity\DemandesGeoloc;
use GeolocationBundle\Form\DemandesGeolocType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ReintermediationController extends Controller
{
    /**
     * @Route("/devis-assurance")
     */
    public function accueilAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entity = new DemandesGeoloc();
        $form = $this->createForm(new DemandesGeolocType(), $entity);

        return $this->render('GeolocationBundle:Default:index.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/trouver-assurance")
     */
    public function accueil2Action()
    {
        $em = $this->getDoctrine()->getManager();

        $entity = new DemandesGeoloc();
        $form = $this->createForm(new DemandesGeolocType(), $entity);

        return $this->render('GeolocationBundle:Default:index_iframes.html.twig', array(
            'form' => $form->createView(),
        ));
    }

//    /**
//     * @Route("/generate-locations")
//     */
//    public function GenerateLocations()
//    {
//        $em = $this->getDoctrine()->getManager();
//
//        $infoslocations = $em
//            ->getRepository('GeolocationBundle:CourtiersGeoloc')
//            ->findAll();
//        ;
//
//        //Output JSON
//        return new JsonResponse($infoslocations);
//    }



    /**
     * @Route("/devis-assurance/votre-recherche", name="demande_geoloc")
     * @Method("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function DemandeGeolocAction(Request $request)
    {
        // Récupération des infos prospect
        $entity = new DemandesGeoloc();
        $form = $this->createForm(new DemandesGeolocType(), $entity);
        $form->handleRequest($request);

        // Enregistrement du formulaire
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
        }

        // Envoi de l'email au prospect
        if($request->request->get('data_for_emails')) {

            $nom_courtier = $request->request->get('nom_courtier');
            $adresse_courtier = $request->request->get('adresse_courtier');
            $telephone_courtier = $request->request->get('telephone_courtier');
            $mobile_courtier = $request->request->get('mobile_courtier');
            $website_courtier = $request->request->get('website_courtier');
            $email_courtier = $request->request->get('email_courtier');
            $postal_courtier = $request->request->get('postal_courtier');
            $ville_courtier = $request->request->get('ville_courtier');

            // Update de la base pour enregistrer le courtier affiché
            $em = $this->getDoctrine()->getManager();
            $courtier = $em
                ->getRepository('GeolocationBundle:DemandesGeoloc')
                ->findOneBy(array(), array('id' => 'DESC'));
            $courtier->setCourtierAffiche($nom_courtier);
            $em->flush();

            $templateFile = "GeolocationBundle:Emails:email_geoloc_prospect.html.twig";

            $body = $this->renderView($templateFile, array(
                'infos_courtier' => array('nom_courtier' => $nom_courtier, 'adresse_courtier' => $adresse_courtier, 'telephone_courtier' => $telephone_courtier, 'website_courtier' => $website_courtier, 'email_courtier' => $email_courtier, 'mobile_courtier' => $mobile_courtier, 'postal_courtier' => $postal_courtier, 'ville_courtier' => $ville_courtier)));
            $message = \Swift_Message::newInstance()
                ->setSubject("[cipres.fr] Résultat de votre recherche : notre Courtier Partenaire le plus proche")
                ->setFrom(array('noreply@cipres.fr' => 'Cipres'))
                ->setTo('gperreau@cipres.fr')
                ->setCc(array('gperreau@cipres.fr'))
                ->setBody($body, 'text/html');

            $this->container->get('mailer')->send($message);


            // Envoi de l'email au courtier

            $email_courtier = $request->request->get('email_courtier');
            $nom_prospect = $request->request->get('nom_prospect');
            $prenom_prospect = $request->request->get('prenom_prospect');
            $adresse_prospect = $request->request->get('adresse_prospect');
            $telephone_prospect = $request->request->get('telephone_prospect');
            $email_prospect = $request->request->get('email_prospect');

            $templateFile = "GeolocationBundle:Emails:email_geoloc_courtier.html.twig";

            $body = $this->renderView($templateFile, array(
                'infos_prospect' => array('email_courtier' => $email_courtier, 'nom_prospect' => $nom_prospect, 'prenom_prospect' => $prenom_prospect, 'adresse_prospect' => $adresse_prospect, 'telephone_prospect' => $telephone_prospect, 'email_prospect' => $email_prospect)));
            $message = \Swift_Message::newInstance()
                ->setSubject("[cipres.fr] Nouvelle demande de contact d'un prospect")
                ->setFrom(array('noreply@cipres.fr' => 'Cipres'))
                ->setTo('gperreau@cipres.fr')
                ->setCc(array('gperreau@cipres.fr'))
                ->setBody($body, 'text/html');

            $this->container->get('mailer')->send($message);
        }


        return $this->render('GeolocationBundle:Default:index.html.twig', array(
            'form' => $form->createView(),
        ));
    }

}
