<?php

namespace GeolocationBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DemandesGeolocType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', 'text', array('label' => 'Nom', 'label_attr' => ['data-error' => "Votre nom doit être composé de lettres uniquement."], 'required' => true, 'attr' => ['class' => 'validate', 'pattern' => '^[A-z]+$']))
            ->add('prenom', 'text', array('label' => 'Prénom', 'label_attr' => ['data-error' => "Votre prénom doit être composé de lettres uniquement."], 'required' => true, 'attr' => ['class' => 'validate', 'pattern' => '^[A-z]+$']))
            ->add('adresse', 'text', array('required' => false))
            ->add('codePostal', 'text', array('required' => false))
            ->add('ville', 'text', array('required' => false))
            ->add('choixContact', "choice", array("required" => true, 'expanded' => true, 'empty_value' => false, 'multiple' => false, 'choices' => array('Oui' => 'Oui', 'Non' => 'Non')))
            ->add('email', 'email', array('required' => false, 'attr' => ['class' => 'validate']))
            ->add('telephone', 'text', array('required' => false))
            ->add('provenanceDemande', 'text', array('required' => false))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GeolocationBundle\Entity\DemandesGeoloc',
            'csrf_protection' => false
        ));
    }
}
