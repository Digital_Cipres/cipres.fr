<?php

namespace PetitsDejsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InscriptionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('codeCourtier', 'text', array('label' => 'Code courtier', 'label_attr' => ['data-error' => "Code courtier invalide"], 'required' => true, 'attr' => ['class' => 'validate', 'pattern' => '^[A-Za-z]?[0-9]{5}$']))
            ->add('nom', 'text', array('label' => 'Nom', 'label_attr' => ['data-error' => "Votre nom doit être composé de lettres uniquement."], 'required' => true, 'attr' => ['class' => 'validate', 'pattern' => '^[A-z_ ]+$']))
            ->add('prenom', 'text', array('label' => 'Prénom', 'label_attr' => ['data-error' => "Votre prénom doit être composé de lettres uniquement."], 'required' => true, 'attr' => ['class' => 'validate', 'pattern' => '^[A-z_ ]+$']))
            ->add('accompagnants', "choice", array("required" => true, 'expanded' => true, 'empty_value' => false, 'multiple' => false, 'choices' => array('Oui' => 'Oui', 'Non' => 'Non')))
            ->add('nbAccompagnants', 'text', array('label' => 'Nombre d\'accompagnants', 'label_attr' => ['data-error' => "Le nombre d'accompagnants doit être inférieur à 10."], 'required' => false, 'attr' => ['class' => 'validate', 'pattern' => '^[0-9]{1}$']))
            ->add('choix_ville', 'text', array('label' => 'Ville', 'required' => false))
            ->add('nomCabinet', 'text', array('required' => false, 'attr' => ['class' => 'validate'] ))
            ->add('emailCourtier', 'text', array('label' => 'Email', 'label_attr' => ['data-error' => "Votre adresse email est invalide."], 'required' => true, 'attr' => ['class' => 'validate', 'pattern' => '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$']))
            ->add('dateLyon', "choice", array("required" => false, 'expanded' => false, 'empty_value' => false, 'multiple' => false, 'choices' => array('29 septembre' => 'le jeudi 29 septembre', '4 octobre' => 'le mardi 4 octobre')))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PetitsDejsBundle\Entity\Inscription',
            'csrf_protection' => false
        ));
    }
}
