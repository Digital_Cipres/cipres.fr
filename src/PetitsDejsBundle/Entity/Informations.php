<?php

namespace PetitsDejsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Informations
 *
 * @ORM\Table(name="informations")
 * @ORM\Entity(repositoryClass="PetitsDejsBundle\Repository\InformationsRepository")
 */
class Informations
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="date", type="string", length=255)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="heure_debut", type="string", length=255)
     */
    private $heureDebut;

    /**
     * @var string
     *
     * @ORM\Column(name="heure_fin", type="string", length=255)
     */
    private $heureFin;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=255)
     */
    private $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="id_ville", type="string", length=255)
     */
    private $idVille;

    /**
     * @var string
     *
     * @ORM\Column(name="hotel", type="string", length=255)
     */
    private $hotel;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255)
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse_postal", type="string", length=255)
     */
    private $adresse_postal;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse_ville", type="string", length=255)
     */
    private $adresse_ville;

    /**
     * @var string
     *
     * @ORM\Column(name="url_map", type="string", length=255)
     */
    private $url_map;

    /**
     * @var string
     *
     * @ORM\Column(name="iframe_map", type="string", length=500)
     */
    private $iframe_map;

    /**
     * @var string
     *
     * @ORM\Column(name="type_reunion", type="string", length=255)
     */
    private $typeReunion;

    /**
     * @var string
     *
     * @ORM\Column(name="inspecteurs", type="string", length=255)
     */
    private $inspecteurs;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param string $date
     *
     * @return Informations
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set heureDebut
     *
     * @param string $heureDebut
     *
     * @return Informations
     */
    public function setHeureDebut($heureDebut)
    {
        $this->heureDebut = $heureDebut;

        return $this;
    }

    /**
     * Get heureDebut
     *
     * @return string
     */
    public function getHeureDebut()
    {
        return $this->heureDebut;
    }

    /**
     * Set heureFin
     *
     * @param string $heureFin
     *
     * @return Informations
     */
    public function setHeureFin($heureFin)
    {
        $this->heureFin = $heureFin;

        return $this;
    }

    /**
     * Get heureFin
     *
     * @return string
     */
    public function getHeureFin()
    {
        return $this->heureFin;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return Informations
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set idVille
     *
     * @param string $idVille
     *
     * @return Informations
     */
    public function setIdVille($idVille)
    {
        $this->idVille = $idVille;

        return $this;
    }

    /**
     * Get idVille
     *
     * @return string
     */
    public function getIdVille()
    {
        return $this->idVille;
    }

    /**
     * Set hotel
     *
     * @param string $hotel
     *
     * @return Informations
     */
    public function setHotel($hotel)
    {
        $this->hotel = $hotel;

        return $this;
    }

    /**
     * Get hotel
     *
     * @return string
     */
    public function getHotel()
    {
        return $this->hotel;
    }

    /**
     * Set typeReunion
     *
     * @param string $typeReunion
     *
     * @return Informations
     */
    public function setTypeReunion($typeReunion)
    {
        $this->typeReunion = $typeReunion;

        return $this;
    }

    /**
     * Get typeReunion
     *
     * @return string
     */
    public function getTypeReunion()
    {
        return $this->typeReunion;
    }

    /**
     * Set inspecteurs
     *
     * @param string $inspecteurs
     *
     * @return Informations
     */
    public function setInspecteurs($inspecteurs)
    {
        $this->inspecteurs = $inspecteurs;

        return $this;
    }

    /**
     * Get inspecteurs
     *
     * @return string
     */
    public function getInspecteurs()
    {
        return $this->inspecteurs;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return Informations
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set urlMap
     *
     * @param string $urlMap
     *
     * @return Informations
     */
    public function setUrlMap($urlMap)
    {
        $this->url_map = $urlMap;

        return $this;
    }

    /**
     * Get urlMap
     *
     * @return string
     */
    public function getUrlMap()
    {
        return $this->url_map;
    }

    /**
     * Set iframeMap
     *
     * @param string $iframeMap
     *
     * @return Informations
     */
    public function setIframeMap($iframeMap)
    {
        $this->iframe_map = $iframeMap;

        return $this;
    }

    /**
     * Get iframeMap
     *
     * @return string
     */
    public function getIframeMap()
    {
        return $this->iframe_map;
    }

    /**
     * Set adressePostal
     *
     * @param string $adressePostal
     *
     * @return Informations
     */
    public function setAdressePostal($adressePostal)
    {
        $this->adresse_postal = $adressePostal;

        return $this;
    }

    /**
     * Get adressePostal
     *
     * @return string
     */
    public function getAdressePostal()
    {
        return $this->adresse_postal;
    }

    /**
     * Set adresseVille
     *
     * @param string $adresseVille
     *
     * @return Informations
     */
    public function setAdresseVille($adresseVille)
    {
        $this->adresse_ville = $adresseVille;

        return $this;
    }

    /**
     * Get adresseVille
     *
     * @return string
     */
    public function getAdresseVille()
    {
        return $this->adresse_ville;
    }
}
