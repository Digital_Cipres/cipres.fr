<?php

namespace PetitsDejsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Inscription
 *
 * @ORM\Table(name="inscription")
 * @ORM\Entity(repositoryClass="PetitsDejsBundle\Repository\InscriptionRepository")
 */
class Inscription
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="accompagnants", type="string", nullable=true)
     */
    private $accompagnants;

    /**
     * @var string
     *
     * @ORM\Column(name="CodeCourtier", type="string", length=7, nullable=true )
     */
    private $codeCourtier;

    /**
     * @var string
     *
     * @ORM\Column(name="nomCabinet", type="string", length=255, nullable=true )
     */
    private $nomCabinet;

    /**
     * @var string
     *
     * @ORM\Column(name="emailCourtier", type="string", length=255, nullable=true )
     */
    private $emailCourtier;

    /**
     * @var string
     *
     * @ORM\Column(name="ChoixVille", type="string", length=255)
     */
    private $ChoixVille;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="Prenom", type="string", length=255)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="NbAccompagnants", type="string", length=2, nullable=true)
     */
    private $nbAccompagnants;

    /**
     * @var string
     *
     * @ORM\Column(name="date_lyon", type="string", length=255, nullable=true)
     */
    private $date_lyon;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateInscription", type="datetime")
     */
    private $dateInscription;


    public function __construct()
    {
        $this->dateInscription = new \DateTime();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set courtierPartenaire
     *
     * @param boolean $courtierPartenaire
     *
     * @return Inscription
     */
    public function setCourtierPartenaire($courtierPartenaire)
    {
        $this->courtierPartenaire = $courtierPartenaire;

        return $this;
    }

    /**
     * Get courtierPartenaire
     *
     * @return bool
     */
    public function getCourtierPartenaire()
    {
        return $this->courtierPartenaire;
    }

    /**
     * Set codeCourtier
     *
     * @param string $codeCourtier
     *
     * @return Inscription
     */
    public function setCodeCourtier($codeCourtier)
    {
        $this->codeCourtier = $codeCourtier;

        return $this;
    }

    /**
     * Get codeCourtier
     *
     * @return string
     */
    public function getCodeCourtier()
    {
        return $this->codeCourtier;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Inscription
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Inscription
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set nbAccompagnants
     *
     * @param string $nbAccompagnants
     *
     * @return Inscription
     */
    public function setNbAccompagnants($nbAccompagnants)
    {
        $this->nbAccompagnants = $nbAccompagnants;

        return $this;
    }

    /**
     * Get nbAccompagnants
     *
     * @return string
     */
    public function getNbAccompagnants()
    {
        return $this->nbAccompagnants;
    }

    /**
     * Set accompagnants
     *
     * @param boolean $accompagnants
     *
     * @return Inscription
     */
    public function setAccompagnants($accompagnants)
    {
        $this->accompagnants = $accompagnants;

        return $this;
    }

    /**
     * Get accompagnants
     *
     * @return boolean
     */
    public function getAccompagnants()
    {
        return $this->accompagnants;
    }

    /**
     * Set choixVille
     *
     * @param string $choixVille
     *
     * @return Inscription
     */
    public function setChoixVille($choixVille)
    {
        $this->ChoixVille = $choixVille;

        return $this;
    }

    /**
     * Get choixVille
     *
     * @return string
     */
    public function getChoixVille()
    {
        return $this->ChoixVille;
    }

    /**
     * Set nomCabinet
     *
     * @param string $nomCabinet
     *
     * @return Inscription
     */
    public function setNomCabinet($nomCabinet)
    {
        $this->nomCabinet = $nomCabinet;

        return $this;
    }

    /**
     * Get nomCabinet
     *
     * @return string
     */
    public function getNomCabinet()
    {
        return $this->nomCabinet;
    }

    /**
     * Set emailCourtier
     *
     * @param string $emailCourtier
     *
     * @return Inscription
     */
    public function setEmailCourtier($emailCourtier)
    {
        $this->emailCourtier = $emailCourtier;

        return $this;
    }

    /**
     * Get emailCourtier
     *
     * @return string
     */
    public function getEmailCourtier()
    {
        return $this->emailCourtier;
    }

    /**
     * Set dateLyon
     *
     * @param string $dateLyon
     *
     * @return Inscription
     */
    public function setDateLyon($dateLyon)
    {
        $this->date_lyon = $dateLyon;

        return $this;
    }

    /**
     * Get dateLyon
     *
     * @return string
     */
    public function getDateLyon()
    {
        return $this->date_lyon;
    }

    /**
     * Set dateInscription
     *
     * @param \DateTime $dateInscription
     *
     * @return Inscription
     */
    public function setDateInscription($dateInscription)
    {
        $this->dateInscription = $dateInscription;

        return $this;
    }

    /**
     * Get dateInscription
     *
     * @return \DateTime
     */
    public function getDateInscription()
    {
        return $this->dateInscription;
    }
}
