<?php

namespace PetitsDejsBundle\Controller;

use PetitsDejsBundle\Entity\Inscription;
use PetitsDejsBundle\Form\InscriptionType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/reunion-information-2016")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        return $this->render('PetitsDejsBundle:Default:index.html.twig', array(
            'success' => 'false'
        ));
    }


    /**
     * @param Request $request
     * @Route("/reunion-information-2016/infos")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    function getEventInfosAction(Request $request)
    {
        $ville = $request->request->get('ville_choisie');

        $em = $this->getDoctrine()->getManager();

        $infos_ville = $em->getRepository('PetitsDejsBundle:Informations')->findBy(
            array('idVille' => $ville)
        );

        $inscription = new Inscription();
        $form = $this->createForm(new InscriptionType(), $inscription);

        return $this->render('PetitsDejsBundle:Default:infos_villes.html.twig', array(
            'informations' => $infos_ville,
            'form' => $form->createView(),
        ));
    }


    /**
     * Traitement du formulaire d'inscription
     *
     * @Route("/reunion-information-2016/inscription", name="inscription_reunion")
     * @Method("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    function inscriptionReunion(Request $request)
    {

        $inscription = new Inscription();
        $form = $this->createForm(new InscriptionType(), $inscription);

        $form->handleRequest($request);
        $data = $form->getData();

        $ville_choisie = $data->getChoixVille();
        $em = $this->getDoctrine()->getManager();

        $infos_ville_choisie = $em->getRepository('PetitsDejsBundle:Informations')->findBy(
            array('idVille' => $ville_choisie)
        );

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($inscription);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Merci pour votre inscription !');
            $success = 'true';


            // Envoi des emails interne
            $templateFile = "PetitsDejsBundle:Emails:email_inscription.html.twig";

            $body = $this->renderView($templateFile, array('infos_participant' => $data));
            $message = \Swift_Message::newInstance()
                ->setSubject("[cipres.fr] Inscription à la réunion d'information")
                ->setFrom(array('noreply@cipres.fr' => 'Cipres'))
//                     ->setTo('rdujardin@cipres.fr ')
//                    ->setBcc(array('commercial@cipres.fr'))
                ->setBcc(array('gperreau@cipres.fr'))
                ->setBody($body, 'text/html');

            $this->container->get('mailer')->send($message);

            //Envoi de l'email de confirmation courtier
            $templateFile = "PetitsDejsBundle:Emails:email_confirmation.html.twig";

            $body = $this->renderView($templateFile, array('infos_participant' => $data, 'infos_ville' => $infos_ville_choisie));
            $message = \Swift_Message::newInstance()
                ->setSubject("[cipres.fr] Votre inscription à la réunion d'information CIPRÉS")
                ->setFrom(array('noreply@cipres.fr' => 'Cipres'))
                ->setTo($inscription->getEmailCourtier())
                ->setBody($body, 'text/html');

            $this->container->get('mailer')->send($message);

            // Retour
            return $this->render('PetitsDejsBundle:Default:index.html.twig', array(
                'form' => $form->createView(),
                'success' => $success,
                'infos_participant' => $data,
                'infos_ville' => $infos_ville_choisie,
            ));

        }
    }





//    CONTROLLERS POUR LES INSCRIPTIONS INSPECTEURS
    /**
     * @Route("/reunion-information-Cipres-2016")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index2Action(Request $request)
    {
        return $this->render('PetitsDejsBundle:Default:index2.html.twig', array(
            'success' => 'false'
        ));
    }


    /**
     * @param Request $request
     * @Route("/reunion-information-2016/infos2")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    function getEventInfos2Action(Request $request)
    {
        $ville = $request->request->get('ville_choisie');

        $em = $this->getDoctrine()->getManager();

        $infos_ville = $em->getRepository('PetitsDejsBundle:Informations')->findBy(
            array('idVille' => $ville)
        );

        $inscription = new Inscription();
        $form = $this->createForm(new InscriptionType(), $inscription);

        return $this->render('PetitsDejsBundle:Default:infos_villes2.html.twig', array(
            'informations' => $infos_ville,
            'form' => $form->createView(),
        ));
    }


    /**
     * Traitement du formulaire d'inscription
     *
     * @Route("/reunion-information-Cipres-2016/inscription", name="inscription_reunion2")
     * @Method("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    function inscriptionReunion2(Request $request)
    {

        $inscription = new Inscription();
        $form = $this->createForm(new InscriptionType(), $inscription);

        $form->handleRequest($request);
        $data = $form->getData();

        $ville_choisie = $data->getChoixVille();
        $em = $this->getDoctrine()->getManager();

        $infos_ville_choisie = $em->getRepository('PetitsDejsBundle:Informations')->findBy(
            array('idVille' => $ville_choisie)
        );

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($inscription);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Merci pour votre inscription !');
            $success = 'true';


            // Envoi des emails interne
            $templateFile = "PetitsDejsBundle:Emails:email_inscription.html.twig";

            $body = $this->renderView($templateFile, array('infos_participant' => $data));
            $message = \Swift_Message::newInstance()
                ->setSubject("[cipres.fr] Inscription à la réunion d'information")
                ->setFrom(array('noreply@cipres.fr' => 'Cipres'))
//                     ->setTo('rdujardin@cipres.fr ')
//                    ->setBcc(array('commercial@cipres.fr'))
                ->setBcc(array('gperreau@cipres.fr'))
                ->setBody($body, 'text/html');

            $this->container->get('mailer')->send($message);

            //Envoi de l'email de confirmation courtier
            $templateFile = "PetitsDejsBundle:Emails:email_confirmation.html.twig";

            $body = $this->renderView($templateFile, array('infos_participant' => $data, 'infos_ville' => $infos_ville_choisie));
            $message = \Swift_Message::newInstance()
                ->setSubject("[cipres.fr] Votre inscription à la réunion d'information CIPRÉS")
                ->setFrom(array('noreply@cipres.fr' => 'Cipres'))
                ->setTo($inscription->getEmailCourtier())
                ->setBody($body, 'text/html');

            $this->container->get('mailer')->send($message);

            // Retour
            return $this->render('PetitsDejsBundle:Default:index.html.twig', array(
                'form' => $form->createView(),
                'success' => $success,
                'infos_participant' => $data,
                'infos_ville' => $infos_ville_choisie,
            ));
        }
    }
}
