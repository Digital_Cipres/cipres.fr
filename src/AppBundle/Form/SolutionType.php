<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class SolutionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre', 'text', array('label' => 'Titre', 'required' => true))
            ->add('sstitre', 'text', array('label' => 'Sous titre', 'required' => false))
            ->add('title', 'text', array('label' => 'Balise Title', 'required' => false))
            ->add('descr', 'text', array('label' => 'Meta Description', 'required' => false))
            ->add('slug', 'text', array('label' => 'Permaliens', 'required' => false))
            ->add('resume', 'text', array('label' => 'Résumé', 'required' => false))
            ->add('image', "file", array("data_class" => null, "required" => false))
            ->add('contenu', 'textarea', array('attr' => array('placeholder' => 'Contenu',), 'required' => false))
        	->add('homepage', "choice", array("required" => false,  'choices' => array(true => 'Oui', false => 'Non')))
        	->add('courtier', "choice", array("required" => false,  'choices' => array(true => 'Oui', false => 'Non')))
        	->add('categorie', "choice", array("required" => false, 'choices' => array('1' => 'TNS', '2' => 'Entreprises', '3' => 'Salariés', '4' => 'Services assurés', '5' => 'Courtiers', '6' => 'Mandataires sociaux')))
        	->add('url', 'url', array('label' => 'URL, si votre bloc doit rediriger vers une autre page (Mettre http://)', 'required' => false))
        	->add('resumetns', 'textarea', array('attr' => array('placeholder' => 'Resumé pour la page TNS',), 'required' => false));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Solution',
        ));
    }

    public function getName()
    {
        return 'appbundle_solutiontype';
    }
}
