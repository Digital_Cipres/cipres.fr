<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class PresseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre', 'text', array('label' => 'Titre', 'required' => true))
            ->add('source', 'text', array('label' => 'Source', 'required' => false))
            ->add('date', 'text', array('label' => 'Date', 'required' => false))
            ->add('resume', 'text', array('label' => 'Résumé', 'required' => false))
            ->add('image', "file", array("data_class" => null, "required" => false))
            ->add('fichier', "file", array("data_class" => null, "required" => false))
            ->add('url', 'url', array('label' => 'URL', 'required' => false))
            ->add('contenu', 'textarea', array('attr' => array('placeholder' => 'Contenu',), 'required' => false))
        	->add('categorie', "choice", array("required" => false,  'choices' => array('1' => 'Dossier', '2' => 'Communiqué')));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Presse',
        ));
    }

    public function getName()
    {
        return 'appbundle_pressetype';
    }
}
