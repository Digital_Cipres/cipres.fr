<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class ActualiteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre', 'text', array('label' => 'Titre', 'required' => true))
            ->add('title', 'text', array('label' => 'Balise Title', 'required' => true))
            ->add('descr', 'text', array('label' => 'Meta Description', 'required' => false))
            ->add('slug', 'text', array('label' => 'Permaliens', 'required' => true))
            ->add('date', 'date', array('widget' => 'single_text', 'format' => 'dd/MM/yyyy', 'label' => 'Date', 'required' => false))

            ->add('debut_publication', 'datetime', [
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy HH:mm',
                'attr' => [
                    'class' => 'form-control input-inline datetimepicker',
                    'data-provide' => 'datetimepicker',
                    'data-date-format' => 'DD-MM-YYYY HH:mm'
                ]
            ])
            ->add('fin_publication', 'datetime', [
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy HH:mm',
                'attr' => [
                    'class' => 'form-control input-inline datetimepicker',
                    'data-provide' => 'datetimepicker',
                    'data-date-format' => 'DD-MM-YYYY HH:mm'
                ]
            ])
            ->add('image', "file", array("data_class" => null, "required" => false))
            ->add('contenu', 'textarea', array('attr' => array('placeholder' => 'Contenu',), 'required' => false))
        	->add('categorie', "choice", array("required" => false,  'choices' => array('1' => 'Agenda', '2' => 'Actualités', '3' => 'Conseils', '4' => 'Service Landing Courtier')))
        	->add('url', 'url', array('label' => 'URL', 'required' => false))
            ->add('enabled', "choice", array('label' => 'Publication', "required" => true,  'choices' => array('1' => 'Publiée  ', '0' => 'Dépubliée')));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Actualite',
        ));
    }

    public function getName()
    {
        return 'appbundle_actualitetype';
    }
}
