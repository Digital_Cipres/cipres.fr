<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class SlideType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('image', "file", array("data_class" => null, "required" => false))
            ->add('code', 'textarea', array('attr' => array('placeholder' => 'Code',), 'required' => false))
            ->add('enabled', "choice", array("required" => false,  'choices' => array(true => 'Oui', false => 'Non')))
        	->add('categorie', "choice", array("required" => false,  'choices' => array('1' => 'Homepage', '2' => 'Landing Courtier', '3' => 'Landing TNS')));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Slide',
        	'csrf_protection' => false
        ));
    }

    public function getName()
    {
        return 'appbundle_slidetype';
    }
}
