<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class MediaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre', 'text', array('label' => 'Titre', 'required' => true))
            ->add('resume', 'text', array('label' => 'Résumé', 'required' => false))
            ->add('image', "file", array("data_class" => null, "required" => false))
            ->add('fichier', "file", array("data_class" => null, "required" => false))
            ->add('url', 'url', array('label' => 'URL (Mettre http://)', 'required' => false))
        	->add('categorie', "choice", array("required" => false,  'choices' => array('1' => 'Photos', '3' => 'Logos')));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Media',
        ));
    }

    public function getName()
    {
        return 'appbundle_mediatype';
    }
}
