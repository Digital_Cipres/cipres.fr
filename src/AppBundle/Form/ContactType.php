<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('source', "choice", array("required" => true, 'empty_value'  => '--', 'empty_data' => null, 'preferred_choices' => array(null),  'choices' => array('1' => 'Un courtier', '2' => 'Un TNS ou une entreprise', '3' => 'Un salarié', '4' => 'Un expert comptable')))
            ->add('souhait', "choice", array("required" => false, 'empty_value'  => '--',  'choices' => array('1' => 'Devenir courtier partenaire', '2' => 'Avoir des informations supplémentaires', '3' => 'Prendre un rendez-vous')))
            ->add('code', 'text', array('label' => 'Code courtier', 'required' => false))
            /*->add('code1', 'text', array('label' => 'Code courtier suite', 'required' => false))*/
            ->add('nomCabinet', 'text', array('label' => 'Nom du cabinet', 'required' => false))
            ->add('nom', 'text', array('label' => 'Nom', 'required' => false))
            ->add('prenom', 'text', array('label' => 'Prénom', 'required' => false))
            ->add('nomInterlocuteur', 'text', array('label' => 'Nom', 'required' => false))
            ->add('prenomInterlocuteur', 'text', array('label' => 'Prénom', 'required' => false))
            ->add('contactPar1', "choice", array("required" => false, 'expanded' => true, 'empty_value' => false, 'multiple' => false, 'choices' => array('1' => 'Email', '2' => 'Téléphone')))
            ->add('email', 'email', array('label' => 'Email', 'required' => false))
            ->add('telephone', 'text', array('label' => 'Téléphone', 'required' => false))
            ->add('emailInterlocuteur', 'email', array('label' => 'Email', 'required' => false))
            ->add('telephoneInterlocuteur', 'text', array('label' => 'Téléphone', 'required' => false))
            ->add('emailCabinet', 'email', array('label' => 'Email', 'required' => false))
            ->add('telephoneCabinet', 'text', array('label' => 'Téléphone', 'required' => false))
            ->add('message', 'textarea', array('label' => 'Message', 'required' => false))
            ->add('adresse', 'text', array('label' => 'Adresse', 'required' => false))
            ->add('codePostal', 'text', array('label' => 'Code Postal', 'required' => false))
            ->add('ville', 'text', array('label' => 'Ville', 'required' => false))
            ->add('siret', 'text', array('label' => 'N° SIRET', 'required' => false))
            ->add('orias', 'text', array('label' => 'N° ORIAS', 'required' => false))
            ->add('catOrias', "choice", array("required" => false, 'expanded' => true, 'empty_value' => false, 'multiple' => false, 'choices' => array('0' => 'Principale', '1' => 'Accessoire')))
            ->add('statut', "choice", array("required" => false, 'expanded' => true, 'empty_value' => false, 'multiple' => false, 'choices' => array('0' => 'Courtier', '1' => 'Agent', '2' => 'Autre')))
            ->add('contactPar2', "choice", array("required" => false, 'expanded' => true, 'empty_value' => false, 'multiple' => false, 'choices' => array('1' => 'A mon cabinet', '2' => 'Par téléphone', '3' => 'Par email')))
            ->add('preference', "choice", array("required" => false, 'expanded' => true, 'empty_value' => false, 'multiple' => false, 'choices' => array('1' => 'Le matin', '2' => 'L\'après-midi')))
            ->add('offre', "choice", array("required" => false, 'expanded' => true, 'multiple' => true, 'choices' => array('1' => 'Prévoyance', '2' => 'Santé', '3' => 'Assurances collectives', '4' => 'Homme clé', '5' => 'Frais généraux permanents')))
        	->add('raison', 'text', array('label' => 'Raison sociale', 'required' => false))
        	->add('objet', 'text', array('label' => 'Objet', 'required' => false))
        	->add('fichier', "file", array("data_class" => null, "required" => false))
        	->add('fichier2', "file", array("data_class" => null, "required" => false))
        	->add('fichier3', "file", array("data_class" => null, "required" => false))
        	->add('fichier4', "file", array("data_class" => null, "required" => false))

            ->add('civilite', "choice", array("required" => false, 'expanded' => true, 'empty_value' => false, 'multiple' => false, 'choices' => array('M' => 'M', 'Mme' => 'Mme')))
            ->add('client', "choice", array("required" => false, 'expanded' => true, 'empty_value' => false, 'multiple' => false, 'choices' => array('0' => 'Oui', '1' => 'Non')))
            ->add('client_salarie', "choice", array("required" => false, 'expanded' => true, 'empty_value' => false, 'multiple' => false, 'choices' => array('0' => 'Oui', '1' => 'Non')))
            ->add('objet_reclam', 'text', array('label' => 'Objet de votre réclamation', 'required' => false))
            ->add('motif_reclam', 'textarea', array('label' => 'Motif de la réclamation', 'required' => false))
            ->add('num_rcs', 'text', array('label' => 'N° d\'immatriculation au RCS ou au répertoire des métiers', 'required' => false))
            ->add('num_contrat', 'text', array('label' => 'Numéro de contrat', 'required' => false))
            ->add('date_naissance', 'date', [
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'attr' => [
                    'class' => 'form-control input-inline datepicker',
                    'data-provide' => 'datepicker',
                    'data-date-format' => 'dd-mm-yyyy'
                ]
            ])
            ->add('nom_ste', 'text', array('label' => 'Nom de la société', 'required' => false))
            ->add('fonction_EC', 'text', array('label' => 'Fonction', 'required' => false))
            ->add('taille_entreprise', "choice", array('label' => 'Taille de l\'entreprise', "required" => true, 'empty_value'  => '--', 'empty_data' => null, 'preferred_choices' => array(null),  'choices' => array('TNS' => 'TNS', 'TPE' => 'TPE ( < 10 salariés )', 'PME' => 'PME ( >11 salariés )', 'GD_compte' => 'Grand compte ( < 100 salariés )')))
            ->add('besoins_EC', "choice", array('label' => 'Vos besoins', "required" => false, 'expanded' => true, 'multiple' => true, 'choices' => array('dirigeant' => 'Complémentaire santé et/ou prévoyance du dirigeant', 'salaries' => 'Complémentaire santé et/ou prévoyance des salariés')))

        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Contact',
        	'csrf_protection' => false
        ));
    }

    public function getName()
    {
        return 'appbundle_contacttype';
    }
}
