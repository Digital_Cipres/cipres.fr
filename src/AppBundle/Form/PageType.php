<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class PageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        	->add('image', "file", array("data_class" => null, "required" => false))
        	->add('title', 'text', array('label' => 'Balise Title', 'required' => true))
        	->add('descr', 'text', array('label' => 'Meta Description', 'required' => false))
        	->add('slug', 'text', array('label' => 'Permaliens', 'required' => false))
            ->add('titre', 'text', array('label' => 'Titre', 'required' => true))
            ->add('contenu', 'textarea', array('attr' => array('placeholder' => 'Contenu',), 'required' => false))
            ->add('enabled', "choice", array("required" => false,  'choices' => array(true => 'Oui', false => 'Non')));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Page',
        ));
    }

    public function getName()
    {
        return 'appbundle_pagetype';
    }
}
