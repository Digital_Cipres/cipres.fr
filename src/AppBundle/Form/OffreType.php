<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class OffreType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre', 'text', array('label' => 'Titre', 'required' => true))
            ->add('date', 'text', array('label' => 'Date', 'required' => false))
            ->add('sstitre', 'text', array('label' => 'Sous titre', 'required' => false))
            ->add('reference', 'text', array('label' => 'Référence', 'required' => false))
            ->add('resume', 'text', array('label' => 'Résumé', 'required' => false))
            ->add('contenu', 'textarea', array('attr' => array('placeholder' => 'Contenu',), 'required' => false))
            ->add('contrat', 'text', array('label' => 'Contrat', 'required' => false))
            ->add('salaire', 'text', array('label' => 'Salaire', 'required' => false))
            ->add('lieu', 'text', array('label' => 'Lieu', 'required' => false))
            ->add('fichier', "file", array("data_class" => null, "required" => false))
        	->add('enabled', "choice", array("required" => false,  'choices' => array(true => 'Oui', false => 'Non')));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Offre',
        ));
    }

    public function getName()
    {
        return 'appbundle_offretype';
    }
}
