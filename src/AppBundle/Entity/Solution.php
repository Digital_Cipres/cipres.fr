<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Solution
 *
 * @ORM\Table(name="cipres_solution")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SolutionRepository")
 */
class Solution
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;
    
    /**
     * @var string
     *
     * @ORM\Column(name="sstitre", type="string", length=255, nullable=true)
     */
    private $sstitre;

    /**
     * @var string
     *
     * @ORM\Column(name="resume", type="string", length=255, nullable=true)
     */
    private $resume;

    /**
     * 
	 * @Assert\File(maxSize = "3M", 
	 *     mimeTypesMessage = "Veuillez charger une image compatible (PNG, JPG, GIF)", mimeTypes={"image/png", "image/jpeg", "image/gif"})
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="contenu", type="text")
     */
    private $contenu;

    /**
     * @var bool
     *
     * @ORM\Column(name="homepage", type="boolean", nullable=true)
     */
    private $homepage;
    
    /**
     * @var bool
     *
     * @ORM\Column(name="courtier", type="boolean", nullable=true)
     */
    private $courtier;
    
    /**
     * @var string
     *
     * @ORM\Column(name="categorie", type="string", nullable=true)
     */
    private $categorie;
    
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;
    
    /**
     * @var string
     *
     * @ORM\Column(name="descr", type="string", length=255, nullable=true)
     */
    private $descr;
    
    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=128, unique=true)
     */
    private $slug;
    
    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;
    
    /**
     * @var string
     *
     * @ORM\Column(name="resumetns", type="text", nullable=true)
     */
    private $resumetns;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Solution
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }
    
    /**
     * Set sstitre
     *
     * @param string $sstitre
     * @return Solution
     */
    public function setSstitre($sstitre)
    {
    	$this->sstitre = $sstitre;
    
    	return $this;
    }
    
    /**
     * Get sstitre
     *
     * @return string
     */
    public function getSstitre()
    {
    	return $this->sstitre;
    }

    /**
     * Set resume
     *
     * @param string $resume
     * @return Solution
     */
    public function setResume($resume)
    {
        $this->resume = $resume;

        return $this;
    }

    /**
     * Get resume
     *
     * @return string 
     */
    public function getResume()
    {
        return $this->resume;
    }

    public function getFullImagePath() {
        return null === $this->image ? null : $this->getUploadRootDir(). $this->image;
    }
 
    protected function getUploadRootDir() {
        return $this->getTmpUploadRootDir().$this->getId()."/";
    }
 
    protected function getTmpUploadRootDir() {
        return __DIR__ . '/../../../web/upload/Solution/';
    }
 
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function uploadImage() {
        // the file property can be empty if the field is not required
        if (null === $this->image) {
            return;
        }
        if(!$this->id){
            $this->image->move($this->getUploadRootDir(), $this->image->getClientOriginalName());
            $this->setImage($this->image->getClientOriginalName());
        }elseif(is_object($this->image)){
            $this->image->move($this->getUploadRootDir(), $this->image->getClientOriginalName());
            $this->setImage($this->image->getClientOriginalName());
        }
    }
     
    /**
     * @ORM\PostPersist()
     */
    public function moveImage()
    {
        if (null === $this->image) {
            return;
        }
        if(!is_dir($this->getUploadRootDir())){
            mkdir($this->getUploadRootDir());
        }
        @copy($this->getTmpUploadRootDir().$this->image, $this->getFullImagePath());
        @unlink($this->getTmpUploadRootDir().$this->image);
    }
 
    /**
     * @ORM\PreRemove()
     */
    public function removeImage()
    {
        @unlink($this->getFullImagePath());
        @rmdir($this->getUploadRootDir());
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Slide
     */
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     * @return Solution
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string 
     */
    public function getContenu()
    {
        return $this->contenu;
    }
    
    /**
     * Set homepage
     *
     * @param boolean $homepage
     * @return Solution
     */
    public function setHomepage($homepage)
    {
    	$this->homepage = $homepage;
    
    	return $this;
    }
    
    /**
     * Get homepage
     *
     * @return boolean
     */
    public function getHomepage()
    {
    	return $this->homepage;
    }
    
    /**
     * Set courtier
     *
     * @param boolean $courtier
     * @return Solution
     */
    public function setCourtier($courtier)
    {
    	$this->courtier = $courtier;
    
    	return $this;
    }
    
    /**
     * Get courtier
     *
     * @return boolean
     */
    public function getCourtier()
    {
    	return $this->courtier;
    }
    
    /**
     * Set categorie
     *
     * @param integer $categorie
     * @return Solution
     */
    public function setCategorie($categorie)
    {
    	$this->categorie = $categorie;
    
    	return $this;
    }
    
    /**
     * Get categorie
     *
     * @return string
     */
    public function getCategorie()
    {
    	return $this->categorie;
    }
    
    /**
     * Set title
     *
     * @param string $title
     * @return Solution
     */
    public function setTitle($title)
    {
    	$this->title = $title;
    
    	return $this;
    }
    
    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
    	return $this->title;
    }
    
    /**
     * Set descr
     *
     * @param string $descr
     * @return Solution
     */
    public function setDescr($descr)
    {
    	$this->descr = $descr;
    
    	return $this;
    }
    
    /**
     * Get descr
     *
     * @return string
     */
    public function getDescr()
    {
    	return $this->descr;
    }
    
    /**
     * Set slug
     *
     * @param string $slug
     * @return Solution
     */
    public function setSlug($slug)
    {
    	$this->slug = $slug;
    
    	return $this;
    }
    
    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
    	return $this->slug;
    }
    
    /**
     * Set url
     *
     * @param string $url
     * @return Solution
     */
    public function setUrl($url)
    {
    	$this->url = $url;
    
    	return $this;
    }
    
    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
    	return $this->url;
    }
    
    /**
     * Set resumetns
     *
     * @param string $resumetns
     * @return Solution
     */
    public function setResumetns($resumetns)
    {
    	$this->resumetns = $resumetns;
    
    	return $this;
    }
    
    /**
     * Get resumetns
     *
     * @return string
     */
    public function getResumetns()
    {
    	return $this->resumetns;
    }
}
