<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Page
 *
 * @ORM\Table(name="cipres_page")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PageRepository")
 */
class Page
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="contenu", type="text", nullable=true)
     */
    private $contenu;

    /**
     * @var int
     *
     * @ORM\Column(name="menu", type="integer", nullable=true)
     */
    private $menu;

    /**
     * @var int
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    private $position;

    /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=true)
     */
    private $enabled;
    
    /**
     *
     * @Assert\File(maxSize = "20M",
     *     mimeTypesMessage = "Veuillez charger une image compatible (PNG, JPG, GIF)", mimeTypes={"image/png", "image/jpeg", "image/gif"})
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;
	
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;
    
    /**
     * @var string
     *
     * @ORM\Column(name="descr", type="string", length=255, nullable=true)
     */
    private $descr;
    
    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=128, unique=true)
     */
    private $slug;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Page
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     * @return Page
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string 
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Set menu
     *
     * @param integer $menu
     * @return Page
     */
    public function setMenu($menu)
    {
        $this->menu = $menu;

        return $this;
    }

    /**
     * Get menu
     *
     * @return integer 
     */
    public function getMenu()
    {
        return $this->menu;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return Page
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return Page
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }
    
    public function getFullImagePath() {
    	return null === $this->image ? null : $this->getUploadRootDir(). $this->image;
    }
    
    protected function getUploadRootDir() {
    	return $this->getTmpUploadRootDir().$this->getId()."/";
    }
    
    protected function getTmpUploadRootDir() {
    	return __DIR__ . '/../../../web/upload/Page/';
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function uploadImage() {
    	// the file property can be empty if the field is not required
    	if (null === $this->image) {
    		return;
    	}
    	if(!$this->id){
    		$this->image->move($this->getUploadRootDir(), $this->image->getClientOriginalName());
    		$this->setImage($this->image->getClientOriginalName());
    	}elseif(is_object($this->image)){
    		$this->image->move($this->getUploadRootDir(), $this->image->getClientOriginalName());
    		$this->setImage($this->image->getClientOriginalName());
    	}
    }
     
    /**
     * @ORM\PostPersist()
     */
    public function moveImage()
    {
    	if (null === $this->image) {
    		return;
    	}
    	if(!is_dir($this->getUploadRootDir())){
    		mkdir($this->getUploadRootDir());
    	}
    	@copy($this->getTmpUploadRootDir().$this->image, $this->getFullImagePath());
    	@unlink($this->getTmpUploadRootDir().$this->image);
    }
    
    /**
     * @ORM\PreRemove()
     */
    public function removeImage()
    {
    	@unlink($this->getFullImagePath());
    	@rmdir($this->getUploadRootDir());
    }
    
    /**
     * Set image
     *
     * @param string $image
     * @return Slide
     */
    public function setImage($image)
    {
    	$this->image = $image;
    
    	return $this;
    }
    
    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
    	return $this->image;
    }
    
    /**
     * Set title
     *
     * @param string $title
     * @return Page
     */
    public function setTitle($title)
    {
    	$this->title = $title;
    
    	return $this;
    }
    
    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
    	return $this->title;
    }
    
    /**
     * Set descr
     *
     * @param string $descr
     * @return Page
     */
    public function setDescr($descr)
    {
    	$this->descr = $descr;
    
    	return $this;
    }
    
    /**
     * Get desc
     *
     * @return string
     */
    public function getDescr()
    {
    	return $this->descr;
    }
    
    /**
     * Set slug
     *
     * @param string $slug
     * @return Page
     */
    public function setSlug($slug)
    {
    	$this->slug = $slug;
    
    	return $this;
    }
    
    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
    	return $this->slug;
    }
}
