<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Media
 *
 * @ORM\Table(name="cipres_media")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MediaRepository")
 */
class Media
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;

    /**
     * 
	 * @Assert\File(maxSize = "30M", 
	 *     mimeTypesMessage = "Veuillez charger une image compatible (PNG, JPG, GIF)", mimeTypes={"image/png", "image/jpeg", "image/gif"})
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @var int
     *
     * @ORM\Column(name="categorie", type="integer")
     */
    private $categorie;

    /**
     * @var string
     *
     * @ORM\Column(name="resume", type="string", length=255, nullable=true)
     */
    private $resume;

    /**
     * 
	 * @Assert\File(maxSize = "300M", 
	 *     mimeTypesMessage = "Veuillez charger une image compatible (PNG, JPG, GIF, PDF, Word, Excel, ZIP)", mimeTypes={"image/png", "image/jpeg", "image/gif", "application/pdf", "application/msword", "application/excel", "application/vnd.ms-excel", "application/x-excel", "application/x-msexcel", "application/zip", "application/octet-stream"})
     * @ORM\Column(name="fichier", type="string", length=255, nullable=true)
     */
    private $fichier;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Media
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    public function getFullImagePath() {
        return null === $this->image ? null : $this->getUploadRootDir(). $this->image;
    }
 
    protected function getUploadRootDir() {
        return $this->getTmpUploadRootDir().$this->getId()."/";
    }
 
    protected function getTmpUploadRootDir() {
        return __DIR__ . '/../../../web/upload/Media/';
    }
 
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function uploadImage() {
        // the file property can be empty if the field is not required
        if (null === $this->image) {
            return;
        }
        if(!$this->id){
            $this->image->move($this->getUploadRootDir(), $this->image->getClientOriginalName());
            $this->setImage($this->image->getClientOriginalName());
        }elseif(is_object($this->image)){
            $this->image->move($this->getUploadRootDir(), $this->image->getClientOriginalName());
            $this->setImage($this->image->getClientOriginalName());
        }
    }
     
    /**
     * @ORM\PostPersist()
     */
    public function moveImage()
    {
        if (null === $this->image) {
            return;
        }
        if(!is_dir($this->getUploadRootDir())){
            mkdir($this->getUploadRootDir());
        }
        @copy($this->getTmpUploadRootDir().$this->image, $this->getFullImagePath());
        @unlink($this->getTmpUploadRootDir().$this->image);
    }
 
    /**
     * @ORM\PreRemove()
     */
    public function removeImage()
    {
        @unlink($this->getFullImagePath());
        @rmdir($this->getUploadRootDir());
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Media
     */
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set categorie
     *
     * @param integer $categorie
     * @return Media
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return integer 
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set resume
     *
     * @param string $resume
     * @return Media
     */
    public function setResume($resume)
    {
        $this->resume = $resume;

        return $this;
    }

    /**
     * Get resume
     *
     * @return string 
     */
    public function getResume()
    {
        return $this->resume;
    }

    public function getFullFichierPath() {
        return null === $this->fichier ? null : $this->getUploadRootDir(). $this->fichier;
    }
 
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function uploadFichier() {
        if (null === $this->fichier) {
            return;
        }
        if(!$this->id){
            $this->fichier->move($this->getUploadRootDir(), $this->fichier->getClientOriginalName());
            $this->setFichier($this->fichier->getClientOriginalName());
        }elseif(is_object($this->fichier)){
            $this->fichier->move($this->getUploadRootDir(), $this->fichier->getClientOriginalName());
            $this->setFichier($this->fichier->getClientOriginalName());
        }
    }
     
    /**
     * @ORM\PostPersist()
     */
    public function moveFichier()
    {
        if (null === $this->fichier) {
            return;
        }
        if(!is_dir($this->getUploadRootDir())){
            mkdir($this->getUploadRootDir());
        }
        @copy($this->getTmpUploadRootDir().$this->fichier, $this->getFullFichierPath());
        @unlink($this->getTmpUploadRootDir().$this->fichier);
    }
 
    /**
     * @ORM\PreRemove()
     */
    public function removeFichier()
    {
        @unlink($this->getFullFichierPath());
        @rmdir($this->getUploadRootDir());
    }

    /**
     * Set fichier
     *
     * @param string $fichier
     * @return Media
     */
    public function setFichier($fichier)
    {
        $this->fichier = $fichier;
    
        return $this;
    }

    /**
     * Get fichier
     *
     * @return string 
     */
    public function getFichier()
    {
        return $this->fichier;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Media
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }
}
