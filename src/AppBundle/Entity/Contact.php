<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Contact
 *
 * @ORM\Table(name="cipres_contact")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ContactRepository")
 */
class Contact
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    private $type;

    /**
     * @var int
     *
     * @ORM\Column(name="source", type="integer")
     */
    private $source;
    
    /**
     * @var int
     *
     * @ORM\Column(name="souhait", type="integer", nullable=true)
     */
    private $souhait;

    /**
     * @var int
     *
     * @ORM\Column(name="client", type="integer", nullable=true)
     */
    private $client;

    /**
     * @var int
     *
     * @ORM\Column(name="client_salarie", type="integer", nullable=true)
     */
    private $client_salarie;
    
    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=5, nullable=true)
     */
    private $code;
    
    /**
     * @var string
     *
     * @ORM\Column(name="code1", type="string", length=6, nullable=true)
     */
    private $code1;
    
    /**
     * @var string
     *
     * @ORM\Column(name="nomCabinet", type="string", length=255, nullable=true)
     */
    private $nomCabinet;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=true)
     */
    private $nom;
    
    /**
     * @var string
     *
     * @ORM\Column(name="nomInterlocuteur", type="string", length=255, nullable=true)
     */
    private $nomInterlocuteur;
    
    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255, nullable=true)
     */
    private $prenom;
    
    /**
     * @var string
     *
     * @ORM\Column(name="prenomInterlocuteur", type="string", length=255, nullable=true)
     */
    private $prenomInterlocuteur;
    
    /**
     * @var int
     *
     * @ORM\Column(name="contactPar1", type="integer", nullable=true)
     */
    private $contactPar1;
    
    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;
    
    /**
     * @var string
     *
     * @ORM\Column(name="emailCabinet", type="string", length=255, nullable=true)
     */
    private $emailCabinet;
    
    /**
     * @var string
     *
     * @ORM\Column(name="emailInterlocuteur", type="string", length=255, nullable=true)
     */
    private $emailInterlocuteur;
    
    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=10, nullable=true)
     */
    private $telephone;
    
    /**
     * @var string
     *
     * @ORM\Column(name="telephoneCabinet", type="string", length=10, nullable=true)
     */
    private $telephoneCabinet;
    
    /**
     * @var string
     *
     * @ORM\Column(name="telephoneInterlocuteur", type="string", length=10, nullable=true)
     */
    private $telephoneInterlocuteur;
    
    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text", nullable=true)
     */
    private $message;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255, nullable=true)
     */
    private $adresse;
    
    /**
     * @var string
     *
     * @ORM\Column(name="codePostal", type="string", length=5, nullable=true)
     */
    private $codePostal;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=255, nullable=true)
     */
    private $ville;
    
    /**
     * @var int
     *
     * @ORM\Column(name="contactPar2", type="integer", nullable=true)
     */
    private $contactPar2;
    
    /**
     * @var int
     *
     * @ORM\Column(name="preference", type="integer", nullable=true)
     */
    private $preference;
    
    /**
     * @var string
     *
     * @ORM\Column(name="offre", type="array", nullable=true)
     */
    private $offre;

    /**
     * @var string
     *
     * @ORM\Column(name="taille_entreprise", type="array", nullable=true)
     */
    private $taille_entreprise;

    /**
     * @var string
     *
     * @ORM\Column(name="besoins_EC", type="array", nullable=true)
     */
    private $besoins_EC;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_ste", type="string", length=255, nullable=true)
     */
    private $nom_ste;

    /**
     * @var string
     *
     * @ORM\Column(name="fonction_EC", type="string", length=255, nullable=true)
     */
    private $fonction_EC;

    /**
     * @var string
     *
     * @ORM\Column(name="raison", type="string", length=255, nullable=true)
     */
    private $raison;
    
    /**
     * @var string
     *
     * @ORM\Column(name="objet", type="string", length=255, nullable=true)
     */
    private $objet;

    /**
     * @var string
     *
     * @ORM\Column(name="nomDirigeant", type="string", length=255, nullable=true)
     */
    private $nomDirigeant;

    /**
     * @var bool
     *
     * @ORM\Column(name="optin", type="boolean", nullable=true)
     */
    private $optin;
    
    /**
     *
     * @Assert\File(maxSize = "300M",
     *     mimeTypesMessage = "Veuillez charger une image compatible (PNG, JPG, GIF, PDF, Word, Excel)", mimeTypes={"image/png", "image/jpeg", "image/gif", "application/pdf", "application/msword", "application/excel", "application/vnd.ms-excel", "application/x-excel", "application/x-msexcel"})
     * @ORM\Column(name="fichier", type="string", length=255, nullable=true)
     */
    private $fichier;
    
    /**
     *
     * @Assert\File(maxSize = "300M",
     *     mimeTypesMessage = "Veuillez charger une image compatible (PNG, JPG, GIF, PDF, Word, Excel)", mimeTypes={"image/png", "image/jpeg", "image/gif", "application/pdf", "application/msword", "application/excel", "application/vnd.ms-excel", "application/x-excel", "application/x-msexcel"})
     * @ORM\Column(name="fichier2", type="string", length=255, nullable=true)
     */
    private $fichier2;
    
    /**
     *
     * @Assert\File(maxSize = "300M",
     *     mimeTypesMessage = "Veuillez charger une image compatible (PNG, JPG, GIF, PDF, Word, Excel)", mimeTypes={"image/png", "image/jpeg", "image/gif", "application/pdf", "application/msword", "application/excel", "application/vnd.ms-excel", "application/x-excel", "application/x-msexcel"})
     * @ORM\Column(name="fichier3", type="string", length=255, nullable=true)
     */
    private $fichier3;
    
    /**
     *
     * @Assert\File(maxSize = "300M",
     *     mimeTypesMessage = "Veuillez charger une image compatible (PNG, JPG, GIF, PDF, Word, Excel)", mimeTypes={"image/png", "image/jpeg", "image/gif", "application/pdf", "application/msword", "application/excel", "application/vnd.ms-excel", "application/x-excel", "application/x-msexcel"})
     * @ORM\Column(name="fichier4", type="string", length=255, nullable=true)
     */
    private $fichier4;
    
    /**
     * @var string
     *
     * @ORM\Column(name="siret", type="string", length=255, nullable=true)
     */
    private $siret;
    
    /**
     * @var string
     *
     * @ORM\Column(name="orias", type="string", length=255, nullable=true)
     */
    private $orias;
    
    /**
     * @var int
     *
     * @ORM\Column(name="catOrias", type="integer", nullable=true)
     */
    private $catOrias;
    
    /**
     * @var int
     *
     * @ORM\Column(name="statut", type="integer", nullable=true)
     */
    private $statut;

    /**
     * @var string
     *
     * @ORM\Column(name="civilite", type="string", nullable=true)
     */
    private $civilite;

    /**
     * @var string
     *
     * @ORM\Column(name="objet_reclam", type="string", length=255, nullable=true)
     */
    private $objet_reclam;

    /**
     * @var string
     *
     * @ORM\Column(name="motif_reclam", type="text", nullable=true)
     */
    private $motif_reclam;

    /**
     * @var string
     *
     * @ORM\Column(name="num_rcs", type="string", length=255, nullable=true)
     */
    private $num_rcs;

    /**
     * @var string
     *
     * @ORM\Column(name="num_contrat", type="string", length=255, nullable=true)
     */
    private $num_contrat;

    /**
     * @var date
     *
     * @ORM\Column(name="date_naissance", type="date", nullable=true)
     */
    private $date_naissance;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Contact
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set source
     *
     * @param integer $source
     * @return Contact
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source
     *
     * @return integer 
     */
    public function getSource()
    {
        return $this->source;
    }
    
    /**
     * Set souhait
     *
     * @param integer $souhait
     * @return Contact
     */
    public function setSouhait($souhait)
    {
    	$this->souhait = $souhait;
    
    	return $this;
    }
    
    /**
     * Get souhait
     *
     * @return integer
     */
    public function getSouhait()
    {
    	return $this->souhait;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Contact
     */
    public function setCode($code)
    {
    	$this->code = $code;
    
    	return $this;
    }
    
    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
    	return $this->code;
    }
    
    /**
     * Set code1
     *
     * @param string $code1
     * @return Contact
     */
    public function setCode1($code1)
    {
    	$this->code1 = $code1;
    
    	return $this;
    }
    
    /**
     * Get code1
     *
     * @return string
     */
    public function getCode1()
    {
    	return $this->code1;
    }
    
    /**
     * Set nomCabinet
     *
     * @param string $nomCabinet
     * @return Contact
     */
    public function setNomCabinet($nomCabinet)
    {
    	$this->nomCabinet = $nomCabinet;
    
    	return $this;
    }
    
    /**
     * Get nomCabinet
     *
     * @return string
     */
    public function getNomCabinet()
    {
    	return $this->nomCabinet;
    }
    
    /**
     * Set nom
     *
     * @param string $nom
     * @return Contact
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }
    
    /**
     * Set nomInterlocuteur
     *
     * @param string $nomInterlocuteur
     * @return Contact
     */
    public function setNomInterlocuteur($nomInterlocuteur)
    {
    	$this->nomInterlocuteur = $nomInterlocuteur;
    
    	return $this;
    }
    
    /**
     * Get nomInterlocuteur
     *
     * @return string
     */
    public function getNomInterlocuteur()
    {
    	return $this->nomInterlocuteur;
    }
    
    /**
     * Set prenom
     *
     * @param string $prenom
     * @return Contact
     */
    public function setPrenom($prenom)
    {
    	$this->prenom = $prenom;
    
    	return $this;
    }
    
    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
    	return $this->prenom;
    }
    
    /**
     * Set prenomInterlocuteur
     *
     * @param string $prenomInterlocuteur
     * @return Contact
     */
    public function setPrenomInterlocuteur($prenomInterlocuteur)
    {
    	$this->prenomInterlocuteur = $prenomInterlocuteur;
    
    	return $this;
    }
    
    /**
     * Get prenomInterlocuteur
     *
     * @return string
     */
    public function getPrenomInterlocuteur()
    {
    	return $this->prenomInterlocuteur;
    }
    
    /**
     * Set contactPar1
     *
     * @param integer $contactPar1
     * @return Contact
     */
    public function setContactPar1($contactPar1)
    {
    	$this->contactPar1 = $contactPar1;
    
    	return $this;
    }
    
    /**
     * Get contactPar1
     *
     * @return integer
     */
    public function getContactPar1()
    {
    	return $this->contactPar1;
    }
    
    /**
     * Set contactPar2
     *
     * @param integer $contactPar2
     * @return Contact
     */
    public function setContactPar2($contactPar2)
    {
    	$this->contactPar2 = $contactPar2;
    
    	return $this;
    }
    
    /**
     * Get contactPar2
     *
     * @return integer
     */
    public function getContactPar2()
    {
    	return $this->contactPar2;
    }
    
    /**
     * Set preference
     *
     * @param integer $preference
     * @return Contact
     */
    public function setPreference($preference)
    {
    	$this->preference = $preference;
    
    	return $this;
    }
    
    /**
     * Get preference
     *
     * @return integer
     */
    public function getPreference()
    {
    	return $this->preference;
    }
    
    /**
     * Set offre
     *
     * @param string $offre
     * @return Contact
     */
    public function setOffre($offre)
    {
    	$this->offre = $offre;
    
    	return $this;
    }
    
    /**
     * Get offre
     *
     * @return Contact
     */
    public function getOffre()
    {
    	return $this->offre;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     * @return Contact
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string 
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set ville
     *
     * @param string $ville
     * @return Contact
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string 
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set codePostal
     *
     * @param string $codePostal
     * @return Contact
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * Get codePostal
     *
     * @return string 
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return Contact
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }
    
    /**
     * Set telephoneInterlocuteur
     *
     * @param string $telephoneInterlocuteur
     * @return Contact
     */
    public function setTelephoneInterlocuteur($telephoneInterlocuteur)
    {
    	$this->telephoneInterlocuteur = $telephoneInterlocuteur;
    
    	return $this;
    }
    
    /**
     * Get telephoneInterlocuteur
     *
     * @return string
     */
    public function getTelephoneInterlocuteur()
    {
    	return $this->telephoneInterlocuteur;
    }
    
    /**
     * Set telephoneCabinet
     *
     * @param string $telephoneCabinet
     * @return Contact
     */
    public function setTelephoneCabinet($telephoneCabinet)
    {
    	$this->telephoneCabinet = $telephoneCabinet;
    
    	return $this;
    }
    
    /**
     * Get telephoneCabinet
     *
     * @return string
     */
    public function getTelephoneCabinet()
    {
    	return $this->telephoneCabinet;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Contact
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }
    
    /**
     * Set emailInterlocuteur
     *
     * @param string $emailInterlocuteur
     * @return Contact
     */
    public function setEmailInterlocuteur($emailInterlocuteur)
    {
    	$this->emailInterlocuteur = $emailInterlocuteur;
    
    	return $this;
    }
    
    /**
     * Get emailInterlocuteur
     *
     * @return string
     */
    public function getEmailInterlocuteur()
    {
    	return $this->emailInterlocuteur;
    }
    
    /**
     * Set emailCabinet
     *
     * @param string $emailCabinet
     * @return Contact
     */
    public function setEmailCabinet($emailCabinet)
    {
    	$this->emailCabinet = $emailCabinet;
    
    	return $this;
    }
    
    /**
     * Get emailCabinet
     *
     * @return string
     */
    public function getEmailCabinet()
    {
    	return $this->emailCabinet;
    }

    /**
     * Set raison
     *
     * @param string $raison
     * @return Contact
     */
    public function setRaison($raison)
    {
        $this->raison = $raison;

        return $this;
    }

    /**
     * Get raison
     *
     * @return string 
     */
    public function getRaison()
    {
        return $this->raison;
    }
    
    /**
     * Set objet
     *
     * @param string $objet
     * @return Contact
     */
    public function setObjet($objet)
    {
    	$this->objet = $objet;
    
    	return $this;
    }
    
    /**
     * Get objet
     *
     * @return string
     */
    public function getObjet()
    {
    	return $this->objet;
    }

    /**
     * Set nomDirigeant
     *
     * @param string $nomDirigeant
     * @return Contact
     */
    public function setNomDirigeant($nomDirigeant)
    {
        $this->nomDirigeant = $nomDirigeant;

        return $this;
    }

    /**
     * Get nomDirigeant
     *
     * @return string 
     */
    public function getNomDirigeant()
    {
        return $this->nomDirigeant;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return Contact
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set optin
     *
     * @param boolean $optin
     * @return Contact
     */
    public function setOptin($optin)
    {
        $this->optin = $optin;

        return $this;
    }

    /**
     * Get optin
     *
     * @return boolean 
     */
    public function getOptin()
    {
        return $this->optin;
    }
    
    public function getFullFichierPath() {
    	return null === $this->fichier ? null : $this->getUploadRootDir(). $this->fichier;
    }
    
    protected function getUploadRootDir() {
    	return $this->getTmpUploadRootDir().$this->getId()."/";
    }
    
    protected function getTmpUploadRootDir() {
    	return __DIR__ . '/../../../web/upload/Contact/';
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function uploadFichier() {
    	if (null === $this->fichier) {
    		return;
    	}
    	if(!$this->id){
    		$this->fichier->move($this->getUploadRootDir(), $this->fichier->getClientOriginalName());
    		$this->setFichier($this->fichier->getClientOriginalName());
    	}elseif(is_object($this->fichier)){
    		$this->fichier->move($this->getUploadRootDir(), $this->fichier->getClientOriginalName());
    		$this->setFichier($this->fichier->getClientOriginalName());
    	}
    }
     
    /**
     * @ORM\PostPersist()
     */
    public function moveFichier()
    {
    	if (null === $this->fichier) {
    		return;
    	}
    	if(!is_dir($this->getUploadRootDir())){
    		mkdir($this->getUploadRootDir());
    	}
    	@copy($this->getTmpUploadRootDir().$this->fichier, $this->getFullFichierPath());
    	@unlink($this->getTmpUploadRootDir().$this->fichier);
    }
    
    /**
     * @ORM\PreRemove()
     */
    public function removeFichier()
    {
    	@unlink($this->getFullFichierPath());
    	@rmdir($this->getUploadRootDir());
    }
    
    /**
     * Set fichier
     *
     * @param string $fichier
     * @return Contact
     */
    public function setFichier($fichier)
    {
    	$this->fichier = $fichier;
    
    	return $this;
    }
    
    /**
     * Get fichier
     *
     * @return string
     */
    public function getFichier()
    {
    	return $this->fichier;
    }
    
    public function getFullFichier2Path() {
    	return null === $this->fichier2 ? null : $this->getUploadRootDir(). $this->fichier2;
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function uploadFichier2() {
    	if (null === $this->fichier2) {
    		return;
    	}
    	if(!$this->id){
    		$this->fichier2->move($this->getUploadRootDir(), $this->fichier2->getClientOriginalName());
    		$this->setFichier2($this->fichier2->getClientOriginalName());
    	}elseif(is_object($this->fichier2)){
    		$this->fichier2->move($this->getUploadRootDir(), $this->fichier2->getClientOriginalName());
    		$this->setFichier2($this->fichier2->getClientOriginalName());
    	}
    }
     
    /**
     * @ORM\PostPersist()
     */
    public function moveFichier2()
    {
    	if (null === $this->fichier2) {
    		return;
    	}
    	if(!is_dir($this->getUploadRootDir())){
    		mkdir($this->getUploadRootDir());
    	}
    	@copy($this->getTmpUploadRootDir().$this->fichier2, $this->getFullFichier2Path());
    	@unlink($this->getTmpUploadRootDir().$this->fichier2);
    }
    
    /**
     * @ORM\PreRemove()
     */
    public function removeFichier2()
    {
    	@unlink($this->getFullFichier2Path());
    	@rmdir($this->getUploadRootDir());
    }
    
    /**
     * Set fichier2
     *
     * @param string $fichier2
     * @return Contact
     */
    public function setFichier2($fichier2)
    {
    	$this->fichier2 = $fichier2;
    
    	return $this;
    }
    
    /**
     * Get fichier2
     *
     * @return string
     */
    public function getFichier2()
    {
    	return $this->fichier2;
    }
    
    public function getFullFichier3Path() {
    	return null === $this->fichier3 ? null : $this->getUploadRootDir(). $this->fichier3;
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function uploadFichier3() {
    	if (null === $this->fichier3) {
    		return;
    	}
    	if(!$this->id){
    		$this->fichier3->move($this->getUploadRootDir(), $this->fichier3->getClientOriginalName());
    		$this->setFichier3($this->fichier3->getClientOriginalName());
    	}elseif(is_object($this->fichier3)){
    		$this->fichier3->move($this->getUploadRootDir(), $this->fichier3->getClientOriginalName());
    		$this->setFichier3($this->fichier3->getClientOriginalName());
    	}
    }
     
    /**
     * @ORM\PostPersist()
     */
    public function moveFichier3()
    {
    	if (null === $this->fichier3) {
    		return;
    	}
    	if(!is_dir($this->getUploadRootDir())){
    		mkdir($this->getUploadRootDir());
    	}
    	@copy($this->getTmpUploadRootDir().$this->fichier3, $this->getFullFichier3Path());
    	@unlink($this->getTmpUploadRootDir().$this->fichier3);
    }
    
    /**
     * @ORM\PreRemove()
     */
    public function removeFichier3()
    {
    	@unlink($this->getFullFichier3Path());
    	@rmdir($this->getUploadRootDir());
    }
    
    /**
     * Set fichier3
     *
     * @param string $fichier3
     * @return Contact
     */
    public function setFichier3($fichier3)
    {
    	$this->fichier3 = $fichier3;
    
    	return $this;
    }
    
    /**
     * Get fichier3
     *
     * @return string
     */
    public function getFichier3()
    {
    	return $this->fichier3;
    }
    
    public function getFullFichier4Path() {
    	return null === $this->fichier4 ? null : $this->getUploadRootDir(). $this->fichier4;
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function uploadFichier4() {
    	if (null === $this->fichier4) {
    		return;
    	}
    	if(!$this->id){
    		$this->fichier4->move($this->getUploadRootDir(), $this->fichier4->getClientOriginalName());
    		$this->setFichier4($this->fichier4->getClientOriginalName());
    	}elseif(is_object($this->fichier4)){
    		$this->fichier4->move($this->getUploadRootDir(), $this->fichier4->getClientOriginalName());
    		$this->setFichier4($this->fichier4->getClientOriginalName());
    	}
    }
     
    /**
     * @ORM\PostPersist()
     */
    public function moveFichier4()
    {
    	if (null === $this->fichier4) {
    		return;
    	}
    	if(!is_dir($this->getUploadRootDir())){
    		mkdir($this->getUploadRootDir());
    	}
    	@copy($this->getTmpUploadRootDir().$this->fichier4, $this->getFullFichier4Path());
    	@unlink($this->getTmpUploadRootDir().$this->fichier4);
    }
    
    /**
     * @ORM\PreRemove()
     */
    public function removeFichier4()
    {
    	@unlink($this->getFullFichier4Path());
    	@rmdir($this->getUploadRootDir());
    }
    
    /**
     * Set fichier4
     *
     * @param string $fichier4
     * @return Contact
     */
    public function setFichier4($fichier4)
    {
    	$this->fichier4 = $fichier4;
    
    	return $this;
    }
    
    /**
     * Get fichier4
     *
     * @return string
     */
    public function getFichier4()
    {
    	return $this->fichier4;
    }
    
    /**
     * Set siret
     *
     * @param string $siret
     * @return Contact
     */
    public function setSiret($siret)
    {
    	$this->siret = $siret;
    
    	return $this;
    }
    
    /**
     * Get siret
     *
     * @return string
     */
    public function getSiret()
    {
    	return $this->siret;
    }
    
    /**
     * Set orias
     *
     * @param string $orias
     * @return Contact
     */
    public function setOrias($orias)
    {
    	$this->orias = $orias;
    
    	return $this;
    }
    
    /**
     * Get orias
     *
     * @return string
     */
    public function getOrias()
    {
    	return $this->orias;
    }
    
    /**
     * Set catOrias
     *
     * @param integer $catOrias
     * @return Contact
     */
    public function setCatOrias($catOrias)
    {
    	$this->catOrias = $catOrias;
    
    	return $this;
    }
    
    /**
     * Get catOrias
     *
     * @return integer
     */
    public function getCatOrias()
    {
    	return $this->catOrias;
    }
    
    /**
     * Set statut
     *
     * @param integer $statut
     * @return Contact
     */
    public function setStatut($statut)
    {
    	$this->statut = $statut;
    
    	return $this;
    }
    
    /**
     * Get statut
     *
     * @return integer
     */
    public function getStatut()
    {
    	return $this->statut;
    }

    /**
     * Set civilite
     *
     * @param string $civilite
     *
     * @return Contact
     */
    public function setCivilite($civilite)
    {
        $this->civilite = $civilite;

        return $this;
    }

    /**
     * Get civilite
     *
     * @return string
     */
    public function getCivilite()
    {
        return $this->civilite;
    }

    /**
     * Set objetReclam
     *
     * @param string $objetReclam
     *
     * @return Contact
     */
    public function setObjetReclam($objetReclam)
    {
        $this->objet_reclam = $objetReclam;

        return $this;
    }

    /**
     * Get objetReclam
     *
     * @return string
     */
    public function getObjetReclam()
    {
        return $this->objet_reclam;
    }

    /**
     * Set motifReclam
     *
     * @param string $motifReclam
     *
     * @return Contact
     */
    public function setMotifReclam($motifReclam)
    {
        $this->motif_reclam = $motifReclam;

        return $this;
    }

    /**
     * Get motifReclam
     *
     * @return string
     */
    public function getMotifReclam()
    {
        return $this->motif_reclam;
    }

    /**
     * Set numRcs
     *
     * @param string $numRcs
     *
     * @return Contact
     */
    public function setNumRcs($numRcs)
    {
        $this->num_rcs = $numRcs;

        return $this;
    }

    /**
     * Get numRcs
     *
     * @return string
     */
    public function getNumRcs()
    {
        return $this->num_rcs;
    }

    /**
     * Set numContrat
     *
     * @param string $numContrat
     *
     * @return Contact
     */
    public function setNumContrat($numContrat)
    {
        $this->num_contrat = $numContrat;

        return $this;
    }

    /**
     * Get numContrat
     *
     * @return string
     */
    public function getNumContrat()
    {
        return $this->num_contrat;
    }

    /**
     * Set dateNaissance
     *
     * @param \DateTime $dateNaissance
     *
     * @return Contact
     */
    public function setDateNaissance($dateNaissance)
    {
        $this->date_naissance = $dateNaissance;

        return $this;
    }

    /**
     * Get dateNaissance
     *
     * @return \DateTime
     */
    public function getDateNaissance()
    {
        return $this->date_naissance;
    }

    /**
     * Set client
     *
     * @param integer $client
     *
     * @return Contact
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return integer
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set clientSalarie
     *
     * @param integer $clientSalarie
     *
     * @return Contact
     */
    public function setClientSalarie($clientSalarie)
    {
        $this->client_salarie = $clientSalarie;

        return $this;
    }

    /**
     * Get clientSalarie
     *
     * @return integer
     */
    public function getClientSalarie()
    {
        return $this->client_salarie;
    }

    /**
     * Set tailleEntreprise
     *
     * @param array $tailleEntreprise
     *
     * @return Contact
     */
    public function setTailleEntreprise($tailleEntreprise)
    {
        $this->taille_entreprise = $tailleEntreprise;

        return $this;
    }

    /**
     * Get tailleEntreprise
     *
     * @return array
     */
    public function getTailleEntreprise()
    {
        return $this->taille_entreprise;
    }

    /**
     * Set besoinsEC
     *
     * @param array $besoinsEC
     *
     * @return Contact
     */
    public function setBesoinsEC($besoinsEC)
    {
        $this->besoins_EC = $besoinsEC;

        return $this;
    }

    /**
     * Get besoinsEC
     *
     * @return array
     */
    public function getBesoinsEC()
    {
        return $this->besoins_EC;
    }

    /**
     * Set nomSte
     *
     * @param string $nomSte
     *
     * @return Contact
     */
    public function setNomSte($nomSte)
    {
        $this->nom_ste = $nomSte;

        return $this;
    }

    /**
     * Get nomSte
     *
     * @return string
     */
    public function getNomSte()
    {
        return $this->nom_ste;
    }

    /**
     * Set fonctionEC
     *
     * @param string $fonctionEC
     *
     * @return Contact
     */
    public function setFonctionEC($fonctionEC)
    {
        $this->fonction_EC = $fonctionEC;

        return $this;
    }

    /**
     * Get fonctionEC
     *
     * @return string
     */
    public function getFonctionEC()
    {
        return $this->fonction_EC;
    }
}
