<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Offre
 *
 * @ORM\Table(name="cipres_offre")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OffreRepository")
 */
class Offre
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;
    
    /**
     * @var string
     *
     * @ORM\Column(name="date", type="string", length=255, nullable=true)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="sstitre", type="string", length=255, nullable=true)
     */
    private $sstitre;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255, nullable=true)
     */
    private $reference;

    /**
     * @var string
     *
     * @ORM\Column(name="resume", type="string", length=255, nullable=true)
     */
    private $resume;

    /**
     * @var string
     *
     * @ORM\Column(name="contenu", type="text", nullable=true)
     */
    private $contenu;

    /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=true)
     */
    private $enabled;
    
    /**
     * @var string
     *
     * @ORM\Column(name="contrat", type="string", length=255, nullable=true)
     */
    private $contrat;
    
    /**
     * @var string
     *
     * @ORM\Column(name="salaire", type="string", length=255, nullable=true)
     */
    private $salaire;
    
    /**
     * @var string
     *
     * @ORM\Column(name="lieu", type="string", length=255, nullable=true)
     */
    private $lieu;
    
    /**
     *
     * @Assert\File(maxSize = "300M",
     *     mimeTypesMessage = "Veuillez charger une image compatible (PNG, JPG, GIF, PDF, Word, Excel)", mimeTypes={"image/png", "image/jpeg", "image/gif", "application/pdf", "application/msword", "application/excel", "application/vnd.ms-excel", "application/x-excel", "application/x-msexcel"})
     * @ORM\Column(name="fichier", type="string", length=255, nullable=true)
     */
    private $fichier;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateAjout", type="datetime")
     */
    private $dateAjout;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Offre
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }
    
    /**
     * Set date
     *
     * @param string $date
     * @return Offre
     */
    public function setDate($date)
    {
    	$this->date = $date;
    
    	return $this;
    }
    
    /**
     * Get date
     *
     * @return string
     */
    public function getDate()
    {
    	return $this->date;
    }

    /**
     * Set sstitre
     *
     * @param string $sstitre
     * @return Offre
     */
    public function setSstitre($sstitre)
    {
        $this->sstitre = $sstitre;

        return $this;
    }

    /**
     * Get sstitre
     *
     * @return string 
     */
    public function getSstitre()
    {
        return $this->sstitre;
    }

    /**
     * Set reference
     *
     * @param string $reference
     * @return Offre
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string 
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set resume
     *
     * @param string $resume
     * @return Offre
     */
    public function setResume($resume)
    {
        $this->resume = $resume;

        return $this;
    }

    /**
     * Get resume
     *
     * @return string 
     */
    public function getResume()
    {
        return $this->resume;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     * @return Offre
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string 
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return Offre
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }
    
    /**
     * Set contrat
     *
     * @param string $contrat
     * @return Offre
     */
    public function setContrat($contrat)
    {
    	$this->contrat = $contrat;
    
    	return $this;
    }
    
    /**
     * Get contrat
     *
     * @return string
     */
    public function getContrat()
    {
    	return $this->contrat;
    }
    
    /**
     * Set salaire
     *
     * @param string $salaire
     * @return Offre
     */
    public function setSalaire($salaire)
    {
    	$this->salaire = $salaire;
    
    	return $this;
    }
    
    /**
     * Get salaire
     *
     * @return string
     */
    public function getSalaire()
    {
    	return $this->salaire;
    }
    
    /**
     * Set lieu
     *
     * @param string $lieu
     * @return Offre
     */
    public function setLieu($lieu)
    {
    	$this->lieu = $lieu;
    
    	return $this;
    }
    
    /**
     * Get lieu
     *
     * @return string
     */
    public function getLieu()
    {
    	return $this->lieu;
    }
    
    public function getFullFichierPath() {
    	return null === $this->fichier ? null : $this->getUploadRootDir(). $this->fichier;
    }
    
    protected function getUploadRootDir() {
    	return $this->getTmpUploadRootDir().$this->getId()."/";
    }
    
    protected function getTmpUploadRootDir() {
    	return __DIR__ . '/../../../web/upload/Offre/';
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function uploadFichier() {
    	if (null === $this->fichier) {
    		return;
    	}
    	if(!$this->id){
    		$this->fichier->move($this->getUploadRootDir(), $this->fichier->getClientOriginalName());
    		$this->setFichier($this->fichier->getClientOriginalName());
    	}elseif(is_object($this->fichier)){
    		$this->fichier->move($this->getUploadRootDir(), $this->fichier->getClientOriginalName());
    		$this->setFichier($this->fichier->getClientOriginalName());
    	}
    }
     
    /**
     * @ORM\PostPersist()
     */
    public function moveFichier()
    {
    	if (null === $this->fichier) {
    		return;
    	}
    	if(!is_dir($this->getUploadRootDir())){
    		mkdir($this->getUploadRootDir());
    	}
    	@copy($this->getTmpUploadRootDir().$this->fichier, $this->getFullFichierPath());
    	@unlink($this->getTmpUploadRootDir().$this->fichier);
    }
    
    /**
     * @ORM\PreRemove()
     */
    public function removeFichier()
    {
    	@unlink($this->getFullFichierPath());
    	@rmdir($this->getUploadRootDir());
    }
    
    /**
     * Set fichier
     *
     * @param string $fichier
     * @return Offre
     */
    public function setFichier($fichier)
    {
    	$this->fichier = $fichier;
    
    	return $this;
    }
    
    /**
     * Get fichier
     *
     * @return string
     */
    public function getFichier()
    {
    	return $this->fichier;
    }
    
    /**
     * Set dateAjout
     *
     * @param \DateTime $dateAjout
     * @return Offre
     */
    public function setDateAjout($dateAjout)
    {
    	$this->dateAjout = $dateAjout;
    
    	return $this;
    }
    
    /**
     * Get dateAjout
     *
     * @return \DateTime
     */
    public function getDateAjout()
    {
    	return $this->dateAjout;
    }
}
