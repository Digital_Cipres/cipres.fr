<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Partenaire
 *
 * @ORM\Table(name="cipres_partenaire")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PartenaireRepository")
 */
class Partenaire
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * 
	 * @Assert\File(maxSize = "3M", 
	 *     mimeTypesMessage = "Veuillez charger une image compatible (PNG, JPG, GIF)", mimeTypes={"image/png", "image/jpeg", "image/gif"})
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;
    
    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function getFullImagePath() {
        return null === $this->image ? null : $this->getUploadRootDir(). $this->image;
    }
 
    protected function getUploadRootDir() {
        return $this->getTmpUploadRootDir().$this->getId()."/";
    }
 
    protected function getTmpUploadRootDir() {
        return __DIR__ . '/../../../web/upload/Partenaire/';
    }
 
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function uploadImage() {
        // the file property can be empty if the field is not required
        if (null === $this->image) {
            return;
        }
        if(!$this->id){
            $this->image->move($this->getUploadRootDir(), $this->image->getClientOriginalName());
            $this->setImage($this->image->getClientOriginalName());
        }elseif(is_object($this->image)){
            $this->image->move($this->getUploadRootDir(), $this->image->getClientOriginalName());
            $this->setImage($this->image->getClientOriginalName());
        }
    }
     
    /**
     * @ORM\PostPersist()
     */
    public function moveImage()
    {
        if (null === $this->image) {
            return;
        }
        if(!is_dir($this->getUploadRootDir())){
            mkdir($this->getUploadRootDir());
        }
        @copy($this->getTmpUploadRootDir().$this->image, $this->getFullImagePath());
        @unlink($this->getTmpUploadRootDir().$this->image);
    }
 
    /**
     * @ORM\PreRemove()
     */
    public function removeImage()
    {
        @unlink($this->getFullImagePath());
        @rmdir($this->getUploadRootDir());
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Partenaire
     */
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }
    
    /**
     * Set titre
     *
     * @param string $titre
     * @return Partenaire
     */
    public function setTitre($titre)
    {
    	$this->titre = $titre;
    
    	return $this;
    }
    
    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
    	return $this->titre;
    }
}
