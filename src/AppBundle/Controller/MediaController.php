<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Media controller.
 *
 * @Route("/")
 */
class MediaController extends Controller
{
	/**
	 * @Route("/presse/mediatheque", name="media_index")
	 * @Method("GET")
     * @Template("Media/index.html.twig")
	 */
	public function indexAction()
	{
		$em = $this->getDoctrine()->getManager();
		 
		$db = $this->getDoctrine()->getManager()->createQueryBuilder()
		->select('media')
		->from('AppBundle\Entity\Media', 'media')
		->addOrderBy('media.categorie', 'DESC')
		->addOrderBy('media.id', 'DESC');
		 
		$medias = $db->getQuery()->getResult();
		 
		$db = $em->createQueryBuilder()
		->select('COUNT(media) as nb')
		->from('AppBundle\Entity\Media', 'media')
		->where('media.categorie = 1');
		 
		$nb1 = $db->getQuery()->getResult();
		 
		$db = $em->createQueryBuilder()
		->select('COUNT(media) as nb')
		->from('AppBundle\Entity\Media', 'media')
		->where('media.categorie = 2');
		 
		$nb2 = $db->getQuery()->getResult();
		 
		$db = $em->createQueryBuilder()
		->select('COUNT(media) as nb')
		->from('AppBundle\Entity\Media', 'media')
		->where('media.categorie = 3');
		 
		$nb3 = $db->getQuery()->getResult();
		 
		return array(
    		'medias'      => $medias,
			'nb1' => $nb1[0]['nb'],
			'nb2' => $nb2[0]['nb'],
			'nb3' => $nb3[0]['nb'],
    	);
	}	
}
