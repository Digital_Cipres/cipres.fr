<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use FOS\UserBundle\Controller\SecurityController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Actualite;
use AppBundle\Form\ActualiteType;

/**
 * @Route("/admin/actualite")
*/
class AdminActualiteController extends Controller
{
    /**
     * @Route("/", name="admin_actualite")
     * @Template("AdminActualite/index.html.twig")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
    	
    	$user = $this->container->get('security.context')->getToken()->getUser();
    	
    	$entities = $em->getRepository('AppBundle:Actualite')->findAll();
        
        return array(
        	"user" => $user,
        	"entities" => $entities,
        );
    }
    
    /**
     * Creates a new Actualite entity.
     *
     * @Route("/create", name="admin_actualite_create")
     * @Method("POST")
     * @Template("AdminActualite/new.html.twig")
     */
    public function createAction(Request $request)
    {
    	$entity = new Actualite();
    	$form = $this->createCreateForm($entity);
    	$form->handleRequest($request);
    	$user = $this->container->get('security.context')->getToken()->getUser();
    
    	if ($form->isValid()) {
    		$em = $this->getDoctrine()->getManager();
    		
    		$champs = $request->get('appbundle_actualitetype');
    		
    		if($champs['slug'] == NULL)
    		{
    			$str = $champs['titre'];
    			$str = htmlentities($str, ENT_NOQUOTES, 'utf-8');
    			// Déspécialise tous les caractères déspécialisables (ex. é->e, œ -> oe, ç->c ou encore ñ->n)
    			$str = preg_replace('/\&([A-Za-z])(?:grave|acute|circ|tilde|uml|ring|cedil)\;/', '$1', $str);
    			$str = preg_replace('/\&([A-Za-z]{2})(?:lig)\;/', '$1', $str);
    			// Supprime tous les caractères non déspécialisables (ex. & = &)
    			$str = preg_replace('/\&([A-Za-z]*)\;/', '', $str);
    			// Remplace tous autres caractères différent d'une lettre, d'un chiffre ou du délimiteur par le délimiteur
    			$str = preg_replace('/[^A-Za-z0-9-]/', '-', $str);
    			// Supprime les doublons de délimiteur
    			$str = preg_replace('/[-]{2,}/', '-', $str);
    			// Convertit la chaine en minuscule
    			$str = strtolower($str);
    			// Suprime les délimiteurs en début et fin de chaine
    			$str = trim($str, '-');
    		
    			$entity->setSlug($str);
    		}
    		
    		$entity->setDate(new \Datetime());
    		
    		$em->persist($entity);
    		$em->flush();
    
    		return $this->redirect($this->generateUrl('admin_actualite'));
    	}
    
    	return array(
    			'entity' => $entity,
    			'form'   => $form->createView(),
    			'user' => $user,
    	);
    }
    
    /**
     * Creates a form to create a Actualite entity.
     *
     * @param Actualite $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Actualite $entity)
    {
    	$form = $this->createForm(new ActualiteType(), $entity, array(
    			'action' => $this->generateUrl('admin_actualite_create'),
    			'method' => 'POST',
    	));
    
    	return $form;
    }
    
    /**
     * Displays a form to create a new Actualite entity.
     *
     * @Route("/new", name="admin_actualite_new")
     * @Method("GET")
     * @Template("AdminActualite/new.html.twig")
     */
    public function newAction()
    {
    	$entity = new Actualite();
    	$form   = $this->createCreateForm($entity);
    	
    	$user = $this->container->get('security.context')->getToken()->getUser();
    
    	return array(
    		'entity' => $entity,
    		'form'   => $form->createView(),
    		"user" => $user,
    	);
    }
    
    /**
     * Finds and displays a Actualite entity.
     *
     * @Route("/{id}/show", name="admin_actualite_show")
     * @Method("GET")
     * @Template("AdminActualite/show.html.twig")
     */
    public function showAction($id)
    {
    	$em = $this->getDoctrine()->getManager();
    
    	$entity = $em->getRepository('AppBundle:Actualite')->find($id);
        
    	if (!$entity) {
    		throw $this->createNotFoundException('Unable to find Actualite entity.');
    	}
    
    	return array(
    		'entity'      => $entity,
    	);
    }
    
    /**
     * Displays a form to edit an existing Actualite entity.
     *
     * @Route("/{id}/edit", name="admin_actualite_edit")
     * @Method("GET")
     * @Template("AdminActualite/edit.html.twig")
     */
    public function editAction($id)
    {
    	$em = $this->getDoctrine()->getManager();
    
    	$entity = $em->getRepository('AppBundle:Actualite')->find($id);
    
    	if (!$entity) {
    		throw $this->createNotFoundException('Unable to find Actualite entity.');
    	}
    
    	$editForm = $this->createEditForm($entity);
    	
    	$user = $this->container->get('security.context')->getToken()->getUser();
    
    	return array(
    		'entity'      => $entity,
    		'edit_form'   => $editForm->createView(),
    		'user' => $user,
    	);
    }
    
    /**
     * Creates a form to edit a Actualite entity.
     *
     * @param Actualite $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Actualite $entity)
    {
    	$form = $this->createForm(new ActualiteType(), $entity, array(
    			'action' => $this->generateUrl('admin_actualite_update', array('id' => $entity->getId())),
    			'method' => 'POST',
    	));
    
    	return $form;
    }
    /**
     * Edits an existing Actualite entity.
     *
     * @Route("/{id}/update", name="admin_actualite_update")
     * @Method("POST")
     * @Template("AdminActualite/edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
    	$em = $this->getDoctrine()->getManager();
    
    	$entity = $em->getRepository('AppBundle:Actualite')->find($id);
    
    	if (!$entity) {
    		throw $this->createNotFoundException('Unable to find Actualite entity.');
    	}
    
    	$tmpImage = $entity->getImage();
    	$tmpSlug = $entity->getSlug();
    	$firstImage = $request->files->get("appbundle_actualitetype");
    	
    	$editForm = $this->createEditForm($entity);
    	$editForm->handleRequest($request);
    
    	if ($editForm->isValid()) 
    	{
    		$champs = $request->get('appbundle_actualitetype');
    		
    		if($firstImage['image'] == null)
    			$entity->setImage($tmpImage);
    		else
    			$entity->setImage($firstImage['image']);
    		
    		if($champs['slug'] != $tmpSlug)
    		{
    			$str = $champs['titre'];
    			$str = htmlentities($str, ENT_NOQUOTES, 'utf-8');
    			$str = preg_replace('/\&([A-Za-z])(?:grave|acute|circ|tilde|uml|ring|cedil)\;/', '$1', $str);
    			$str = preg_replace('/\&([A-Za-z]{2})(?:lig)\;/', '$1', $str);
    			$str = preg_replace('/\&([A-Za-z]*)\;/', '', $str);
    			$str = preg_replace('/[^A-Za-z0-9-]/', '-', $str);
    			$str = preg_replace('/[-]{2,}/', '-', $str);
    			$str = strtolower($str);
    			$str = trim($str, '-');
    		
    			$entity->setSlug($str);
    		
    			/*FAIRE REDIRECTION 301*/
    		}
    		
    		$em->persist($entity);
    		$em->flush();
    
    		return $this->redirect($this->generateUrl('admin_actualite'));
    	}
    	
    	$user = $this->container->get('security.context')->getToken()->getUser();
    
    	return array(
    		'entity'      => $entity,
    		'edit_form'   => $editForm->createView(),
    		'user' => $user,
    	);
    }
    
    /**
     * Deletes a Actualite entity.
     *
     * @Route("/{id}/delete", name="admin_actualite_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, $id)
    {
    	$form = $this->createDeleteForm($id);
    	$form->handleRequest($request);
    
    		$em = $this->getDoctrine()->getManager();
    		$entity = $em->getRepository('AppBundle:Actualite')->find($id);
    
    		if (!$entity) {
    			throw $this->createNotFoundException('Unable to find Actualite entity.');
    		}
    
    		$em->remove($entity);
    		$em->flush();
    
    	return $this->redirect($this->generateUrl('admin_actualite'));
    }
    
    /**
     * Creates a form to delete a Actualite entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
    	return $this->createFormBuilder()
    	->setAction($this->generateUrl('admin_actualite_delete', array('id' => $id)))
    	->setMethod('GET')
    	->add('submit', 'submit', array('label' => 'Delete'))
    	->getForm()
    	;
    }
}
