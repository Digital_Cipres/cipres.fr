<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Newsletter;
use AppBundle\Form\NewsletterType;

/**
 * Newsletter controller.
 *
 * @Route("/newsletter")
 */
class NewsletterController extends Controller
{
    /**
     * Creates a new Newsletter entity.
     *
     * @Route("/create", name="newsletter_create")
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
    	$em = $this->getDoctrine()->getManager();
    	
    	$entity = new Newsletter();
    	
    	$form = $this->createCreateForm($entity);
    	$form->handleRequest($request);
    		
    	if ($form->isValid()) {

    		$entity->setDate(new \Datetime());
    		$em->persist($entity);
    		$em->flush();
    				
    		$this->get('session')->getFlashBag()->add('success_news', 'Votre inscription à la newsletter a bien été prise en compte.');
    		
    		return $this->redirect($this->generateUrl('homepage'));	
    	}
    
    	$this->get('session')->getFlashBag()->add('error_news', 'Un problème est survenu lors de votre inscription.');
    	return $this->redirect($this->generateUrl('homepage'));
    }
    
    /**
     * Creates a form to create a Newsletter entity.
     *
     * @param Newsletter $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Newsletter $entity)
    {
    	$form = $this->createForm(new NewsletterType(), $entity, array(
    		'action' => $this->generateUrl('newsletter_create'),
    		'method' => 'POST',
    	));
    
    	return $form;
    }
}
