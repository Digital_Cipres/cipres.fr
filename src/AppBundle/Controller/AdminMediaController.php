<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use FOS\UserBundle\Controller\SecurityController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Media;
use AppBundle\Form\MediaType;

/**
 * @Route("/admin/media")
*/
class AdminMediaController extends Controller
{
    /**
     * @Route("/", name="admin_media")
     * @Template("AdminMedia/index.html.twig")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
    	
    	$user = $this->container->get('security.context')->getToken()->getUser();
    	
    	$entities = $em->getRepository('AppBundle:Media')->findAll();
        
        return array(
        	"user" => $user,
        	"entities" => $entities,
        );
    }
    
    /**
     * Creates a new Media entity.
     *
     * @Route("/create", name="admin_media_create")
     * @Method("POST")
     * @Template("AdminMedia/new.html.twig")
     */
    public function createAction(Request $request)
    {
    	$entity = new Media();
    	$form = $this->createCreateForm($entity);
    	$form->handleRequest($request);
    	$user = $this->container->get('security.context')->getToken()->getUser();
    
    	if ($form->isValid()) {
    		$em = $this->getDoctrine()->getManager();
    		
    		$em->persist($entity);
    		$em->flush();
    
    		return $this->redirect($this->generateUrl('admin_media'));
    	}
    
    	return array(
    			'entity' => $entity,
    			'form'   => $form->createView(),
    			'user' => $user,
    	);
    }
    
    /**
     * Creates a form to create a Media entity.
     *
     * @param Media $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Media $entity)
    {
    	$form = $this->createForm(new MediaType(), $entity, array(
    			'action' => $this->generateUrl('admin_media_create'),
    			'method' => 'POST',
    	));
    
    	return $form;
    }
    
    /**
     * Displays a form to create a new Media entity.
     *
     * @Route("/new", name="admin_media_new")
     * @Method("GET")
     * @Template("AdminMedia/new.html.twig")
     */
    public function newAction()
    {
    	$entity = new Media();
    	$form   = $this->createCreateForm($entity);
    	
    	$user = $this->container->get('security.context')->getToken()->getUser();
    
    	return array(
    		'entity' => $entity,
    		'form'   => $form->createView(),
    		"user" => $user,
    	);
    }
    
    /**
     * Finds and displays a Media entity.
     *
     * @Route("/{id}/show", name="admin_media_show")
     * @Method("GET")
     * @Template("AdminMedia/show.html.twig")
     */
    public function showAction($id)
    {
    	$em = $this->getDoctrine()->getManager();
    
    	$entity = $em->getRepository('AppBundle:Media')->find($id);
        
    	if (!$entity) {
    		throw $this->createNotFoundException('Unable to find Media entity.');
    	}
    
    	return array(
    		'entity'      => $entity,
    	);
    }
    
    /**
     * Displays a form to edit an existing Media entity.
     *
     * @Route("/{id}/edit", name="admin_media_edit")
     * @Method("GET")
     * @Template("AdminMedia/edit.html.twig")
     */
    public function editAction($id)
    {
    	$em = $this->getDoctrine()->getManager();
    
    	$entity = $em->getRepository('AppBundle:Media')->find($id);
    
    	if (!$entity) {
    		throw $this->createNotFoundException('Unable to find Media entity.');
    	}
    
    	$editForm = $this->createEditForm($entity);
    	
    	$user = $this->container->get('security.context')->getToken()->getUser();
    
    	return array(
    		'entity'      => $entity,
    		'edit_form'   => $editForm->createView(),
    		'user' => $user,
    	);
    }
    
    /**
     * Creates a form to edit a Media entity.
     *
     * @param Media $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Media $entity)
    {
    	$form = $this->createForm(new MediaType(), $entity, array(
    			'action' => $this->generateUrl('admin_media_update', array('id' => $entity->getId())),
    			'method' => 'POST',
    	));
    
    	return $form;
    }
    /**
     * Edits an existing Media entity.
     *
     * @Route("/{id}/update", name="admin_media_update")
     * @Method("POST")
     * @Template("AdminMedia/edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
    	$em = $this->getDoctrine()->getManager();
    
    	$entity = $em->getRepository('AppBundle:Media')->find($id);
    
    	if (!$entity) {
    		throw $this->createNotFoundException('Unable to find Media entity.');
    	}
    
    	$tmpImage = $entity->getImage();
    	$tmpFile = $entity->getFichier();
    	$firstImage = $request->files->get("appbundle_mediatype");
    	
    	$editForm = $this->createEditForm($entity);
    	$editForm->handleRequest($request);
    
    	if ($editForm->isValid()) 
    	{
    		if($firstImage['image'] == null)
    			$entity->setImage($tmpImage);
    		else
    			$entity->setImage($firstImage['image']);
    		
    		if($firstImage['fichier'] == null)
    			$entity->setFichier($tmpFile);
    		else
    			$entity->setFichier($firstImage['fichier']);
    		
    		$em->persist($entity);
    		$em->flush();
    
    		return $this->redirect($this->generateUrl('admin_media'));
    	}
    	
    	$user = $this->container->get('security.context')->getToken()->getUser();
    
    	return array(
    		'entity'      => $entity,
    		'edit_form'   => $editForm->createView(),
    		'user' => $user,
    	);
    }
    
    /**
     * Deletes a Media entity.
     *
     * @Route("/{id}/delete", name="admin_media_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, $id)
    {
    	$form = $this->createDeleteForm($id);
    	$form->handleRequest($request);
    
    		$em = $this->getDoctrine()->getManager();
    		$entity = $em->getRepository('AppBundle:Media')->find($id);
    
    		if (!$entity) {
    			throw $this->createNotFoundException('Unable to find Media entity.');
    		}
    
    		$em->remove($entity);
    		$em->flush();
    
    	return $this->redirect($this->generateUrl('admin_media'));
    }
    
    /**
     * Creates a form to delete a Media entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
    	return $this->createFormBuilder()
    	->setAction($this->generateUrl('admin_media_delete', array('id' => $id)))
    	->setMethod('GET')
    	->add('submit', 'submit', array('label' => 'Delete'))
    	->getForm()
    	;
    }
}
