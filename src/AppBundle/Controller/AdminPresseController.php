<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use FOS\UserBundle\Controller\SecurityController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Presse;
use AppBundle\Form\PresseType;

/**
 * @Route("/admin/presse")
*/
class AdminPresseController extends Controller
{
    /**
     * @Route("/", name="admin_presse")
     * @Template("AdminPresse/index.html.twig")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
    	
    	$user = $this->container->get('security.context')->getToken()->getUser();
    	
    	$entities = $em->getRepository('AppBundle:Presse')->findAll();
        
        return array(
        	"user" => $user,
        	"entities" => $entities,
        );
    }
    
    /**
     * Creates a new Presse entity.
     *
     * @Route("/create", name="admin_presse_create")
     * @Method("POST")
     * @Template("AdminPresse/new.html.twig")
     */
    public function createAction(Request $request)
    {
    	$entity = new Presse();
    	$form = $this->createCreateForm($entity);
    	$form->handleRequest($request);
    	$user = $this->container->get('security.context')->getToken()->getUser();
    
    	if ($form->isValid()) {
    		$em = $this->getDoctrine()->getManager();
    			
    		$em->persist($entity);
    		$em->flush();
    
    		return $this->redirect($this->generateUrl('admin_presse'));
    	}
    
    	return array(
    			'entity' => $entity,
    			'form'   => $form->createView(),
    			'user' => $user,
    	);
    }
    
    /**
     * Creates a form to create a Presse entity.
     *
     * @param Presse $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Presse $entity)
    {
    	$form = $this->createForm(new PresseType(), $entity, array(
    			'action' => $this->generateUrl('admin_presse_create'),
    			'method' => 'POST',
    	));
    
    	return $form;
    }
    
    /**
     * Displays a form to create a new Presse entity.
     *
     * @Route("/new", name="admin_presse_new")
     * @Method("GET")
     * @Template("AdminPresse/new.html.twig")
     */
    public function newAction()
    {
    	$entity = new Presse();
    	$form   = $this->createCreateForm($entity);
    	
    	$user = $this->container->get('security.context')->getToken()->getUser();
    
    	return array(
    		'entity' => $entity,
    		'form'   => $form->createView(),
    		"user" => $user,
    	);
    }
    
    /**
     * Finds and displays a Presse entity.
     *
     * @Route("/{id}/show", name="admin_presse_show")
     * @Method("GET")
     * @Template("AdminPresse/show.html.twig")
     */
    public function showAction($id)
    {
    	$em = $this->getDoctrine()->getManager();
    
    	$entity = $em->getRepository('AppBundle:Presse')->find($id);
        
    	if (!$entity) {
    		throw $this->createNotFoundException('Unable to find Presse entity.');
    	}
    
    	return array(
    		'entity'      => $entity,
    	);
    }
    
    /**
     * Displays a form to edit an existing Presse entity.
     *
     * @Route("/{id}/edit", name="admin_presse_edit")
     * @Method("GET")
     * @Template("AdminPresse/edit.html.twig")
     */
    public function editAction($id)
    {
    	$em = $this->getDoctrine()->getManager();
    
    	$entity = $em->getRepository('AppBundle:Presse')->find($id);
    
    	if (!$entity) {
    		throw $this->createNotFoundException('Unable to find Presse entity.');
    	}
    
    	$editForm = $this->createEditForm($entity);
    	
    	$user = $this->container->get('security.context')->getToken()->getUser();
    
    	return array(
    		'entity'      => $entity,
    		'edit_form'   => $editForm->createView(),
    		'user' => $user,
    	);
    }
    
    /**
     * Creates a form to edit a Presse entity.
     *
     * @param Presse $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Presse $entity)
    {
    	$form = $this->createForm(new PresseType(), $entity, array(
    			'action' => $this->generateUrl('admin_presse_update', array('id' => $entity->getId())),
    			'method' => 'POST',
    	));
    
    	return $form;
    }
    /**
     * Edits an existing Presse entity.
     *
     * @Route("/{id}/update", name="admin_presse_update")
     * @Method("POST")
     * @Template("AdminPresse/edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
    	$em = $this->getDoctrine()->getManager();
    
    	$entity = $em->getRepository('AppBundle:Presse')->find($id);
    
    	if (!$entity) {
    		throw $this->createNotFoundException('Unable to find Presse entity.');
    	}
    	
    	$tmpImage = $entity->getImage();
    	$tmpFichier = $entity->getFichier();
    	$firstImage = $request->files->get("appbundle_pressetype");
    
    	$editForm = $this->createEditForm($entity);
    	$editForm->handleRequest($request);
    
    	if ($editForm->isValid()) {
    		
    		if($firstImage['image'] == null)
    			$entity->setImage($tmpImage);
    		else
    			$entity->setImage($firstImage['image']);
    		
    		if($firstImage['fichier'] == null)
    			$entity->setFichier($tmpFichier);
    		else
    			$entity->setFichier($firstImage['fichier']);
    		
    		$em->flush();
    
    		return $this->redirect($this->generateUrl('admin_presse'));
    	}
    	
    	$user = $this->container->get('security.context')->getToken()->getUser();
    
    	return array(
    		'entity'      => $entity,
    		'edit_form'   => $editForm->createView(),
    		'user' => $user,
    	);
    }
    
    /**
     * Deletes a Presse entity.
     *
     * @Route("/{id}/delete", name="admin_presse_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, $id)
    {
    	$form = $this->createDeleteForm($id);
    	$form->handleRequest($request);
    
    		$em = $this->getDoctrine()->getManager();
    		$entity = $em->getRepository('AppBundle:Presse')->find($id);
    
    		if (!$entity) {
    			throw $this->createNotFoundException('Unable to find Presse entity.');
    		}
    
    		$em->remove($entity);
    		$em->flush();
    
    	return $this->redirect($this->generateUrl('admin_presse'));
    }
    
    /**
     * Creates a form to delete a Presse entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
    	return $this->createFormBuilder()
    	->setAction($this->generateUrl('admin_presse_delete', array('id' => $id)))
    	->setMethod('GET')
    	->add('submit', 'submit', array('label' => 'Delete'))
    	->getForm()
    	;
    }
}
