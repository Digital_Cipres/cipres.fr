<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class NonTrouveController extends Controller
{
    /**
     * @Route("/404/", name="non_trouve")
     */
    public function show404Action()
    {

        return $this->render('404.html.twig', array(

        ));

    }
}
