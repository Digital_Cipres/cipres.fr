<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use FOS\UserBundle\Controller\SecurityController as BaseController;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin")
*/
class AdminController extends BaseController
{
    /**
     * @Route("/", name="admin")
     * @Template("Admin/index.html.twig")
     */
    public function indexAction()
    {
        $em = $this->container->get('doctrine');
        
        $user = $this->container->get('security.context')->getToken()->getUser();
        
        return array(
        	"user" => $user,
        );
    }

    /**
     * @Route("/login", name="admin_login")
     * @Template("User/login.html.twig")
     */
    public function loginAction(Request $request)
    {
        // @FIXME : Forcing URL Admin si Auth OK
        $request = $this->container->get('request');
        $session = $request->getSession();
        $route = $this->container->get('router')->generate('admin', array(), true);
        $session->set('_security.main.target_path',$route);

        return parent::loginAction($request);
    }

    protected function renderLogin(array $data)
    {
        return $data;
    }

    /**
     * @Route("/login_check", name="admin_login_check")
     */
    public function checkAction()
    {
        return parent::checkAction();
    }

    /**
     * @Route("/logout", name="admin_logout")
     */
    public function logoutAction() {
        return parent::logoutAction();
    }

    /**
     * @Route("/logged", name="admin_logged")
     * @Template()
     */
    public function loggedAction() {
        return array();
    }
}
