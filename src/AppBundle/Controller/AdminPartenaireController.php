<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use FOS\UserBundle\Controller\SecurityController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Partenaire;
use AppBundle\Form\PartenaireType;

/**
 * @Route("/admin/partenaire")
*/
class AdminPartenaireController extends Controller
{
    /**
     * @Route("/", name="admin_partenaire")
     * @Template("AdminPartenaire/index.html.twig")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
    	
    	$user = $this->container->get('security.context')->getToken()->getUser();
    	
    	$entities = $em->getRepository('AppBundle:Partenaire')->findAll();
        
        return array(
        	"user" => $user,
        	"entities" => $entities,
        );
    }
    
    /**
     * Creates a new Partenaire entity.
     *
     * @Route("/create", name="admin_partenaire_create")
     * @Method("POST")
     * @Template("AdminPartenaire/new.html.twig")
     */
    public function createAction(Request $request)
    {
    	$entity = new Partenaire();
    	$form = $this->createCreateForm($entity);
    	$form->handleRequest($request);
    	$user = $this->container->get('security.context')->getToken()->getUser();
    
    	if ($form->isValid()) {
    		$em = $this->getDoctrine()->getManager();
    			
    		$em->persist($entity);
    		$em->flush();
    
    		return $this->redirect($this->generateUrl('admin_partenaire'));
    	}
    
    	return array(
    			'entity' => $entity,
    			'form'   => $form->createView(),
    			'user' => $user,
    	);
    }
    
    /**
     * Creates a form to create a Partenaire entity.
     *
     * @param Partenaire $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Partenaire $entity)
    {
    	$form = $this->createForm(new PartenaireType(), $entity, array(
    			'action' => $this->generateUrl('admin_partenaire_create'),
    			'method' => 'POST',
    	));
    
    	return $form;
    }
    
    /**
     * Displays a form to create a new Partenaire entity.
     *
     * @Route("/new", name="admin_partenaire_new")
     * @Method("GET")
     * @Template("AdminPartenaire/new.html.twig")
     */
    public function newAction()
    {
    	$entity = new Partenaire();
    	$form   = $this->createCreateForm($entity);
    	
    	$user = $this->container->get('security.context')->getToken()->getUser();
    
    	return array(
    		'entity' => $entity,
    		'form'   => $form->createView(),
    		"user" => $user,
    	);
    }
    
    /**
     * Finds and displays a Partenaire entity.
     *
     * @Route("/{id}/show", name="admin_partenaire_show")
     * @Method("GET")
     * @Template("AdminPartenaire/show.html.twig")
     */
    public function showAction($id)
    {
    	$em = $this->getDoctrine()->getManager();
    
    	$entity = $em->getRepository('AppBundle:Partenaire')->find($id);
        
    	if (!$entity) {
    		throw $this->createNotFoundException('Unable to find Partenaire entity.');
    	}
    
    	return array(
    		'entity'      => $entity,
    	);
    }
    
    /**
     * Displays a form to edit an existing Partenaire entity.
     *
     * @Route("/{id}/edit", name="admin_partenaire_edit")
     * @Method("GET")
     * @Template("AdminPartenaire/edit.html.twig")
     */
    public function editAction($id)
    {
    	$em = $this->getDoctrine()->getManager();
    
    	$entity = $em->getRepository('AppBundle:Partenaire')->find($id);
    
    	if (!$entity) {
    		throw $this->createNotFoundException('Unable to find Partenaire entity.');
    	}
    
    	$editForm = $this->createEditForm($entity);
    	
    	$user = $this->container->get('security.context')->getToken()->getUser();
    
    	return array(
    		'entity'      => $entity,
    		'edit_form'   => $editForm->createView(),
    		'user' => $user,
    	);
    }
    
    /**
     * Creates a form to edit a Partenaire entity.
     *
     * @param Partenaire $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Partenaire $entity)
    {
    	$form = $this->createForm(new PartenaireType(), $entity, array(
    			'action' => $this->generateUrl('admin_partenaire_update', array('id' => $entity->getId())),
    			'method' => 'POST',
    	));
    
    	return $form;
    }
    /**
     * Edits an existing Partenaire entity.
     *
     * @Route("/{id}/update", name="admin_partenaire_update")
     * @Method("POST")
     * @Template("AdminPartenaire/edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
    	$em = $this->getDoctrine()->getManager();
    
    	$entity = $em->getRepository('AppBundle:Partenaire')->find($id);
    
    	if (!$entity) {
    		throw $this->createNotFoundException('Unable to find Partenaire entity.');
    	}
    	
    	$tmpImage = $entity->getImage();
    	$firstImage = $request->files->get("appbundle_partenairetype");
    
    	$editForm = $this->createEditForm($entity);
    	$editForm->handleRequest($request);
    
    	if ($editForm->isValid()) {
    		
    		if($firstImage['image'] == null)
    			$entity->setImage($tmpImage);
    		else
    			$entity->setImage($firstImage['image']);
    		
    		$em->flush();
    
    		return $this->redirect($this->generateUrl('admin_partenaire'));
    	}
    	
    	$user = $this->container->get('security.context')->getToken()->getUser();
    
    	return array(
    		'entity'      => $entity,
    		'edit_form'   => $editForm->createView(),
    		'user' => $user,
    	);
    }
    
    /**
     * Deletes a Partenaire entity.
     *
     * @Route("/{id}/delete", name="admin_partenaire_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, $id)
    {
    	$form = $this->createDeleteForm($id);
    	$form->handleRequest($request);
    
    		$em = $this->getDoctrine()->getManager();
    		$entity = $em->getRepository('AppBundle:Partenaire')->find($id);
    
    		if (!$entity) {
    			throw $this->createNotFoundException('Unable to find Partenaire entity.');
    		}
    
    		$em->remove($entity);
    		$em->flush();
    
    	return $this->redirect($this->generateUrl('admin_partenaire'));
    }
    
    /**
     * Creates a form to delete a Partenaire entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
    	return $this->createFormBuilder()
    	->setAction($this->generateUrl('admin_partenaire_delete', array('id' => $id)))
    	->setMethod('GET')
    	->add('submit', 'submit', array('label' => 'Delete'))
    	->getForm()
    	;
    }
}
