<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use FOS\UserBundle\Controller\SecurityController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Slide;
use AppBundle\Form\SlideType;

/**
 * @Route("/admin/slide")
*/
class AdminSlideController extends Controller
{
    /**
     * @Route("/", name="admin_slide")
     * @Template("AdminSlide/index.html.twig")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
    	
    	$user = $this->container->get('security.context')->getToken()->getUser();
    	
    	$entities = $em->getRepository('AppBundle:Slide')->findAll();
        
        return array(
        	"user" => $user,
        	"entities" => $entities,
        );
    }
    
    /**
     * Creates a new Slide entity.
     *
     * @Route("/create", name="admin_slide_create")
     * @Method("POST")
     * @Template("AdminSlide/new.html.twig")
     */
    public function createAction(Request $request)
    {
    	$entity = new Slide();
    	$form = $this->createCreateForm($entity);
    	$form->handleRequest($request);
    	$user = $this->container->get('security.context')->getToken()->getUser();
    
    	if ($form->isValid()) {
    		$em = $this->getDoctrine()->getManager();
    			
    		$em->persist($entity);
    		$em->flush();
    
    		return $this->redirect($this->generateUrl('admin_slide'));
    	}
    
    	return array(
    			'entity' => $entity,
    			'form'   => $form->createView(),
    			'user' => $user,
    	);
    }
    
    /**
     * Creates a form to create a Slide entity.
     *
     * @param Slide $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Slide $entity)
    {
    	$form = $this->createForm(new SlideType(), $entity, array(
    			'action' => $this->generateUrl('admin_slide_create'),
    			'method' => 'POST',
    	));
    
    	return $form;
    }
    
    /**
     * Displays a form to create a new Slide entity.
     *
     * @Route("/new", name="admin_slide_new")
     * @Method("GET")
     * @Template("AdminSlide/new.html.twig")
     */
    public function newAction()
    {
    	$entity = new Slide();
    	$form   = $this->createCreateForm($entity);
    	
    	$user = $this->container->get('security.context')->getToken()->getUser();
    
    	return array(
    		'entity' => $entity,
    		'form'   => $form->createView(),
    		"user" => $user,
    	);
    }
    
    /**
     * Finds and displays a Slide entity.
     *
     * @Route("/{id}/show", name="admin_slide_show")
     * @Method("GET")
     * @Template("AdminSlide/show.html.twig")
     */
    public function showAction($id)
    {
    	$em = $this->getDoctrine()->getManager();
    
    	$entity = $em->getRepository('AppBundle:Slide')->find($id);
        
    	if (!$entity) {
    		throw $this->createNotFoundException('Unable to find Slide entity.');
    	}
    
    	return array(
    		'entity'      => $entity,
    	);
    }
    
    /**
     * Displays a form to edit an existing Slide entity.
     *
     * @Route("/{id}/edit", name="admin_slide_edit")
     * @Method("GET")
     * @Template("AdminSlide/edit.html.twig")
     */
    public function editAction($id)
    {
    	$em = $this->getDoctrine()->getManager();
    
    	$entity = $em->getRepository('AppBundle:Slide')->find($id);
    
    	if (!$entity) {
    		throw $this->createNotFoundException('Unable to find Slide entity.');
    	}
    
    	$editForm = $this->createEditForm($entity);
    	
    	$user = $this->container->get('security.context')->getToken()->getUser();
    
    	return array(
    		'entity'      => $entity,
    		'edit_form'   => $editForm->createView(),
    		'user' => $user,
    	);
    }
    
    /**
     * Creates a form to edit a Slide entity.
     *
     * @param Slide $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Slide $entity)
    {
    	$form = $this->createForm(new SlideType(), $entity, array(
    			'action' => $this->generateUrl('admin_slide_update', array('id' => $entity->getId())),
    			'method' => 'POST',
    	));
    
    	return $form;
    }
    /**
     * Edits an existing Slide entity.
     *
     * @Route("/{id}/update", name="admin_slide_update")
     * @Method("POST")
     * @Template("AdminSlide/edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
    	$em = $this->getDoctrine()->getManager();
    
    	$entity = $em->getRepository('AppBundle:Slide')->find($id);
    
    	if (!$entity) {
    		throw $this->createNotFoundException('Unable to find Slide entity.');
    	}
    
    	$tmpImage = $entity->getImage();
    	$firstImage = $request->files->get("appbundle_slidetype");
    	
    	$editForm = $this->createEditForm($entity);
    	$editForm->handleRequest($request);
    
    	if ($editForm->isValid()) {
    		
    		if($firstImage['image'] == null)
    			$entity->setImage($tmpImage);
    		else
    			$entity->setImage($firstImage['image']);
    		
    		$em->flush();
    
    		return $this->redirect($this->generateUrl('admin_slide'));
    	}
    	
    	$user = $this->container->get('security.context')->getToken()->getUser();
    
    	return array(
    		'entity'      => $entity,
    		'edit_form'   => $editForm->createView(),
    		'user' => $user,
    	);
    }
    
    /**
     * Deletes a Slide entity.
     *
     * @Route("/{id}/delete", name="admin_slide_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, $id)
    {
    	$form = $this->createDeleteForm($id);
    	$form->handleRequest($request);
    
    	
    		$em = $this->getDoctrine()->getManager();
    		$entity = $em->getRepository('AppBundle:Slide')->find($id);
    
    		if (!$entity) {
    			throw $this->createNotFoundException('Unable to find Slide entity.');
    		}
    
    		$em->remove($entity);
    		$em->flush();
    	
    
    	return $this->redirect($this->generateUrl('admin_slide'));
    }
    
    /**
     * Creates a form to delete a Slide entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
    	return $this->createFormBuilder()
    	->setAction($this->generateUrl('admin_slide_delete', array('id' => $id)))
    	->setMethod('GET')
    	->add('submit', 'submit', array('label' => 'Delete'))
    	->getForm()
    	;
    }
}
