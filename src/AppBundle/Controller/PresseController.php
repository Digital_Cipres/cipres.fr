<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Presse controller.
 *
 * @Route("/")
 */
class PresseController extends Controller
{
    /**
     * @Route("/Espace-presse/Revue-de-presse", name="presse_categorie")
     */
    public function categorieAction()
    {
    	$em = $this->getDoctrine()->getManager();
    	
    	$type = 1 ;
    	
    	$db = $this->getDoctrine()->getManager()->createQueryBuilder()
    	->select('presse')
    	->from('AppBundle\Entity\Presse', 'presse')
    	->where('presse.categorie = 1')
    	->orderBy('presse.id', 'DESC');
    	
    	$presses = $db->getQuery()->getResult();

        return $this->render('Presse/categorie.html.twig', array(
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
        	'presses' => $presses,
        	'type' => $type,
        	'actif' => 7,
        ));
    }
    
    /**
     * @Route("/presse/communique", name="presse_communique_categorie")
     */
    public function communiqueCategorieAction()
    {
    	$em = $this->getDoctrine()->getManager();
    	 
    	$type = 2 ;
    	 
    	$db = $this->getDoctrine()->getManager()->createQueryBuilder()
    	->select('presse')
    	->from('AppBundle\Entity\Presse', 'presse')
    	->where('presse.categorie = 2')
    	->orderBy('presse.id', 'DESC');
    	 
    	$presses = $db->getQuery()->getResult();
    
    	return $this->render('Presse/categorie.html.twig', array(
    			'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
    			'presses' => $presses,
    			'type' => $type,
    			'actif' => 8,
    	));
    }
    
    /**
     * @Route("/presse/contact/", name="presse_contact")
     */
    public function contactAction($slug = "presse/contact")
    {
    	$em = $this->getDoctrine()->getManager();
    
    	$entity = $em->getRepository('AppBundle:Page')->findBySlug($slug);
    
    	if (!$entity) {
    		throw $this->createNotFoundException('Unable to find Page entity.');
    	}
    
    	return $this->render('page/show.html.twig', array(
    			'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
    			'slug' => $slug,
    			'entity' => $entity[0],
    			'actif' => 6,
    	));
    }
    
    /**
     * @Route("/Espace-presse/{id}/show", name="presse_show")
     * @Method("GET")
     * @Template("Presse/show.html.twig")
     */
    public function showAction($id = 0)
    {
    	$em = $this->getDoctrine()->getManager();
    
    	$entity = $em->getRepository('AppBundle:Presse')->find($id);
    
    	if (!$entity) {
    		throw $this->createNotFoundException('Unable to find Presse entity.');
    	}
    
    	return array(
    			'entity'      => $entity,
    	);
    }
}
