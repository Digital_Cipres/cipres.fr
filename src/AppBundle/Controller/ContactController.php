<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Contact;
use AppBundle\Form\ContactType;

/**
 * Contact controller.
 *
 * @Route("/")
 */
class ContactController extends Controller
{
    /**
     * @Route("/contact",name="contact_index")
     */
    public function indexAction()
    {
    	$em = $this->getDoctrine()->getManager();
    	
    	$entity = new Contact();
    	$form   = $this->createCreateForm($entity);

        return $this->render('Contact/index.html.twig', array(
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
        	'form'   => $form->createView(),
        	'actif' => 9,
        ));
    }
    
    /**
     * Creates a form to create a Contact entity.
     *
     * @param Contact $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Contact $entity)
    {
    	$form = $this->createForm(new ContactType(), $entity, array(
    			'action' => $this->generateUrl('contact_create'),
    			'method' => 'POST',
    	));
    
    	return $form;
    }
    
    /**
     * Creates a new Contact entity.
     *
     * @Route("/contact/create", name="contact_create")
     * @Method("POST")
     * @Template("Contact/index.html.twig")
     */
    public function createAction(Request $request)
    {
    	$em = $this->getDoctrine()->getManager();
    	
    	$entity = new Contact();
    	
    	if($request->get('form_offre') != NULL)
    	{
    		$champs = $request->get('form_offre');
    		$entity->setType(1);
    		
    		$offre = $em->getRepository('AppBundle:Offre')->find($champs['id']);
    		
    		if($champs['nom'] != NULL)
    			$entity->setNom($champs['nom']);
    		
    		if($champs['prenom'] != NULL)
    			$entity->setPrenom($champs['prenom']);
    		
    		if($champs['mail'] != NULL)
    			$entity->setEmail($champs['mail']);
    		
    		if($champs['tel'] != NULL)
    			$entity->setTelephone($champs['tel']);
    		
    		if($champs['message'] != NULL)
    			$entity->setMessage($champs['message']);
    		
    		$entity->setSource('');
    		
    		$em->persist($entity);
    		$em->flush();
    		
    		if($_FILES['cv']['name'] != NULL)
    		{
    			$entity->setFichier($_FILES['cv']['name']);
    		
    			mkdir(getcwd().'/upload/Contact/'.$entity->getId(), 0775);
    			$uploaddir = getcwd().'/upload/Contact/'.$entity->getId().'/';
    		
	    		$uploadfile = $uploaddir . basename($_FILES['cv']['name']);
	    		
	    		if (move_uploaded_file($_FILES['cv']['tmp_name'], $uploadfile)) 
	    		{
	    			echo "Le fichier est valide, et a été téléchargé avec succès. Voici plus d'informations :\n";
	    		} 
	    		
	    		$em->persist($entity);
	    		$em->flush();
    		}
    		
    		if($_FILES['lettre']['name'] != NULL)
    		{
    			$entity->setFichier2($_FILES['lettre']['name']);
    		
    			if($entity->getFichier() == NULL)
    				mkdir(getcwd().'/upload/Contact/'.$entity->getId(), 0775);
    			
    			$uploaddir = getcwd().'/upload/Contact/'.$entity->getId().'/';
    		
    			$uploadfile = $uploaddir . basename($_FILES['lettre']['name']);
    			 
    			if (move_uploaded_file($_FILES['lettre']['tmp_name'], $uploadfile))
    			{
    				echo "Le fichier est valide, et a été téléchargé avec succès. Voici plus d'informations :\n";
    			}
    			 
    			$em->persist($entity);
    			$em->flush();
    		}
    		
    		$this->get('session')->getFlashBag()->add('success', 'Votre candidature nous a bien été envoyée.');
    		
    		$templateFile = "Contact/email1.html.twig";
    			
    		$body = $this->renderView($templateFile, array('entity' => $entity, 'offre' => $offre));
    		$message = \Swift_Message::newInstance()
    		->setSubject("[cipres.fr] Candidature au poste de ".$offre->getTitre())
    		->setFrom(array('noreply@cipres.fr' => 'Cipres'))
    		->setTo('recrutement@cipres.fr')
    		->setBcc(array('contact@cipres.fr'))
            // ->setTo('gperreau@cipres.fr')
            // ->setBcc(array('ncueff@cipres.fr'))
    		->setBody($body, 'text/html');
    		
    		if($entity->getFichier() != NULL)
    			$message->attach(\Swift_Attachment::fromPath(getcwd().'/upload/Contact/'.$entity->getId().'/'.$entity->getFichier()));
    		if($entity->getFichier2() != NULL)
    			$message->attach(\Swift_Attachment::fromPath(getcwd().'/upload/Contact/'.$entity->getId().'/'.$entity->getFichier2()));
    		 
    		$this->container->get('mailer')->send($message);
    		
    		return $this->redirect($this->generateUrl('offre_index'));
    	}
    	elseif($request->get('form_courtier') != NULL)
    	{
    		$champs = $request->get('form_courtier');
    		$entity->setType(5);
    		
    		if($champs['nomCabinet'] != NULL)
    			$entity->setNomCabinet($champs['nomCabinet']);
    		
    		if($champs['adresse'] != NULL)
    			$entity->setAdresse($champs['adresse']);
    		
    		if($champs['codePostal'] != NULL)
    			$entity->setCodePostal($champs['codePostal']);
    		
    		if($champs['ville'] != NULL)
    			$entity->setVille($champs['ville']);
    		
    		if($champs['emailCabinet'] != NULL)
    			$entity->setEmailCabinet($champs['emailCabinet']);
    		
    		if($champs['telephoneCabinet'] != NULL)
    			$entity->setTelephoneCabinet($champs['telephoneCabinet']);
    		
    		if($champs['catOrias'] != NULL)
    			$entity->setCatOrias($champs['catOrias']);
    		
    		if($champs['statut'] != NULL)
    			$entity->setStatut($champs['statut']);
    		
    		if($champs['nom'] != NULL)
    			$entity->setNom($champs['nom']);
    		
    		if($champs['prenom'] != NULL)
    			$entity->setPrenom($champs['prenom']);
    		
    		if($champs['email'] != NULL)
    			$entity->setEmail($champs['email']);
    		
    		if($champs['telephone'] != NULL)
    			$entity->setTelephone($champs['telephone']);
    		
    		if($champs['nomInterlocuteur'] != NULL)
    			$entity->setNomInterlocuteur($champs['nomInterlocuteur']);
    		
    		if($champs['prenomInterlocuteur'] != NULL)
    			$entity->setPrenomInterlocuteur($champs['prenomInterlocuteur']);
    		
    		if($champs['telephoneInterlocuteur'] != NULL)
    			$entity->setTelephoneInterlocuteur($champs['telephoneInterlocuteur']);
    		
    		if($champs['emailInterlocuteur'] != NULL)
    			$entity->setEmailInterlocuteur($champs['emailInterlocuteur']);
    		
    		if($champs['message'] != NULL)
    			$entity->setMessage($champs['message']);
    		
    		$entity->setSource('');
    		
    		$em->persist($entity);
    		$em->flush();
    		
    		if($_FILES['fichier']['name'] != NULL)
    		{
    			$entity->setFichier($_FILES['fichier']['name']);
    		
    			mkdir(getcwd().'/upload/Contact/'.$entity->getId(), 0775);
    			$uploaddir = getcwd().'/upload/Contact/'.$entity->getId().'/';
    		
	    		$uploadfile = $uploaddir . basename($_FILES['fichier']['name']);
	    		
	    		if (move_uploaded_file($_FILES['fichier']['tmp_name'], $uploadfile)) 
	    		{
	    			echo "Le fichier est valide, et a été téléchargé avec succès. Voici plus d'informations :\n";
	    		} 
	    		
	    		$em->persist($entity);
	    		$em->flush();
    		}
    		
    		if($_FILES['fichier2']['name'] != NULL)
    		{
    			$entity->setFichier2($_FILES['fichier2']['name']);
    		
    			if($entity->getFichier() == NULL)
    				mkdir(getcwd().'/upload/Contact/'.$entity->getId(), 0775);
    			
    			$uploaddir = getcwd().'/upload/Contact/'.$entity->getId().'/';
    		
    			$uploadfile = $uploaddir . basename($_FILES['fichier2']['name']);
    			 
    			if (move_uploaded_file($_FILES['fichier2']['tmp_name'], $uploadfile))
    			{
    				echo "Le fichier est valide, et a été téléchargé avec succès. Voici plus d'informations :\n";
    			}
    			 
    			$em->persist($entity);
    			$em->flush();
    		}
    		
    		if($_FILES['fichier3']['name'] != NULL)
    		{
    			$entity->setFichier3($_FILES['fichier3']['name']);
    		
    			if($entity->getFichier() == NULL && $entity->getFichier2() != NULL)
    				mkdir(getcwd().'/upload/Contact/'.$entity->getId(), 0775);
    			 
    			$uploaddir = getcwd().'/upload/Contact/'.$entity->getId().'/';
    		
    			$uploadfile = $uploaddir . basename($_FILES['fichier3']['name']);
    		
    			if (move_uploaded_file($_FILES['fichier3']['tmp_name'], $uploadfile))
    			{
    				echo "Le fichier est valide, et a été téléchargé avec succès. Voici plus d'informations :\n";
    			}
    		
    			$em->persist($entity);
    			$em->flush();
    		}
    		
    		$this->get('session')->getFlashBag()->add('success', 'Nous avons bien enregistré votre demande.');
    		
    		return $this->redirect($this->generateUrl('landing_courtier'));
    	}
    	elseif($request->get('form_contact') != NULL)
    	{
    		$champs = $request->get('form_contact');
    		$entity->setType(2);
    		
    		if($champs['nom'] != NULL)
    			$entity->setNom($champs['nom']);
    		
    		if($champs['prenom'] != NULL)
    			$entity->setPrenom($champs['prenom']);
    		
    		if($champs['mail'] != NULL)
    			$entity->setEmail($champs['mail']);
    		
    		if($champs['tel'] != NULL)
    			$entity->setTelephone($champs['tel']);
    		
    		if($champs['message'] != NULL)
    			$entity->setMessage($champs['message']);
    		
    		if($champs['domaine'] != NULL)
    			$entity->setSource($champs['domaine']);
    		
    		$em->persist($entity);
    		$em->flush();
    		
    		if($_FILES['cv']['name'] != NULL)
    		{
    			$entity->setFichier($_FILES['cv']['name']);
    		
    			mkdir(getcwd().'/upload/Contact/'.$entity->getId(), 0775);
    			$uploaddir = getcwd().'/upload/Contact/'.$entity->getId().'/';
    		
	    		$uploadfile = $uploaddir . basename($_FILES['cv']['name']);
	    		
	    		if (move_uploaded_file($_FILES['cv']['tmp_name'], $uploadfile)) 
	    		{
	    			echo "Le fichier est valide, et a été téléchargé avec succès. Voici plus d'informations :\n";
	    		} 
	    		
	    		$em->persist($entity);
	    		$em->flush();
    		}
    		
    		if($_FILES['lettre']['name'] != NULL)
    		{
    			$entity->setFichier2($_FILES['lettre']['name']);
    		
    			if($entity->getFichier() == NULL)
    				mkdir(getcwd().'/upload/Contact/'.$entity->getId(), 0775);
    			
    			$uploaddir = getcwd().'/upload/Contact/'.$entity->getId().'/';
    		
    			$uploadfile = $uploaddir . basename($_FILES['lettre']['name']);
    			 
    			if (move_uploaded_file($_FILES['lettre']['tmp_name'], $uploadfile))
    			{
    				echo "Le fichier est valide, et a été téléchargé avec succès. Voici plus d'informations :\n";
    			}
    			 
    			$em->persist($entity);
    			$em->flush();
    		}
    		
    		
    		
    		$this->get('session')->getFlashBag()->add('success', 'Votre candidature nous a bien été envoyée.');
    		
    		$templateFile = "Contact/email2.html.twig";
    		 
    		$body = $this->renderView($templateFile, array('entity' => $entity));
    		$message = \Swift_Message::newInstance()
    		->setSubject("[cipres.fr] Candidature spontanée")
    		->setFrom(array('noreply@cipres.fr' => 'Cipres'))
    		->setTo('recrutement@cipres.fr')
    		->setBcc(array('contact@cipres.fr'))
            // ->setTo('gperreau@cipres.fr')
            // ->setBcc(array('ncueff@cipres.fr'))
    		->setBody($body, 'text/html');
    		
    		if($entity->getFichier() != NULL)
    			$message->attach(\Swift_Attachment::fromPath(getcwd().'/upload/Contact/'.$entity->getId().'/'.$entity->getFichier()));
    		if($entity->getFichier2() != NULL)
    			$message->attach(\Swift_Attachment::fromPath(getcwd().'/upload/Contact/'.$entity->getId().'/'.$entity->getFichier2()));
    		 
    		$this->container->get('mailer')->send($message);
    		
    		return $this->redirect($this->generateUrl('page_show', array('slug' => 'recrutement')));
    	}
    	elseif($request->get('form_presse') != NULL)
    	{
    		$champs = $request->get('form_presse');
    		$entity->setType(4);
    	
    		if($champs['nom'] != NULL)
    			$entity->setNom($champs['nom']);
    	
    		if($champs['prenom'] != NULL)
    			$entity->setPrenom($champs['prenom']);
    		
    		if($champs['societe'] != NULL)
    			$entity->setRaison($champs['societe']);
    	
    		if($champs['email'] != NULL)
    			$entity->setEmail($champs['email']);
    	
    		if($champs['telephone'] != NULL)
    			$entity->setTelephone($champs['telephone']);
    	
    		if($champs['objet'] != NULL)
    			$entity->setObjet($champs['objet']);
    		
    		if($champs['message'] != NULL)
    			$entity->setMessage($champs['message']);
    	
    		$entity->setSource('');
    		
    		$em->persist($entity);
    		$em->flush();
    	
    		$this->get('session')->getFlashBag()->add('success', 'Votre demande nous a bien été envoyée.');
    	
    		$templateFile = "Contact/email6.html.twig";
    		 
    		$body = $this->renderView($templateFile, array('entity' => $entity));
    		$message = \Swift_Message::newInstance()
    		->setSubject("[cipres.fr] Prise de contact presse")
    		->setFrom(array('noreply@cipres.fr' => 'Cipres'))
    		->setTo('presse@cipres.fr')
            // ->setTo('gperreau@cipres.fr')
            ->setBcc(array('contact@cipres.fr', 'cipres@wellcom.fr'))
    		->setBody($body, 'text/html');
    		 
    		$this->container->get('mailer')->send($message);
    	
    		return $this->redirect($this->generateUrl('presse_contact'));
    	}
    	elseif($request->get('form_media') != NULL)
    	{
    		$champs = $request->get('form_media');
    		$entity->setType(6);
    		
    		$media = $em->getRepository('AppBundle:Media')->find($champs['id']);
    		
    		if($champs['nom'] != NULL)
    			$entity->setNom($champs['nom']);
    		
    		if($champs['prenom'] != NULL)
    			$entity->setPrenom($champs['prenom']);
    		
    		if($champs['mail'] != NULL)
    			$entity->setEmail($champs['mail']);
    		
    		if($champs['societe'] != NULL)
    			$entity->setNomCabinet($champs['societe']);
    		
    		$entity->setSource('');
    		
    		$em->persist($entity);
    		$em->flush();
    		
    		$this->get('session')->getFlashBag()->add('success', 'Votre candidature nous a bien été envoyée.');
    		
    		$templateFile = "Contact/email7.html.twig";
    			
    		$body = $this->renderView($templateFile, array('entity' => $entity, 'media' => $media));
    		$message = \Swift_Message::newInstance()
    		->setSubject("[cipres.fr] Votre demande de média")
    		->setFrom(array('noreply@cipres.fr' => 'Cipres'))
    		->setTo($entity->getEmail())
            // ->setTo('gperreau@cipres.fr')
            // ->setBcc(array('ncueff@cipres.fr'))
    		->setBody($body, 'text/html');
    		
    		if($media->getFichier() != NULL)
    			$message->attach(\Swift_Attachment::fromPath(getcwd().'/upload/Media/'.$media->getId().'/'.$media->getFichier()));
    		 
    		$this->container->get('mailer')->send($message);
    		
    		return $this->redirect($this->generateUrl('media_index'));
    	}
    	elseif($request->get('form_partenaire') != NULL)
    	{
    		$champs = $request->get('form_partenaire');
    		$entity->setType(8);
    		
    		if($champs['nomCabinet'] != NULL)
    			$entity->setNomCabinet($champs['nomCabinet']);
    		if($champs['adresse'] != NULL)
    			$entity->setAdresse($champs['adresse']);
    		if($champs['codePostal'] != NULL)
    			$entity->setCodePostal($champs['codePostal']);
    		if($champs['ville'] != NULL)
    			$entity->setVille($champs['ville']);
    		if($champs['emailCabinet'] != NULL)
    			$entity->setEmailCabinet($champs['emailCabinet']);
    		if($champs['telephoneCabinet'] != NULL)
    			$entity->setTelephoneCabinet($champs['telephoneCabinet']);
    		if($champs['siret'] != NULL)
    			$entity->setSiret($champs['siret']);
    		if($champs['orias'] != NULL)
    			$entity->setOrias($champs['orias']);
    		if($champs['nom'] != NULL)
    			$entity->setNom($champs['nom']);
    		if($champs['prenom'] != NULL)
    			$entity->setPrenom($champs['prenom']);
    		if($champs['email'] != NULL)
    			$entity->setEmail($champs['email']);
    		if($champs['telephone'] != NULL)
    			$entity->setTelephone($champs['telephone']);
    		if($champs['message'] != NULL)
    			$entity->setMessage($champs['message']);
    		
    		$entity->setSource('');
    		
    		$em->persist($entity);
    		$em->flush();
    		
    		if($_FILES['fichier']['name'] != NULL)
    		{
    			$entity->setFichier($_FILES['fichier']['name']);
    		
    			mkdir(getcwd().'/upload/Contact/'.$entity->getId(), 0775);
    			$uploaddir = getcwd().'/upload/Contact/'.$entity->getId().'/';
    		
    			$uploadfile = $uploaddir . basename($_FILES['fichier']['name']);
    			 
    			if (move_uploaded_file($_FILES['fichier']['tmp_name'], $uploadfile))
    			{
    				echo "Le fichier est valide, et a été téléchargé avec succès. Voici plus d'informations :\n";
    			}
    			 
    			$em->persist($entity);
    			$em->flush();
    		}
    		
    		if($_FILES['fichier2']['name'] != NULL)
    		{
    			$entity->setFichier2($_FILES['fichier2']['name']);
    		
    			mkdir(getcwd().'/upload/Contact/'.$entity->getId(), 0775);
    			$uploaddir = getcwd().'/upload/Contact/'.$entity->getId().'/';
    		
    			$uploadfile = $uploaddir . basename($_FILES['fichier2']['name']);
    		
    			if (move_uploaded_file($_FILES['fichier2']['tmp_name'], $uploadfile))
    			{
    				echo "Le fichier est valide, et a été téléchargé avec succès. Voici plus d'informations :\n";
    			}
    		
    			$em->persist($entity);
    			$em->flush();
    		}
    		
    		if($_FILES['fichier3']['name'] != NULL)
    		{
    			$entity->setFichier($_FILES['fichier3']['name']);
    		
    			mkdir(getcwd().'/upload/Contact/'.$entity->getId(), 0775);
    			$uploaddir = getcwd().'/upload/Contact/'.$entity->getId().'/';
    		
    			$uploadfile = $uploaddir . basename($_FILES['fichier3']['name']);
    		
    			if (move_uploaded_file($_FILES['fichier3']['tmp_name'], $uploadfile))
    			{
    				echo "Le fichier est valide, et a été téléchargé avec succès. Voici plus d'informations :\n";
    			}
    		
    			$em->persist($entity);
    			$em->flush();
    		}
    		
    		$templateFile = "Contact/email8.html.twig";
    		
    		$body = $this->renderView($templateFile, array('entity' => $entity));
    		$message = \Swift_Message::newInstance()
    		->setSubject("[cipres.fr] Devenir courtier partenaire")
    		->setFrom(array('noreply@cipres.fr' => 'Cipres'))
    		->setTo('commercial@cipres.fr')
    		->setBcc(array('contact@cipres.fr'))
            // ->setTo('gperreau@cipres.fr')
            // ->setBcc(array('ncueff@cipres.fr'))
    		->setBody($body, 'text/html');
    		
    		if($entity->getFichier() != NULL)
    			$message->attach(\Swift_Attachment::fromPath(getcwd().'/upload/Contact/'.$entity->getId().'/'.$entity->getFichier()));
    		if($entity->getFichier2() != NULL)
    			$message->attach(\Swift_Attachment::fromPath(getcwd().'/upload/Contact/'.$entity->getId().'/'.$entity->getFichier2()));
    		if($entity->getFichier3() != NULL)
    			$message->attach(\Swift_Attachment::fromPath(getcwd().'/upload/Contact/'.$entity->getId().'/'.$entity->getFichier3()));
    			
    		$this->container->get('mailer')->send($message);
    		
    		$this->get('session')->getFlashBag()->add('success', 'Votre demande nous a bien été envoyée.');
    		
    		return $this->redirect($this->generateUrl('page_show', array('slug' => 'courtier-partenaire')));
    	}
    	else
    	{
    		$form = $this->createCreateForm($entity);
    		$form->handleRequest($request);
    		
    		if ($form->isValid()) {

    			$champs = $request->get('appbundle_contacttype');
    			
    			$entity->setType(3);
    			
    			if($champs['source'] == 1 && $champs['souhait'] == 2)
    			{
    				$champsC = $request->get('form_c');
    				
    				if($champsC['nomCabinet1'] != NULL)
    					$entity->setNomCabinet($champsC['nomCabinet1']);
    				if($champsC['nom1'] != NULL)
    					$entity->setNom($champsC['nom1']);
    				if($champsC['prenom1'] != NULL)
    					$entity->setPrenom($champsC['prenom1']);
    				if($champsC['email1'] != NULL)
    					$entity->setEmail($champsC['email1']);
    				if($champsC['telephone1'] != NULL)
    					$entity->setTelephone($champsC['telephone1']);
    				if($champsC['message1'] != NULL)
    					$entity->setMessage($champsC['message1']);
    				
    				$templateFile = "Contact/email3.html.twig";
    				 
    				$body = $this->renderView($templateFile, array('entity' => $entity));
    				$message = \Swift_Message::newInstance()
    				->setSubject("[cipres.fr] Demande d’informations supplémentaires")
    				->setFrom(array('noreply@cipres.fr' => 'Cipres'))
    				// ->setTo('commercial@cipres.fr')
    				// ->setBcc(array('contact@cipres.fr'))
                    ->setTo('gperreau@cipres.fr')
                    ->setBcc(array('ncueff@cipres.fr'))
    				->setBody($body, 'text/html');
    				 
    				$this->container->get('mailer')->send($message);
    			}
    			elseif($champs['source'] == 1 && $champs['souhait'] == 3)
    			{
    				$champsC = $request->get('form_c2');
    				
    				if($champsC['nomCabinet'] != NULL)
    					$entity->setNomCabinet($champsC['nomCabinet']);
    				if($champsC['nom'] != NULL)
    					$entity->setNom($champsC['nom']);
    				if($champsC['prenom'] != NULL)
    					$entity->setPrenom($champsC['prenom']);
    				if($champsC['adresse'] != NULL)
    					$entity->setAdresse($champsC['adresse']);
    				if($champsC['cp'] != NULL)
    					$entity->setCodePostal($champsC['cp']);
    				if($champsC['ville'] != NULL)
    					$entity->setVille($champsC['ville']);
    				if($champsC['email'] != NULL)
    					$entity->setEmail($champsC['email']);
    				if($champsC['telephone'] != NULL)
    					$entity->setTelephone($champsC['telephone']);
    				if($champsC['message'] != NULL)
    					$entity->setMessage($champsC['message']);
    				
    				$templateFile = "Contact/email4.html.twig";
    					
    				$body = $this->renderView($templateFile, array('entity' => $entity));
    				$message = \Swift_Message::newInstance()
    				->setSubject("[cipres.fr] Demande de rendez-vous courtier")
    				->setFrom(array('noreply@cipres.fr' => 'Cipres'))
    				// ->setTo('commercial@cipres.fr')
    				// ->setBcc(array('contact@cipres.fr'))
                    ->setTo('gperreau@cipres.fr')
                    ->setBcc(array('ncueff@cipres.fr'))
    				->setBody($body, 'text/html');
    					
    				$this->container->get('mailer')->send($message);
    			}

//				==== FORMULAIRES ENTREPRISE OU TNS ====
				elseif($champs['source'] == 2 && $champs['client'] == 0)
				{
					$champsC = $request->get('form_c6_1');

					if($request->get('appbundle_contacttype[civilite]') != NULL)
						$entity->setCivilite($request->get('appbundle_contacttype[civilite]'));
					if($champsC['nomReclam'] != NULL)
						$entity->setNom($champsC['nomReclam']);
					if($champsC['prenomReclam'] != NULL)
						$entity->setPrenom($champsC['prenomReclam']);
					if($request->get('raison') != NULL)
						$entity->setRaison($request->get('raison'));
					if($champsC['email'] != NULL)
						$entity->setEmail($champsC['email']);
					if($champsC['telephone'] != NULL)
						$entity->setTelephone($champsC['telephone']);
					if($request->get('num_rcs') != NULL)
						$entity->setNumRcs($request->get('num_rcs'));
					if($request->get('objet_reclam') != NULL)
						$entity->setObjetReclam($request->get('objet_reclam'));
					if($request->get('motif_reclam') != NULL)
						$entity->setMotifReclam($request->get('motif_reclam'));
					if($request->get('num_contrat') != NULL)
						$entity->setNumContrat($request->get('num_contrat'));

					$templateFile = "Contact/email10.html.twig";

					$body = $this->renderView($templateFile, array('entity' => $entity));
					$message = \Swift_Message::newInstance()
						->setSubject("[cipres.fr] Demande de réclamation Entreprise ou TNS")
						->setFrom(array('noreply@cipres.fr' => 'Cipres'))
						// ->setTo('commercial@cipres.fr')
						// ->setBcc(array('contact@cipres.fr'))
						->setTo('gperreau@cipres.fr')
//						->setBcc(array('ncueff@cipres.fr'))
						->setBody($body, 'text/html');

					$this->container->get('mailer')->send($message);

					$em->persist($entity);
					$em->flush();
				}

    			elseif($champs['source'] == 2 && $champs['client'] == 1)
    			{
    				$champsC = $request->get('form_c6');
    				
    				if($champsC['nom'] != NULL)
    					$entity->setNom($champsC['nom']);
    				if($champsC['prenom'] != NULL)
    					$entity->setPrenom($champsC['prenom']);
    				if($champsC['raison'] != NULL)
    					$entity->setRaison($champsC['raison']);
    				if($champsC['adresse'] != NULL)
    					$entity->setAdresse($champsC['adresse']);
    				if($champsC['codePostal'] != NULL)
    					$entity->setCodePostal($champsC['codePostal']);
    				if($champsC['ville'] != NULL)
    					$entity->setVille($champsC['ville']);
    				if($champsC['email'] != NULL)
    					$entity->setEmail($champsC['email']);
    				if($champsC['telephone'] != NULL)
    					$entity->setTelephone($champsC['telephone']);
    				if($champsC['message'] != NULL)
    					$entity->setMessage($champsC['message']);
    				
    				$em->persist($entity);
    				$em->flush();
    				
    				$templateFile = "Contact/email9.html.twig";
    					
    				$body = $this->renderView($templateFile, array('entity' => $entity));
    				$message = \Swift_Message::newInstance()
    				->setSubject("[cipres.fr] Demande d’un prospect entreprise ou TNS")
    				->setFrom(array('noreply@cipres.fr' => 'Cipres'))
//    				->setTo('commercial@cipres.fr')
//    				->setBcc(array('contact@cipres.fr'))
                     ->setTo('gperreau@cipres.fr')
                     ->setBcc(array('ncueff@cipres.fr'))
    				->setBody($body, 'text/html');
    					
    				$this->container->get('mailer')->send($message);
    			}

//				==== FORMULAIRES SALARIE ====
				elseif($champs['source'] == 3 && $champs['client_salarie'] == 0)
				{
					$champsC = $request->get('form_c8_1');

					if($champsC['civilite'] != NULL)
						$entity->setCivilite($champsC['civilite']);
					if($champsC['nom'] != NULL)
						$entity->setNom($champsC['nom']);
					if($champsC['prenom'] != NULL)
						$entity->setPrenom($champsC['prenom']);
					if($champsC['adresse'] != NULL)
						$entity->setAdresse($champsC['adresse']);
					if($champsC['codePostal'] != NULL)
						$entity->setCodePostal($champsC['codePostal']);
					if($champsC['email'] != NULL)
						$entity->setEmail($champsC['email']);
					if($champsC['telephone'] != NULL)
						$entity->setTelephone($champsC['telephone']);
					if($request->get('appbundle_contacttype[date_naissance]') != NULL)
						$entity->setDateNaissance($request->get('appbundle_contacttype[date_naissance]'));
					if($champsC['objet_reclam'] != NULL)
						$entity->setObjetReclam($champsC['objet_reclam']);
					if($champsC['motif_reclam'] != NULL)
						$entity->setMotifReclam($champsC['motif_reclam']);
					if($champsC['num_contrat'] != NULL)
						$entity->setNumContrat($champsC['num_contrat']);

					$templateFile = "Contact/email12.html.twig";

					$body = $this->renderView($templateFile, array('entity' => $entity));
					$message = \Swift_Message::newInstance()
						->setSubject("[cipres.fr] Demande de réclamation Salarié")
						->setFrom(array('noreply@cipres.fr' => 'Cipres'))
						// ->setTo('commercial@cipres.fr')
						// ->setBcc(array('contact@cipres.fr'))
						->setTo('gperreau@cipres.fr')
//						->setBcc(array('ncueff@cipres.fr'))
						->setBody($body, 'text/html');

					$this->container->get('mailer')->send($message);

					$em->persist($entity);
					$em->flush();
				}

				elseif($champs['source'] == 3 && $champs['client_salarie'] == 1)
				{
					$champsC = $request->get('form_c8');

					if($champsC['nom'] != NULL)
						$entity->setNom($champsC['nom']);
					if($champsC['prenom'] != NULL)
						$entity->setPrenom($champsC['prenom']);
					if($champsC['adresse'] != NULL)
						$entity->setAdresse($champsC['adresse']);
					if($champsC['codePostal'] != NULL)
						$entity->setCodePostal($champsC['codePostal']);
					if($champsC['ville'] != NULL)
						$entity->setVille($champsC['ville']);
					if($champsC['email'] != NULL)
						$entity->setEmail($champsC['email']);
					if($champsC['telephone'] != NULL)
						$entity->setTelephone($champsC['telephone']);
					if($champsC['message'] != NULL)
						$entity->setMessage($champsC['message']);

					$em->persist($entity);
					$em->flush();

					$templateFile = "Contact/email11.html.twig";

					$body = $this->renderView($templateFile, array('entity' => $entity));
					$message = \Swift_Message::newInstance()
						->setSubject("[cipres.fr] Demande d’un prospect salarié")
						->setFrom(array('noreply@cipres.fr' => 'Cipres'))
//    				->setTo('commercial@cipres.fr')
//    				->setBcc(array('contact@cipres.fr'))
						->setTo('gperreau@cipres.fr')
						->setBody($body, 'text/html');

					$this->container->get('mailer')->send($message);
				}

//				==== FORMULAIRE EXPERT COMPTABLE ====
				elseif($champs['source'] == 4) {
					$champsC = $request->get('form_c9');

					if($champsC['nom'] != NULL)
						$entity->setNom($champsC['nom']);
					if($champsC['prenom'] != NULL)
						$entity->setPrenom($champsC['prenom']);
					if($champsC['adresse'] != NULL)
						$entity->setAdresse($champsC['adresse']);
					if($champsC['codePostal'] != NULL)
						$entity->setCodePostal($champsC['codePostal']);
					if($champsC['ville'] != NULL)
						$entity->setVille($champsC['ville']);
					if($champsC['email'] != NULL)
						$entity->setEmail($champsC['email']);
					if($champsC['telephone'] != NULL)
						$entity->setTelephone($champsC['telephone']);
					if($champsC['nomSte'] != NULL)
						$entity->setNomSte($champsC['nomSte']);
					if($champsC['civilite'] != NULL)
						$entity->setCivilite($champsC['civilite']);
					if($champsC['fonction_EC'] != NULL)
						$entity->setFonctionEC($champsC['fonction_EC']);
					if($request->get('appbundle_contacttype[taille_entreprise]') != NULL)
						$entity->setTailleEntreprise($request->get('appbundle_contacttype[taille_entreprise]'));
					if($request->get('appbundle_contacttype[besoinsEC]') != NULL)
						$entity->setBesoinsEC($request->get('appbundle_contacttype[besoinsEC]'));

					$em->persist($entity);
					$em->flush();

					$templateFile = "Contact/email13.html.twig";

					$body = $this->renderView($templateFile, array('entity' => $entity));
					$message = \Swift_Message::newInstance()
						->setSubject("[cipres.fr] Demande d’un prospect Expert comptable")
						->setFrom(array('noreply@cipres.fr' => 'Cipres'))
//    				->setTo('commercial@cipres.fr')
//    				->setBcc(array('contact@cipres.fr'))
						->setTo('gperreau@cipres.fr')
						->setBody($body, 'text/html');

					$this->container->get('mailer')->send($message);
				}


    			$em->persist($entity);
    			$em->flush();
    		
    			$this->get('session')->getFlashBag()->add('success', 'Votre demande nous a bien été envoyée.');
    			
    			return $this->redirect($this->generateUrl('contact_index'));
    		}	
    	}
    
    	return array(
    			'entity' => $entity,
    			'form'   => $form->createView(),
    	);
    }
}
