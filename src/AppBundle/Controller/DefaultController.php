<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
    	$em = $this->getDoctrine()->getManager();
    	
    	$slide = $em->getRepository('AppBundle:Slide')->find(1);
    	$page = $em->getRepository('AppBundle:Page')->find(1);

    	$db = $em->createQueryBuilder()
    	->select('solution')
    	->from('AppBundle\Entity\Solution', 'solution')
    	->where('solution.homepage = 1')
    	->orderBy('solution.id', 'DESC')
    	->setMaxResults(3);
    	
    	$solutions = $db->getQuery()->getResult();
    	
    	$db = $this->getDoctrine()->getManager()->createQueryBuilder()
    	->select('actualite')
    	->from('AppBundle\Entity\Actualite', 'actualite')
    	->where('actualite.categorie = 2')
		->andwhere('actualite.enabled = 1')
		->andwhere('actualite.debut_publication <= :today')
		->andwhere('actualite.fin_publication > :today')
		->setParameter('today', new \DateTime())
    	->orderBy('actualite.id', 'DESC')
    	->setMaxResults(3);
    	 
    	$actualites = $db->getQuery()->getResult();
    	
    	$db = $this->getDoctrine()->getManager()->createQueryBuilder()
    	->select('partenaire')
    	->from('AppBundle\Entity\Partenaire', 'partenaire')
    	->orderBy('partenaire.titre', 'ASC');
    	
    	$partenaires = $db->getQuery()->getResult();

        return $this->render('default/index.html.twig', array(
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
        	'slide' => $slide,
        	'page' => $page,
        	'solutions' => $solutions,
        	'actualites' => $actualites,
        	'partenaires' => $partenaires,
        ));
    }
    
    /**
     * @Route("/landing/courtier", name="landing_courtier")
     */
    public function landingCourtierAction(Request $request)
    {
    	$em = $this->getDoctrine()->getManager();
    	 
    	$slide = $em->getRepository('AppBundle:Slide')->find(2);
    	$page = $em->getRepository('AppBundle:Page')->find(9);
    	 
    	$db = $em->createQueryBuilder()
    	->select('solution')
    	->from('AppBundle\Entity\Solution', 'solution')
    	->where('solution.courtier = 1')
    	->orderBy('solution.id', 'DESC')
    	->setMaxResults(3);
    	 
    	$solutions = $db->getQuery()->getResult();
    	 
    	$db = $this->getDoctrine()->getManager()->createQueryBuilder()
    	->select('actualite')
    	->from('AppBundle\Entity\Actualite', 'actualite')
    	->where('actualite.categorie = 4')
    	->orderBy('actualite.id', 'DESC')
    	->setMaxResults(3);
    
    	$actualites = $db->getQuery()->getResult();
    
    	return $this->render('default/landingCourtier.html.twig', array(
    			'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
    			'slide' => $slide,
    			'page' => $page,
    			'solutions' => $solutions,
    			'actualites' => $actualites,
    	));
    }
    
    /**
     * @Route("/landing/tns", name="landing_tns")
     */
    public function landingTnsAction(Request $request)
    {
    	$em = $this->getDoctrine()->getManager();
    
    	$slides = $em->getRepository('AppBundle:Slide')->findByCategorie(3);
    	$page = $em->getRepository('AppBundle:Page')->find(10);
    
    	$db = $this->getDoctrine()->getManager()->createQueryBuilder()
		->select('solution')
		->from('AppBundle\Entity\Solution', 'solution')
		->where('solution.categorie != 5')
		->orderBy('solution.id', 'DESC');
		 
		$solutions = $db->getQuery()->getResult();
    
    	return $this->render('default/landingTns.html.twig', array(
    			'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
    			'slides' => $slides,
    			'page' => $page,
    			'solutions' => $solutions,
    	));
    }
}
