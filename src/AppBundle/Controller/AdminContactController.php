<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use FOS\UserBundle\Controller\SecurityController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Contact;

/**
 * @Route("/admin/contact")
*/
class AdminContactController extends Controller
{
    /**
     * @Route("/", name="admin_contact")
     * @Template("AdminContact/index.html.twig")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
    	
    	$user = $this->container->get('security.context')->getToken()->getUser();
    	
    	$entities = $em->getRepository('AppBundle:Contact')->findAll();
        
        return array(
        	"user" => $user,
        	"entities" => $entities,
        );
    }
    
    /**
     * Finds and displays a Contact entity.
     *
     * @Route("/{id}/show", name="admin_contact_show")
     * @Method("GET")
     * @Template("AdminContact/show.html.twig")
     */
    public function showAction($id)
    {
    	$em = $this->getDoctrine()->getManager();
    
    	$entity = $em->getRepository('AppBundle:Contact')->find($id);
    	
    	$user = $this->container->get('security.context')->getToken()->getUser();
        
    	if (!$entity) {
    		throw $this->createNotFoundException('Unable to find Contact entity.');
    	}
    
    	return array(
    		'entity'      => $entity,
    		'user' => $user,
    	);
    }
}
