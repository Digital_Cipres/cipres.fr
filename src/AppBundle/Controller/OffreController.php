<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
/**
 * Offre controller.
 *
 * @Route("/")
 */
class OffreController extends Controller
{
	/**
	 * @Route("/offre-emploi-assurance", name="offre_index")
	 * @Method("GET")
     * @Template("Offre/index.html.twig")
	 */
	public function indexAction()
	{
		$em = $this->getDoctrine()->getManager();
		 
		$db = $this->getDoctrine()->getManager()->createQueryBuilder()
		->select('offre')
		->from('AppBundle\Entity\Offre', 'offre')
		->where('offre.enabled = 1')
		->orderBy('offre.dateAjout', 'DESC');
		 
		$offres = $db->getQuery()->getResult();

		return array(
    		'offres'      => $offres,
			'actif' => 5,
    	);
	}
}
