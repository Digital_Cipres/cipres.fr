<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use FOS\UserBundle\Controller\SecurityController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Solution;
use AppBundle\Form\SolutionType;

/**
 * @Route("/admin/solution")
*/
class AdminSolutionController extends Controller
{
    /**
     * @Route("/", name="admin_solution")
     * @Template("AdminSolution/index.html.twig")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
    	
    	$user = $this->container->get('security.context')->getToken()->getUser();
    	
    	$entities = $em->getRepository('AppBundle:Solution')->findAll();
        
        return array(
        	"user" => $user,
        	"entities" => $entities,
        );
    }
    
    /**
     * Creates a new Solution entity.
     *
     * @Route("/create", name="admin_solution_create")
     * @Method("POST")
     * @Template("AdminSolution/new.html.twig")
     */
    public function createAction(Request $request)
    {
    	$entity = new Solution();
    	$form = $this->createCreateForm($entity);
    	$form->handleRequest($request);
    	$user = $this->container->get('security.context')->getToken()->getUser();
    
    	if ($form->isValid()) {
    		$em = $this->getDoctrine()->getManager();
    		
    		$champs = $request->get('appbundle_solutiontype');
    		
    		if($champs['slug'] == NULL)
    		{
    			$str = $champs['titre'];
    			$str = htmlentities($str, ENT_NOQUOTES, 'utf-8');
    			// Déspécialise tous les caractères déspécialisables (ex. é->e, œ -> oe, ç->c ou encore ñ->n)
    			$str = preg_replace('/\&([A-Za-z])(?:grave|acute|circ|tilde|uml|ring|cedil)\;/', '$1', $str);
    			$str = preg_replace('/\&([A-Za-z]{2})(?:lig)\;/', '$1', $str);
    			// Supprime tous les caractères non déspécialisables (ex. & = &)
    			$str = preg_replace('/\&([A-Za-z]*)\;/', '', $str);
    			// Remplace tous autres caractères différent d'une lettre, d'un chiffre ou du délimiteur par le délimiteur
    			$str = preg_replace('/[^A-Za-z0-9-]/', '-', $str);
    			// Supprime les doublons de délimiteur
    			$str = preg_replace('/[-]{2,}/', '-', $str);
    			// Convertit la chaine en minuscule
    			$str = strtolower($str);
    			// Suprime les délimiteurs en début et fin de chaine
    			$str = trim($str, '-');
    			 
    			$entity->setSlug($str);
    		}
    			
    		$em->persist($entity);
    		$em->flush();
    
    		return $this->redirect($this->generateUrl('admin_solution'));
    	}
    
    	return array(
    			'entity' => $entity,
    			'form'   => $form->createView(),
    			'user' => $user,
    	);
    }
    
    /**
     * Creates a form to create a Solution entity.
     *
     * @param Solution $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Solution $entity)
    {
    	$form = $this->createForm(new SolutionType(), $entity, array(
    			'action' => $this->generateUrl('admin_solution_create'),
    			'method' => 'POST',
    	));
    
    	return $form;
    }
    
    /**
     * Displays a form to create a new Solution entity.
     *
     * @Route("/new", name="admin_solution_new")
     * @Method("GET")
     * @Template("AdminSolution/new.html.twig")
     */
    public function newAction()
    {
    	$entity = new Solution();
    	$form   = $this->createCreateForm($entity);
    	
    	$user = $this->container->get('security.context')->getToken()->getUser();
    
    	return array(
    		'entity' => $entity,
    		'form'   => $form->createView(),
    		"user" => $user,
    	);
    }
    
    /**
     * Finds and displays a Solution entity.
     *
     * @Route("/{id}/show", name="admin_solution_show")
     * @Method("GET")
     * @Template("AdminSolution/show.html.twig")
     */
    public function showAction($id)
    {
    	$em = $this->getDoctrine()->getManager();
    
    	$entity = $em->getRepository('AppBundle:Solution')->find($id);
        
    	if (!$entity) {
    		throw $this->createNotFoundException('Unable to find Solution entity.');
    	}
    
    	return array(
    		'entity'      => $entity,
    	);
    }
    
    /**
     * Displays a form to edit an existing Solution entity.
     *
     * @Route("/{id}/edit", name="admin_solution_edit")
     * @Method("GET")
     * @Template("AdminSolution/edit.html.twig")
     */
    public function editAction($id)
    {
    	$em = $this->getDoctrine()->getManager();
    
    	$entity = $em->getRepository('AppBundle:Solution')->find($id);
    
    	if (!$entity) {
    		throw $this->createNotFoundException('Unable to find Solution entity.');
    	}
    
    	$editForm = $this->createEditForm($entity);
    	
    	$user = $this->container->get('security.context')->getToken()->getUser();
    
    	return array(
    		'entity'      => $entity,
    		'edit_form'   => $editForm->createView(),
    		'user' => $user,
    	);
    }
    
    /**
     * Creates a form to edit a Solution entity.
     *
     * @param Solution $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Solution $entity)
    {
    	$form = $this->createForm(new SolutionType(), $entity, array(
    			'action' => $this->generateUrl('admin_solution_update', array('id' => $entity->getId())),
    			'method' => 'POST',
    	));
    
    	return $form;
    }
    /**
     * Edits an existing Solution entity.
     *
     * @Route("/{id}/update", name="admin_solution_update")
     * @Method("POST")
     * @Template("AdminSolution/edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
    	$em = $this->getDoctrine()->getManager();
    
    	$entity = $em->getRepository('AppBundle:Solution')->find($id);
    
    	if (!$entity) {
    		throw $this->createNotFoundException('Unable to find Solution entity.');
    	}
    	
    	$tmpImage = $entity->getImage();
    	$tmpSlug = $entity->getSlug();
    	$firstImage = $request->files->get("appbundle_solutiontype");
    
    	$editForm = $this->createEditForm($entity);
    	$editForm->handleRequest($request);
    
    	if ($editForm->isValid()) {
    		
    		$champs = $request->get('appbundle_solutiontype');
    		
    		if($firstImage['image'] == null)
    			$entity->setImage($tmpImage);
    		else
    			$entity->setImage($firstImage['image']);
    		
    		if($champs['slug'] == "")
    		{
    			$str = $champs['titre'];
    			$str = htmlentities($str, ENT_NOQUOTES, 'utf-8');
    			$str = preg_replace('/\&([A-Za-z])(?:grave|acute|circ|tilde|uml|ring|cedil)\;/', '$1', $str);
    			$str = preg_replace('/\&([A-Za-z]{2})(?:lig)\;/', '$1', $str);
    			$str = preg_replace('/\&([A-Za-z]*)\;/', '', $str);
    			$str = preg_replace('/[^A-Za-z0-9-]/', '-', $str);
    			$str = preg_replace('/[-]{2,}/', '-', $str);
    			$str = strtolower($str);
    			$str = trim($str, '-');
    				
    			$entity->setSlug($str);
    		
    			/*FAIRE REDIRECTION 301*/
    		}
    		elseif($champs['slug'] != $tmpSlug)
    		{
    			$entity->setSlug($champs['slug']);
    		}
    		
    		$em->flush();
    
    		return $this->redirect($this->generateUrl('admin_solution'));
    	}
    	
    	$user = $this->container->get('security.context')->getToken()->getUser();
    
    	return array(
    		'entity'      => $entity,
    		'edit_form'   => $editForm->createView(),
    		'user' => $user,
    	);
    }
    
    /**
     * Deletes a Solution entity.
     *
     * @Route("/{id}/delete", name="admin_solution_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, $id)
    {
    	$form = $this->createDeleteForm($id);
    	$form->handleRequest($request);
    
    		$em = $this->getDoctrine()->getManager();
    		$entity = $em->getRepository('AppBundle:Solution')->find($id);
    
    		if (!$entity) {
    			throw $this->createNotFoundException('Unable to find Solution entity.');
    		}
    
    		$em->remove($entity);
    		$em->flush();
    
    	return $this->redirect($this->generateUrl('admin_solution'));
    }
    
    /**
     * Creates a form to delete a Solution entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
    	return $this->createFormBuilder()
    	->setAction($this->generateUrl('admin_solution_delete', array('id' => $id)))
    	->setMethod('GET')
    	->add('submit', 'submit', array('label' => 'Delete'))
    	->getForm()
    	;
    }
}
