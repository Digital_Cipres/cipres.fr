<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Slide controller.
 *
 * @Route("/")
 */
class PageController extends Controller
{
    /**
     * @Route("{slug}", name="page_show")
     */
    public function showAction($slug = "")
    {
    	$em = $this->getDoctrine()->getManager();
    	
    	$entity = $em->getRepository('AppBundle:Page')->findBySlug($slug);
    	
   	 	if (!$entity) {
    		throw $this->createNotFoundException('Unable to find Page entity.');
    	}
    	
        return $this->render('page/show.html.twig', array(
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
        	'slug' => $slug,
        	'entity' => $entity[0],
        ));
    }
}
