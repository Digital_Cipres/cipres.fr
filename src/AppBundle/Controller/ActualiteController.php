<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Actualite controller.
 *
 * @Route("/actualite-tns")
 */
class ActualiteController extends Controller
{
    /**
     * @Route("/", name="actualite_agenda")
     */
    public function indexAction()
    {
    	$em = $this->getDoctrine()->getManager();
    	
    	$db = $this->getDoctrine()->getManager()->createQueryBuilder()
    	->select('actualite')
    	->from('AppBundle\Entity\Actualite', 'actualite')
    	->where('actualite.categorie != 4')
		->andwhere('actualite.debut_publication <= :today')
		->andwhere('actualite.fin_publication > :today')
		->setParameter('today', new \DateTime())
    	->orderBy('actualite.id', 'DESC');
    	
    	$actualites = $db->getQuery()->getResult();
    	
    	$db = $em->createQueryBuilder()
    	->select('COUNT(actualite) as nb')
    	->from('AppBundle\Entity\Actualite', 'actualite')
    	->where('actualite.categorie = 1');
    	
    	$nb1 = $db->getQuery()->getResult();
    	
    	$db = $em->createQueryBuilder()
    	->select('COUNT(actualite) as nb')
    	->from('AppBundle\Entity\Actualite', 'actualite')
    	->where('actualite.categorie = 2');
    	 
    	$nb2 = $db->getQuery()->getResult();
    	
    	$db = $em->createQueryBuilder()
    	->select('COUNT(actualite) as nb')
    	->from('AppBundle\Entity\Actualite', 'actualite')
    	->where('actualite.categorie = 3');
    	 
    	$nb3 = $db->getQuery()->getResult();

        return $this->render('actualite/index.html.twig', array(
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
        	'actualites' => $actualites,
        	'actif' => 2,
        	'nb1' => $nb1[0]['nb'],
        	'nb2' => $nb2[0]['nb'],
        	'nb3' => $nb3[0]['nb'],
        ));
    }

    /**
     * @Route("/{slug}", name="actualite_show", requirements={"slug"=".+"})
     * @Method("GET")
     * @Template("actualite/show.html.twig")
     */
    public function showAction($slug = "")
    {
    	$em = $this->getDoctrine()->getManager();
    
    	$entity = $em->getRepository('AppBundle:Actualite')->findBySlug($slug);
    
    	if (!$entity) {
    		throw $this->createNotFoundException('Unable to find Actualite entity.');
    	}
    
    	return array(
    		'entity'      => $entity[0],
    		'actif' => 2,
    	);
    }
}
