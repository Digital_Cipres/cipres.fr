<?php

namespace EnewsBundle\Controller;

use EnewsBundle\Entity\Sondages;
use EnewsBundle\Form\SondagesType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use EnewsBundle\Entity\Articles;
use EnewsBundle\Entity\Newsletters;

/**
 * Enews controller.
 *
 * @Route("/Cipres-News")
 */
class EnewsController extends Controller
{
    /**
     * @Route("/", name="archive_newsletters")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        // On récupère toutes les newsletters
        $enews = $em
            ->getRepository('EnewsBundle:Newsletters')
            ->findByPubliee(1);
        ;
        if (null === $enews) {
            throw new NotFoundHttpException("Aucune newsletter n'a été trouvée");
        }

        // On récupère la liste des articles de chaque newsletter
        $query = $em->createQuery('SELECT a FROM EnewsBundle:Articles a WHERE a.position = 1');
        $listArticles = $query->getResult();

        return $this->render('EnewsBundle:Enews:index.html.twig', array(
            'newsletters'       => $enews,
            'listArticles'      => $listArticles,
        ));
    }


//    /**
//     * Génère un pdf d'après la page d'article
//     *
//     * @Route("/{edition_to_show}/{slug}/{pdf}/", name="generate_pdf")
//     * @internal param string $slug , int $edition_to_show, int $pdf
//     * @param string $slug
//     * @return Response
//     */
//    public function generatePdfAction($slug = "")
//    {
//        $em = $this->getDoctrine()->getManager();
//
////      Récupération des articles
//        $contenu_article = $em->getRepository('EnewsBundle:Articles')->findBySlug($slug);
//        if (!$contenu_article) {
//            throw $this->createNotFoundException('Unable to find Article entity.');
//        }
//
//
////      Récupération des articles lié à l'article affiché (pour le sommaire)
//        $edition_to_show = $contenu_article[0]->getEnewsId();
//
//        $articles_news = $em->getRepository('EnewsBundle:Articles')->findBy(array('enews_id' => $edition_to_show));
//
//
//        $html = $this->renderView('EnewsBundle:Enews:news.html.twig', array(
//            'contenu_article'      => $contenu_article,
//            'sommaire'             => $articles_news,
//        ));
//
//        return new Response(
//            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
//            200,
//            array(
//                'Content-Type'          => 'application/pdf',
//                'Content-Disposition'   => 'attachment; filename="file.pdf"',
//                'contenu_article'      => $contenu_article,
//                'sommaire'             => $articles_news,
//            )
//        );
//    }




    /**
     * @Route("/{edition_to_show}/{slug}", name="newsletter_show", requirements={"slug"=".+"})
     * @param string $slug , int $edition_to_show
     * @param Request $request
     * @return array
     */
    public function showAction($slug = "", Request $request)
    {
        // Gestion des sessions pour les articles réservés aux différents clubs
        $session = $this->get('session');
        if (!$session->has('name')) {
            $session_clubs = $request->query->get('club');
            if ($session_clubs == 'club_bronze') {
                $session = $request->getSession();
                $session->set('name', 'club_bronze');
            } elseif ($session_clubs == 'club_argent') {
                $session = $request->getSession();
                $session->set('name', 'club_argent');
            } elseif ($session_clubs == 'club_or') {
                $session = $request->getSession();
                $session->set('name', 'club_or');
            } elseif ($session_clubs == 'club_platine') {
                $session = $request->getSession();
                $session->set('name', 'club_platine');
            } elseif ($session_clubs == 'no_club') {
                $session = $request->getSession();
                $session->set('name', 'no_club');
            }
        }


        if ($this->container->get('session')->isStarted()) {

            // Requête de récupération des articles pour les clubs or et platine

            //Vérification que l'enews contient des articles réservés aux clubs
//            $em = $this->getDoctrine()->getManager();
//            $criteria = new \Doctrine\Common\Collections\Criteria();
//            $criteria->where($criteria->expr()->eq('avec_contenu_club', true));
//            $enews_clubs = $em->getRepository('EnewsBundle:Newsletters')->matching($criteria);

            $session = $request->getSession();
            if ( ($session->get('name') == 'club_or') || ($session->get('name') == 'club_platine') || ($session->get('name') == 'club_argent') || ($session->get('name') == 'club_bronze')) {
                $em = $this->getDoctrine()->getManager();
                $criteria = new \Doctrine\Common\Collections\Criteria();
                $criteria->where($criteria->expr()->eq('slug', $slug));

                $contenu_article = $em->getRepository('EnewsBundle:Articles')->matching($criteria);

                // Récupération des articles lié à l'article affiché (pour le sommaire)
                $edition_to_show = $contenu_article[0]->getEnews();

                $articles_news = $em->getRepository('EnewsBundle:Articles')->findBy(array('enews' => $edition_to_show));
            }
            elseif (($session->get('name') == 'no_club') || !$session->has('name')) {
                $em = $this->getDoctrine()->getManager();
                $criteria = new \Doctrine\Common\Collections\Criteria();
                $criteria->where($criteria->expr()->eq('slug', $slug));
                $criteria->andWhere($criteria->expr()->eq('contenu_club', false));

                $contenu_article = $em->getRepository('EnewsBundle:Articles')->matching($criteria);

                // Récupération des articles lié à l'article affiché (pour le sommaire)
                $edition_to_show = $contenu_article[0]->getEnews();

                $articles_news = $em->getRepository('EnewsBundle:Articles')->findBy(array('enews' => $edition_to_show, 'contenu_club' => false));
            }
        }


        if (!$contenu_article) {
            throw $this->createNotFoundException('Unable to find Article entity.');
        }

        return $this->render('EnewsBundle:Enews:news.html.twig', array(
            'contenu_article'      => $contenu_article,
            'sommaire'             => $articles_news,
        ));
    }
}
