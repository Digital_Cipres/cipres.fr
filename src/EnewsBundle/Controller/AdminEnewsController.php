<?php



namespace EnewsBundle\Controller;


use EnewsBundle\EnewsBundle;
use EnewsBundle\Form\ArticlesType;
use EnewsBundle\Form\NewslettersType;
use Proxies\__CG__\AppBundle\Entity\Newsletter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpFoundation\Request;

use EnewsBundle\Entity\Articles;

use EnewsBundle\Entity\Newsletters;



/**
 * @Route("/admin/enews")
 */

class AdminEnewsController extends Controller
{
    /**
     * Affichage des newsletters existantes
     * 
     * @Route("/", name="admin_enews_step1")
     * @Template("EnewsBundle:AdminEnews:index.html.twig")
     */

    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.context')->getToken()->getUser();
        $entities = $em->getRepository('EnewsBundle:Newsletters')->findAll();

        return array(
            "user" => $user,
            "entities" => $entities,
        );
    }

    /**
     * Affichage des articles d'une newsletter
     * 
     * @Route("/articles/{id_enews}", name="admin_enews_step2", requirements={"id_enews"=".+"})
     * @Template("EnewsBundle:AdminEnews:index_articles.html.twig")
     * @param string $id_enews
     * @return array
     */

    public function indexArticlesAction($id_enews=" ")
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.context')->getToken()->getUser();
        $entities = $em->getRepository('EnewsBundle:Articles')->findBy(array('enews' => $id_enews));

        return array(
            "user"      =>      $user,
            "entities"  =>      $entities,
            "enewsid"   =>      $id_enews,
        );
    }


    /**
     * Creates a new Newsletter entity.
     *
     * @Route("/create-newsletter", name="admin_create_newenews")
     * @Method("POST")
     * @Template("EnewsBundle:AdminEnews:new_enews.html.twig")
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */

    public function createEnewsAction(Request $request)
    {
        $entity = new Newsletters();

        $form = $this->createCreateEnewsForm($entity);
        $form->handleRequest($request);
        $user = $this->container->get('security.context')->getToken()->getUser();

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $champs = $request->get('EnewsBundle:NewslettersType');

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_enews_step1'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'user' => $user,
        );
    }


    /**
     * Creates a form to create a Newsletter entity.
     *
     * @param Newsletters $entity The entity
     * @return \Symfony\Component\Form\Form The form
     */

    private function createCreateEnewsForm(Newsletters $entity)
    {
        $form = $this->createForm(new NewslettersType(), $entity, array(
            'action' => $this->generateUrl('admin_create_newenews'),
            'method' => 'POST',
        ));

        return $form;
    }


    /**
     * Displays a form to create a new Newsletter entity.
     *
     * @Route("/new_enews", name="admin_enews_new")
     * @Method("GET")
     * @Template("EnewsBundle:AdminEnews:new_enews.html.twig")
     */

    public function newEnewsAction()
    {
        $entity = new Newsletters();
        $form   = $this->createCreateEnewsForm($entity);
        $user = $this->container->get('security.context')->getToken()->getUser();

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            "user" => $user,
        );
    }



    /**
     * Displays a form to edit an existing Newsletter entity.
     *
     * @Route("/edit/{id_enews}", name="admin_enews_edit")
     * @Method("GET")
     * @Template("EnewsBundle:AdminEnews:edit_enews.html.twig")
     * @param string $id_enews
     * @return array
     */

    public function editAction($id_enews ="")
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EnewsBundle:Newsletters')->find($id_enews);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Newsletter entity.');
        }

        $editForm = $this->createEditEnewsForm($entity);

        $user = $this->container->get('security.context')->getToken()->getUser();

        return array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'user' => $user,
        );
    }

    /**
     * Creates a form to edit a Newsletter entity.
     *
     * @param Newsletters $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */

    private function createEditEnewsForm(Newsletters $entity)
    {
        $form = $this->createForm(new NewslettersType(), $entity, array(
            'action' => $this->generateUrl('admin_enews_update', array('id_enews' => $entity->getEnewsId())),
            'method' => 'POST',
        ));

        return $form;
    }


    /**
     * Edits an existing Newsletter entity.
     *
     * @Route("/update_enews/{id_enews}", name="admin_enews_update")
     * @Template("EnewsBundle:AdminEnews:edit_enews.html.twig")
     * @param Request $request
     * @param string $id_enews
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */

    public function updateAction(Request $request, $id_enews = "")
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EnewsBundle:Newsletters')->find($id_enews);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Newsletter entity.');
        }

        $editForm = $this->createEditEnewsForm($entity);

        $editForm->handleRequest($request);

        if ($editForm->isValid())
        {
            $champs = $request->get('EnewsBundle:NewsletterType');


            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_enews_step1'));
        }

        $user = $this->container->get('security.context')->getToken()->getUser();

        return array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'user' => $user,
        );
    }


    /**
     * Deletes a Newsletter entity.
     *
     * @Route("/delete_enews/{id}", name="admin_enews_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('EnewsBundle:Newsletters')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Newsletter entity.');
        }

        $articles_to_delete = $entity->getArticles();
//        $em->remove($articles_to_delete);
        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('admin_enews_step1'));
    }

    /**
     * Creates a form to delete a Newsletter entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_enews_delete', array('id' => $id)))
            ->setMethod('GET')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
            ;
    }





//=========================================================================================
//============================= GESTION DES ARTICLES ======================================
//=========================================================================================
    /**
     * Creates a new Article entity.
     *
     * @Route("/create-article", name="admin_enews_create_article")
     * @Method("POST")
     * @Template("EnewsBundle:AdminEnews:new_article.html.twig")
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function createArticleAction(Request $request)
    {
        $entity = new Articles();

        $form = $this->createCreateArticlesForm($entity);
        $form->handleRequest($request);
        $user = $this->container->get('security.context')->getToken()->getUser();


        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $champs = $request->get('EnewsBundle:ArticlesType');

//            if($champs['slug'] == NULL)
//            {
//
//                $str = $champs['titre'];
//
//                $str = htmlentities($str, ENT_NOQUOTES, 'utf-8');
//
//                // Déspécialise tous les caractères déspécialisables (ex. é->e, œ -> oe, ç->c ou encore ñ->n)
//
//                $str = preg_replace('/\&([A-Za-z])(?:grave|acute|circ|tilde|uml|ring|cedil)\;/', '$1', $str);
//
//                $str = preg_replace('/\&([A-Za-z]{2})(?:lig)\;/', '$1', $str);
//
//                // Supprime tous les caractères non déspécialisables (ex. & = &)
//
//                $str = preg_replace('/\&([A-Za-z]*)\;/', '', $str);
//
//                // Remplace tous autres caractères différent d'une lettre, d'un chiffre ou du délimiteur par le délimiteur
//
//                $str = preg_replace('/[^A-Za-z0-9-]/', '-', $str);
//
//                // Supprime les doublons de délimiteur
//
//                $str = preg_replace('/[-]{2,}/', '-', $str);
//
//                // Convertit la chaine en minuscule
//
//                $str = strtolower($str);
//
//                // Suprime les délimiteurs en début et fin de chaine
//
//                $str = trim($str, '-');
//
//
//                $entity->setSlug($str);
//            }

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_enews_step1'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'user' => $user,
        );
    }



    /**
     * Creates a form to create an Articles entity.
     *
     * @param Articles $entity The entity
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateArticlesForm(Articles $entity)
    {
        $form = $this->createForm(new ArticlesType(), $entity, array(
            'action' => $this->generateUrl('admin_enews_create_article'),
            'method' => 'POST',
        ));

        return $form;
    }



    /**
     * Displays a form to create a new Articles entity.
     *
     * @Route("/new_article", name="admin_articles_new")
     * @Method("GET")
     * @Template("EnewsBundle:AdminEnews:new_article.html.twig")
     */
    public function newArticleAction()
    {
        $entity = new Articles();
        $form   = $this->createCreateArticlesForm($entity);
        $user = $this->container->get('security.context')->getToken()->getUser();

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            "user" => $user,
        );
    }


    /**
     * Displays a form to edit an existing Article entity.
     *
     * @Route("/edit_article/{id}", name="admin_article_edit")
     * @Method("GET")
     * @Template("EnewsBundle:AdminEnews:edit_article.html.twig")
     * @param string $id
     * @return array
     */

    public function editArticleAction($id ="")
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EnewsBundle:Articles')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Articles entity.');
        }

        $editForm = $this->createEditArticleForm($entity);

        $user = $this->container->get('security.context')->getToken()->getUser();

        return array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'user' => $user,
            'id_article' => $id,
        );
    }

    /**
     * Creates a form to edit an Article entity.
     *
     * @param Articles $entity
     * @return \Symfony\Component\Form\Form The form
     * @internal param $EnewsBundle :Articles $entity The entity
     */

    private function createEditArticleForm(Articles $entity)
    {
        $form = $this->createForm(new ArticlesType(), $entity, array(

            'action' => $this->generateUrl('admin_article_update', array('id' => $entity->getId())),
            'method' => 'POST',
        ));

        return $form;
    }


    /**
     * Edits an existing Articles entity.
     *
     * @Route("/update/{id}", name="admin_article_update")
     * @Method("POST")
     * @Template("EnewsBundle:AdminEnews:edit_article.html.twig")
     * @param Request $request
     * @param $id
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */

    public function updateArticleAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EnewsBundle:Articles')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Article entity.');
        }

        $tmpImage = $entity->getVisuelArticle();
        $firstImage = $request->files->get("EnewsBundle:ArticlesType");

        $editForm = $this->createEditArticleForm($entity);

        $editForm->handleRequest($request);

        $tmpImage = $entity->getVisuelArticle();
        $firstImage = $request->files->get('EnewsBundle:ArticlesType:visuelArticle');

        $editForm = $this->createEditArticleForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid())
        {
            $champs = $request->get('EnewsBundle:ArticlesType:visuelArticle');

            if($firstImage == null)
                $entity->setVisuelArticle($tmpImage);
            else
                $entity->setVisuelArticle($firstImage['visuelArticle']);


            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_enews_step1'));
        }


        $user = $this->container->get('security.context')->getToken()->getUser();

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'user' => $user,
        );
    }





    /**
     * Deletes an Article entity.
     *
     * @Route("/delete_article/{id}", name="admin_article_delete")
     * @Method("GET")
     */
    public function deleteArticleAction(Request $request, $id)
    {
        $form = $this->createDeleteArticleForm($id);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('EnewsBundle:Articles')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Article entity.');
        }

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('admin_enews_step1'));
    }

    
    
    /**
     * Creates a form to delete a Newsletter entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteArticleForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_article_delete', array('id' => $id)))
            ->setMethod('GET')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
            ;
    }

}

