<?php

namespace EnewsBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticlesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('position', 'text', array('label' => 'Position de l\'actu', 'required' => true))
            ->add('categorie', 'choice', array('label' => 'Catégorie', 'required' => true, 'choices' => array('Challenge' => 'Challenge', 'Juridique' => 'Juridique', 'Agenda' => 'Agenda', 'Produit' => 'Produit', 'Club' => 'Club', 'Expert-comptable' => 'Expert-comptable')))
            ->add('typeActu', 'choice', array('label' => 'Type de l\'actu', 'required' => true, 'choices' => array('Bon à savoir' => 'Bon à savoir', 'A la une' => 'A la une', 'Zoom sur' => 'Zoom sur', 'Challenge' => 'challenge', 'Interview' => 'Interview', 'Avantage club' => 'Avantage club')))
            ->add('titre', 'text', array('label' => 'Titre de l\'actu', 'required' => true))
            ->add('slug', 'text', array('label' => 'Url de l\'actu', 'required' => true))
            ->add('contenu', 'textarea', array('label' => 'Contenu de l\'actu', 'required' => true))
            ->add('visuelArticle', 'file', array("data_class" => null, "required" => false))
            ->add('contenu_club', "choice", array("required" => false, 'empty_value'  => '--',  'choices' => array('1' => 'Oui', '0' => 'Non')))
//            ->add('club', "choice", array("required" => false, 'empty_value'  => '--',  'choices' => array('club_bronze' => 'Bronze', 'club_argent' => 'Argent', 'club_or' => 'Or', 'club_platine' => 'Platine', 'club_or_et_platine' => 'Or et Platine')))
            ->add('enews', EntityType::class, array(
                'class' => 'EnewsBundle\Entity\Newsletters',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.enews_id', 'ASC');
                },
                'choice_label' => 'enewsid',
                'multiple' => false,
                'expanded' => false,
            ));
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EnewsBundle\Entity\Articles'
        ));
    }
}
