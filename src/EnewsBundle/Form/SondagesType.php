<?php

namespace EnewsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SondagesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tpsLecture', 'choice', array('label' => '1.	Combien de temps estimez-vous accorder à la lecture de la newsletter ?', 'required' => false, 'choices' => array('30min' => '30 minutes', '15min' => '15 minutes', '-5min' => 'Moins de 5 minutes', 'none' => 'je ne la lis pas')))
            ->add('interet', "choice", array('label' => '2. Seriez-vous intéressé par ?', "required" => false, 'expanded' => true, 'multiple' => true, 'choices' => array('droit_et_reglementation' => 'Des sujets sur le droit et la réglementation', 'assurance_de_personnes' => 'Des sujets d’assurance de personnes', 'autre' => 'Autre')))
            ->add('precisionInteret', 'text', array('label' => 'Veuillez préciser', 'required' => false))
            ->add('satisfaction', 'textarea', array('label' => '3.	Etes-vous satisfait du contenu de la newsletter actuelle ? Quelle rubrique semble manquer ? Quels sujets aimeriez-vous voir abordés ?', 'required' => false))
            ->add('sujetPrefere', 'textarea', array('label' => '4.	Quel sujet de la newsletter avez-vous préféré ?', 'required' => false))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EnewsBundle\Entity\Sondages'
        ));
    }
}
