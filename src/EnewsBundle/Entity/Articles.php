<?php

namespace EnewsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;


/**
 * Articles
 *
 * @ORM\Table(name="articles")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="EnewsBundle\Repository\ArticlesRepository")
 */
class Articles
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="position", type="integer")
     */
    private $position;


    /**
     * @var string
     *
     * @ORM\Column(name="categorie", type="string", length=255)
     */
    private $categorie;

    /**
     * @var string
     *
     * @ORM\Column(name="typeActu", type="string", length=255)
     */
    private $typeActu;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     */
    private $slug;
    
    /**
     * @var bool
     *
     * @ORM\Column(name="contenu_club", type="boolean")
     */
    private $contenu_club;

//    /**
//     * @var string
//     *
//     * @ORM\Column(name="club", type="string", length=255)
//     */
//    private $club;
    



    /**
     * @var string
     *
     * @ORM\Column(name="contenu", type="text")
     */
    private $contenu;

    /**
     *
     * @Assert\File(maxSize = "3M",
     *     mimeTypesMessage = "Veuillez charger une image compatible (PNG, JPG, GIF)", mimeTypes={"image/png", "image/jpeg", "image/gif"})
     * @ORM\Column(name="visuelArticle", type="string", length=255, nullable=true)
     */
    private $visuelArticle;



    /**
     * @ORM\ManyToOne(targetEntity="EnewsBundle\Entity\Newsletters", cascade={"remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $enews;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return Articles
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }


    /**
     * Set categorie
     *
     * @param string $categorie
     *
     * @return Articles
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return string
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set typeActu
     *
     * @param string $typeActu
     *
     * @return Articles
     */
    public function setTypeActu($typeActu)
    {
        $this->typeActu = $typeActu;

        return $this;
    }

    /**
     * Get typeActu
     *
     * @return string
     */
    public function getTypeActu()
    {
        return $this->typeActu;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Articles
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Articles
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     *
     * @return Articles
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    public function getFullImagePath() {
        return null === $this->visuelArticle ? null : $this->getUploadRootDir(). $this->visuelArticle;
    }

    protected function getUploadRootDir() {
        return $this->getTmpUploadRootDir().$this->getEnews()->getEnewsId()."/";
    }

    protected function getTmpUploadRootDir() {
        return __DIR__ . '/../../../web/upload/CipresNews/';
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function uploadImage() {
        // the file property can be empty if the field is not required
        if (null === $this->visuelArticle) {
            return;
        }
        if(!$this->id){
            $this->visuelArticle->move($this->getUploadRootDir(), $this->visuelArticle->getClientOriginalName());
            $this->setVisuelArticle($this->visuelArticle->getClientOriginalName());
        }elseif(is_object($this->visuelArticle)){
            $this->visuelArticle->move($this->getUploadRootDir(), $this->visuelArticle->getClientOriginalName());
            $this->setVisuelArticle($this->visuelArticle->getClientOriginalName());
        }
    }

    /**
     * @ORM\PostPersist()
     */
    public function moveImage()
    {
        if (null === $this->visuelArticle) {
            return;
        }
        if(!is_dir($this->getUploadRootDir())){
            mkdir($this->getUploadRootDir());
        }
        @copy($this->getTmpUploadRootDir().$this->visuelArticle, $this->getFullImagePath());
        @unlink($this->getTmpUploadRootDir().$this->visuelArticle);
    }

    /**
     * @ORM\PreRemove()
     */
    public function removeImage()
    {
        @unlink($this->getFullImagePath());
        @rmdir($this->getUploadRootDir());
    }

    /**
     * Set visuelArticle
     *
     * @param string $visuelArticle
     * @return Articles
     */
    public function setVisuelArticle($visuelArticle)
    {
        $this->visuelArticle = $visuelArticle;

        return $this;
    }

    /**
     * Get visuelArticle
     *
     * @return string
     */
    public function getVisuelArticle()
    {
        return $this->visuelArticle;
    }

    /**
     * Set enews
     *
     * @param Newsletters $enews
     *
     * @return Articles
     */
    public function setEnews(Newsletters $enews)
    {
        $this->enews = $enews;
        return $this;
    }

    /**
     * Get enews
     *
     * @return Newsletters
     */
    public function getEnews()
    {
        return $this->enews;
    }

    /**
     * Set contenuClub
     *
     * @param boolean $contenuClub
     *
     * @return Articles
     */
    public function setContenuClub($contenuClub)
    {
        $this->contenu_club = $contenuClub;

        return $this;
    }

    /**
     * Get contenuClub
     *
     * @return boolean
     */
    public function getContenuClub()
    {
        return $this->contenu_club;
    }

    /**
     * Set club
     *
     * @param string $club
     *
     * @return Articles
     */
    public function setClub($club)
    {
        $this->club = $club;

        return $this;
    }

    /**
     * Get club
     *
     * @return string
     */
    public function getClub()
    {
        return $this->club;
    }
}
