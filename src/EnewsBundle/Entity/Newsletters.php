<?php

namespace EnewsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Newsletters
 *
 * @ORM\Table(name="newsletters")
 * @ORM\Entity(repositoryClass="EnewsBundle\Repository\NewslettersRepository")
 */
class Newsletters
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;


    /**
     * @var bool
     *
     * @ORM\Column(name="publiee", type="boolean")
     */
    private $publiee;

    /**
     * @var string
     *
     * @ORM\Column(name="enews_id", type="string")
     */
    private $enews_id;
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Newsletters
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set publiee
     *
     * @param boolean $publiee
     *
     * @return Newsletters
     */
    public function setPubliee($publiee)
    {
        $this->publiee = $publiee;

        return $this;
    }

    /**
     * Get publiee
     *
     * @return boolean
     */
    public function getPubliee()
    {
        return $this->publiee;
    }



    /**
     * Set enewsId
     *
     * @param integer $enewsId
     *
     * @return Newsletters
     */
    public function setEnewsId($enewsId)
    {
        $this->enews_id = $enewsId;

        return $this;
    }

    /**
     * Get enewsId
     *
     * @return integer
     */
    public function getEnewsId()
    {
        return $this->enews_id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->articles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add article
     *
     * @param \EnewsBundle\Entity\Articles $article
     *
     * @return Newsletters
     */
    public function addArticle(\EnewsBundle\Entity\Articles $article)
    {
        $this->articles[] = $article;

        return $this;
    }

    /**
     * Remove article
     *
     * @param \EnewsBundle\Entity\Articles $article
     */
    public function removeArticle(\EnewsBundle\Entity\Articles $article)
    {
        $this->articles->removeElement($article);
    }

    /**
     * Get articles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArticles()
    {
        return $this->articles;
    }
}
