<?php

namespace EnewsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sondages
 *
 * @ORM\Table(name="sondages")
 * @ORM\Entity(repositoryClass="EnewsBundle\Repository\SondagesRepository")
 */
class Sondages
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tpsLecture", type="string", length=255)
     */
    private $tpsLecture;

    /**
     * @var string
     *
     * @ORM\Column(name="interet", type="string", length=255)
     */
    private $interet;

    /**
     * @var string
     *
     * @ORM\Column(name="precisionInteret", type="string", length=255)
     */
    private $precisionInteret;

    /**
     * @var string
     *
     * @ORM\Column(name="satisfaction", type="text")
     */
    private $satisfaction;

    /**
     * @var string
     *
     * @ORM\Column(name="sujetPrefere", type="string", length=255)
     */
    private $sujetPrefere;


    public function __construct()
    {
        $this->date = new \DateTime('now');
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tpsLecture
     *
     * @param string $tpsLecture
     *
     * @return Sondages
     */
    public function setTpsLecture($tpsLecture)
    {
        $this->tpsLecture = $tpsLecture;

        return $this;
    }

    /**
     * Get tpsLecture
     *
     * @return string
     */
    public function getTpsLecture()
    {
        return $this->tpsLecture;
    }

    /**
     * Set interet
     *
     * @param string $interet
     *
     * @return Sondages
     */
    public function setInteret($interet)
    {
        $this->interet = $interet;

        return $this;
    }

    /**
     * Get interet
     *
     * @return string
     */
    public function getInteret()
    {
        return $this->interet;
    }

    /**
     * Set satisfaction
     *
     * @param string $satisfaction
     *
     * @return Sondages
     */
    public function setSatisfaction($satisfaction)
    {
        $this->satisfaction = $satisfaction;

        return $this;
    }

    /**
     * Get satisfaction
     *
     * @return string
     */
    public function getSatisfaction()
    {
        return $this->satisfaction;
    }

    /**
     * Set sujetPrefere
     *
     * @param string $sujetPrefere
     *
     * @return Sondages
     */
    public function setSujetPrefere($sujetPrefere)
    {
        $this->sujetPrefere = $sujetPrefere;

        return $this;
    }

    /**
     * Get sujetPrefere
     *
     * @return string
     */
    public function getSujetPrefere()
    {
        return $this->sujetPrefere;
    }

    /**
     * Set precisionInteret
     *
     * @param string $precisionInteret
     *
     * @return Sondages
     */
    public function setPrecisionInteret($precisionInteret)
    {
        $this->precisionInteret = $precisionInteret;

        return $this;
    }

    /**
     * Get precisionInteret
     *
     * @return string
     */
    public function getPrecisionInteret()
    {
        return $this->precisionInteret;
    }
}
