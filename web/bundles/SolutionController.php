<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Solution controller.
 *
 * @Route("/")
 */
class SolutionController extends Controller
{
	/**
	 * @Route("/Nos-solutions", name="solution_index")
	 * @Method("GET")
     * @Template("Solution/index.html.twig")
	 */
	public function indexAction()
	{
		$em = $this->getDoctrine()->getManager();
		 
		$db = $this->getDoctrine()->getManager()->createQueryBuilder()
		->select('solution')
		->from('AppBundle\Entity\Solution', 'solution')
		->where('solution.categorie != 5')
		->orderBy('solution.id', 'DESC');
		 
		$solutions = $db->getQuery()->getResult();
		
		$db = $em->createQueryBuilder()
		->select('COUNT(solution) as nb')
		->from('AppBundle\Entity\Solution', 'solution')
		->where('solution.categorie = 1');
	
		$nb1 = $db->getQuery()->getResult();
		
		$db = $em->createQueryBuilder()
		->select('COUNT(solution) as nb')
		->from('AppBundle\Entity\Solution', 'solution')
		->where('solution.categorie = 2');
		
		$nb2 = $db->getQuery()->getResult();
		
		$db = $em->createQueryBuilder()
		->select('COUNT(solution) as nb')
		->from('AppBundle\Entity\Solution', 'solution')
		->where('solution.categorie = 3');
		
		$nb3 = $db->getQuery()->getResult();
		
		$db = $em->createQueryBuilder()
		->select('COUNT(solution) as nb')
		->from('AppBundle\Entity\Solution', 'solution')
		->where('solution.categorie = 4');
		
		$nb4 = $db->getQuery()->getResult();
		
		$search = 0;
		$solution1 = 0;
		$solution2 = 0;
		$solution3 = 0;
		$solution4 = 0;
		$solution5 = 0;
		$solution6 = 0;
		
		if(isset($_GET['vousetes']) || isset($_GET['assurances']))
		{
			$search = 1;
			
			$vousetes = $_GET['vousetes'];
			$assurances = $_GET['assurances'];
			
			if($vousetes == '1')
			{
				if(in_array('1', $assurances))
				{
					$solution1 = 1;
					if(in_array('2', $assurances))
					{
						$solution2 = 0;
					}
				}
				if(in_array('2', $assurances) && $solution1 == 0)
				{
					$solution2 = 1;
				}
				if(in_array('4', $assurances))
				{
					$solution3 = 1;
				}
				if(in_array('5', $assurances))
				{
					$solution4 = 1;
				}
			}
			elseif($vousetes == '2')
			{
				if(in_array('3', $assurances))
				{
					$solution5 = 1;
				}
				if(in_array('4', $assurances))
				{
					$solution3 = 1;
				}
				if(in_array('5', $assurances))
				{
					$solution4 = 1;
				}
			}
			elseif($vousetes == '3')
			{
				if(in_array('2', $assurances))
				{
					$solution6 = 1;
				}
			}
		}
		 
		return array(
    		'solutions'      => $solutions,
			'actif' => 1,
			'nb1' => $nb1[0]['nb'],
			'nb2' => $nb2[0]['nb'],
			'nb3' => $nb3[0]['nb'],
			'nb4' => $nb4[0]['nb'],
			'solution1' => $solution1,
			'solution2' => $solution2,
			'solution3' => $solution3,
			'solution4' => $solution4,
			'solution5' => $solution5,
			'solution6' => $solution6,
			'search' => $search,
    	);
	}
	
    /**
     * @Route("/Nos-solutions-TNS/{slug}", name="solution_tns_show", requirements={"slug"=".+"})
     * @Method("GET")
     * @Template("Solution/show.html.twig")
     */
    public function showTnsAction($slug = "")
    {
        $em = $this->getDoctrine()->getManager();
    
    	$entity = $em->getRepository('AppBundle:Solution')->findBySlug($slug);
        
    	if (!$entity) {
    		throw $this->createNotFoundException('Unable to find Solution entity.');
    	}
    
    	return array(
    		'entity'      => $entity[0],
    		'actif' => 1,
    	);
    }
    
    /**
     * @Route("/Nos-solutions-entreprise/{slug}", name="solution_entreprise_show", requirements={"slug"=".+"})
     * @Method("GET")
     * @Template("Solution/show.html.twig")
     */
    public function showEntrepriseAction($slug = "")
    {
    	$em = $this->getDoctrine()->getManager();
    
    	$entity = $em->getRepository('AppBundle:Solution')->findBySlug($slug);
    
    	if (!$entity) {
    		throw $this->createNotFoundException('Unable to find Solution entity.');
    	}
    
    	return array(
    			'entity'      => $entity[0],
    			'actif' => 1,
    	);
    }
}
