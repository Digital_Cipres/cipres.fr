$(function() {
//====================================================================
//======================== GESTION DU SOMMAIRE===== ==================
//====================================================================
    // Suppression des paramètres utm_source et club pour que le lien de la première actu cliquée ne s'affiche pas
    var win = window;
    var removeUtms = function(){
        var location = win.location;
        if (location.search.indexOf('utm_') != -1 && history.replaceState) {
            history.replaceState({}, '', window.location.toString().replace(/(\&|\?)utm([_a-zA-Z0-9=]+)/g, ""));
        }
        if (location.search.indexOf('club') != -1 && history.replaceState) {
            history.replaceState({}, '', window.location.toString().replace(/(\&|\?)club([_a-zA-Z0-9=]+)/g, ""));
        }
    };
    removeUtms();

    // Récupération du slug de l'url pour masquer l'actu en cours dans le sommaire
    var url = decodeURIComponent(window.location);
    var slugEnews = url.split("/").pop();

    $('.sommaire a').each(function(){
        var urlSommaire = decodeURIComponent($(this).attr('href'));
        var urlToCompare = urlSommaire.split("/").pop();

        if(urlToCompare == slugEnews){
            $('a[href$="'+ urlToCompare +'"]').closest('li').hide();
        }
    });
});


//====================================================================
//=================== ANIMATION DES BLOCS AU SCROLL ==================
//====================================================================
jQuery(document).ready(function() {
    jQuery('.post').addClass("notdisplayed").viewportChecker({
        classToAdd: 'displayed animated bounceInUp',
        offset: 100
    });

    jQuery('.bloc').addClass("notdisplayed").viewportChecker({
        classToAdd: 'displayed animated flipInX',
        offset: 120
    });

    jQuery('.button').addClass("notdisplayed").viewportChecker({
        classToAdd: 'displayed animated pulse',
        offset: 120
    });
});
