$(function() {
    //=====================================================
    //======== Pulgin materialize pour les selects ========
    //=====================================================
    $('select').material_select();


    //=====================================================
    //======== Adresse remplie selon code postal ==========
    //=====================================================
    $('#code_postal').blur(function(){
        var zip = $(this).val();
        var city = '';
        //requête à google geocode api
        $.getJSON('http://maps.googleapis.com/maps/api/geocode/json?address='+zip+'&components=country:FR').success(function(response){
            //trouve la ville
            var address_components = response.results[0].address_components;
            $.each(address_components, function(index, component){
                var types = component.types;
                $.each(types, function(index, type){
                    if(type == 'locality') {
                        city = component.long_name;
                    }
                    if(type == 'administrative_area_level_1') {
                        state = component.short_name;
                    }
                });
            });
            //pré-remplie la ville
            $('#ville').val(city);
            $('#ville-container .select-dropdown').val(city);

            var cities = response.results[0].postcode_localities;
            if(cities) {
                //turn city into a dropdown if necessary
                var $select = $(document.createElement('select'));
                $.each(cities, function(index, locality){
                    var $option = $(document.createElement('option'));
                    $option.html(locality);
                    $option.attr('value',locality);
                    if(city == locality) {
                        $option.attr('selected','selected');
                    }
                    $select.append($option);
                });
                $select.attr( { id:"ville", name:"ville"} );
                $('#ville_container').html($select);
                $('#ville_container select').material_select();
            } else {
                $('#ville').val(city);
            }
        });
    });

    $('select').change(function(){
        if ($(this).val()!=''){
            console.log("change");
            var parent = $(this).parent();
            $(parent).children('input').addClass('valid');
        }
    });

    $('#ville_container select').change(function(){
        if ($(this).val()!=''){
            console.log("change");
            var parent = $(this).parent();
            $(parent).children('input').addClass('valid');
        }
    });

    //==============================================================
    //======== Affichage du champ tel selon choix contact ==========
    //==============================================================
    $('input:radio[name="demandes_geoloc[choixContact]"]').change(function(){
        if($(this).val() == 'Oui'){
            $('.telephone').show();
            $('.telephone input').attr('required', 'required');
        }
        else {
            $('.telephone input').removeAttr('required');
            $('.telephone').hide();
        }
    });


    //====================================================================
    //=================== ANIMATION DES BLOCS AU SCROLL ==================
    //====================================================================
    $(document).ready(function() {
        $('.bloc_etapes').addClass("notdisplayed").viewportChecker({
            classToAdd: 'displayed animated slideInLeft',
            offset: 20
        })
    });

    $('#trigger_geoloc').click(function(e){
        e.preventDefault();
        $('.container-geoloc').removeClass('col-sm-6').addClass('col-sm-12');

        // Animation des blocs choix de la réintermédiation
        var animBlocGeoloc = new TimelineLite();
        animBlocGeoloc.to(".container-devis", 1, {display: "none"})
            .to(".choix_reintermediation", 1, {width: "100%"});
    });


    //====================================================================
    //=================== INITIALISE PLUGIN GEOLOC =======================
    //====================================================================

    $(window).load(function() {
        // Check viewport width
        var viewport = $(window).width();

        if(viewport < 361) {
            $('#bh-sl-map-container').storeLocator({'mapSettings' : {
                zoom     : 15
            }});
        }

        else {
            $('#bh-sl-map-container').storeLocator();
        }
    });





    //==============================================================
    //=========== Empêche double soumission du formulaire ==========
    //==============================================================
    // $(document).ready(function() {
    //     $('form').submit(function() {
    //         if(typeof jQuery.data(this, "disabledOnSubmit") == 'undefined') {
    //             jQuery.data(this, "disabledOnSubmit", { submited: true });
    //             $('button[type=submit]', this).each(function() {
    //                 $(this).attr("disabled", "disabled");
    //             });
    //             return true;
    //         }
    //         else
    //         {
    //             return false;
    //         }
    //     });
    // });
});