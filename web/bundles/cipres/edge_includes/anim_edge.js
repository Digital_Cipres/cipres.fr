/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='http://pcevent2.fr/web/bundles/cipres/img/',
        js='../js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'scroll',
                            type: 'image',
                            rect: ['0px', '13px', '37px', '20px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"scroll.png",'0px','0px']
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: [undefined, undefined, '37px', '42px'],
                            overflow: 'hidden',
                            fill: ["rgba(32,36,211,1.00)"]
                        }
                    }
                },
                timeline: {
                    duration: 1750,
                    autoPlay: true,
                    data: [
                        [
                            "eid1",
                            "background-image",
                            0,
                            0,
                            "linear",
                            "${Stage}",
                            [270,[['rgba(255,255,255,0)',0],['rgba(255,255,255,0)',100]]],
                            [270,[['rgba(255,255,255,0)',0],['rgba(255,255,255,0)',100]]]
                        ],
                        [
                            "eid2",
                            "background-color",
                            0,
                            0,
                            "linear",
                            "${Stage}",
                            'rgba(32,36,211,1.00)',
                            'rgba(32,36,211,1.00)'
                        ],
                        [
                            "eid7",
                            "top",
                            0,
                            0,
                            "linear",
                            "${scroll}",
                            '13px',
                            '13px'
                        ],
                        [
                            "eid9",
                            "top",
                            1500,
                            250,
                            "linear",
                            "${scroll}",
                            '13px',
                            '23px'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("http://pcevent2.fr/web/bundles/cipres/edge_includes/anim_edgeActions.js");
})("EDGE-303393871");
